# kxClans

kxClans is a CraftBukkit, Spigot, and Sponge plugin designed so players can found their own clans to claim and own land.

## Getting Started

Follow these directions to install and run kxClans on your server.

### Prerequisites

A Minecraft server modded with CraftBukkit, Spigot, or Sponge is required to run kxClans.

### Installing

Installing kxClans is as simple as placing the compiled JAR in your server's plugins folder. kxClans will automatically start in the proper mode for your server; the same JAR will work on CraftBukkit, Spigot, and Sponge.

## Configuration

kxClans will create a config.yml file within its own data folder in your plugins folder. This configuration contains all settings for the plugin.

### Configuration Sections

* database - Select the type of database kxClans will use. Supported options: yaml, sql, none (for custom database solutions).
* cachedata - Use in-memory caching for the database.
* sql - Database connection settings for MySQL.
* powerperhour - Amount of power to give players per hour of playtime.
* sysclans - Configure the names and descriptions of the three system clans.
* enemycmdsblocked - A list of commands to block when a player is in enemy territory.
* tpdelay - Time in seconds to wait before teleporting a player.
* showTerritoryWithTitles - Toggle between titles and messages in chat when a player enters territory.
* colors - Color codes to use for different levels of clan policy. Refer to [this page](https://minecraft.gamepedia.com/Formatting_codes#Color_codes) for the codes.
* showadminflags - Show admin flags to users without /cadmin bypass enabled.
* flags - Toggle flags and who can use them.
* ranktitles - Titles to use in addition to player-set titles in clan messages.
* deathpowerloss - Amount of power to lose when the player dies.
* chatformat - Have kxClans format the chat to add a clan prefix; use only if another chat formatting plugin is unavailable.
* chatkeys - Text to replace in the chat format.
* clanchats - Enable or disable specific levels of clan chat.
* defaultperms - Default permissions when creating a clan.
* configVersion - Used by the plugin to tell if the config needs to be updated to add new keys.

## Permissions

All permissions in kxClans default to OP.
Access to /c - kxclans.command; access to /cadmin - kxclans.admin.
All user commands have the permission kxclans.command.<command> with <command> replaced with the command name.
All admin commands have the permission kxclans.admin.<command>.

### Permission Groups

* kxclans.group.user - Grants access to all user commands.
* kxclans.group.mod - Grants access to all user commands, and /cadmin bypass.
* kxclans.group.admin - Gracts access to all commands.

## Author

* NESlover - plugin code