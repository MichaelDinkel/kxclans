package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.Rank;
import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandLeader implements Subcommand {

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.leader")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to set a faction leader.");
            return;
        }

        try {

            StringBuilder clan = new StringBuilder(ClansDatabase.getDatabase().getClan((Player) sender));
            boolean otherclan = false;

            if (args.length > 2) {

                clan = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                    sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                    return;
                }

                otherclan = true;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan.toString());

            if (!PermissionManager.getInstance().canUse(sender, "leader", clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to change its leader.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.YELLOW + "Set the clan leader");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " leader <Leader> [Clan]");
                return;
            }

            String leader = ClansDatabase.getDatabase().getLeader(clan.toString());

            if (leader != null && leader.equalsIgnoreCase(args[1])) {

                sender.sendMessage(leader + ChatColor.RED + " already leads " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);

                return;
            }

            OfflinePlayer newLeader = Bukkit.getOfflinePlayer(args[1]);

            List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan.toString());

            if (!players.contains(newLeader.getUniqueId())) {
                sender.sendMessage(args[1] + ChatColor.RED + " is not a member of " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);

                return;
            }

            ClansDatabase.getDatabase().setRank(newLeader.getUniqueId(), Rank.LEADER);
            if (leader != null) ClansDatabase.getDatabase().setRank(Bukkit.getOfflinePlayer(leader).getUniqueId(), Rank.CAPTAIN);

            for (UUID u : players) {

                Player p = Bukkit.getPlayer(u);

                if (p != null) {

                    p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + sender.getName() + ChatColor.YELLOW + " has set the clan leader to "
                            + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + newLeader.getName());

                }
            }

            if (otherclan) {

                String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW + " has set the clan leader to  "
                        + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(senderclan, clan.toString())) + newLeader.getName());
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
