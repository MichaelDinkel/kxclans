package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandDescription implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.desc")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to set a clan description.");
            return;
        }

        try {

            String clan = ClansDatabase.getDatabase().getClan((Player) sender);

            if (!PermissionManager.getInstance().canUse(sender, "desc", clan)) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan + ChatColor.RED + " does not allow you to set the description.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            if (args.length == 1) {

                ClansDatabase.getDatabase().setDesc(clan, "");

                List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan);

                for (UUID u : players) {

                    Player p = Bukkit.getPlayer(u);

                    if (p != null) {

                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW
                                + " has removed the clan description.");

                    }
                }

            } else {

                StringBuilder desc = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    desc.append(args[i]);
                    desc.append(" ");

                }

                desc = new StringBuilder(desc.substring(0, desc.length() - 1));

                if (!desc.toString().matches("[a-zA-Z0-9 ]*")) {

                    sender.sendMessage(ChatColor.RED + "The clan description " + ChatColor.WHITE + desc.toString() + ChatColor.RED + " contains invalid characters. Clan descriptions may only contain letters, numbers, and spaces.");

                    return;

                }

                if (desc.length() > 128) {

                    sender.sendMessage(ChatColor.RED + "The clan description " + ChatColor.WHITE + desc.toString() + ChatColor.RED + " is too long. Clan descriptions can be at most 128 characters.");

                    return;

                }

                ClansDatabase.getDatabase().setDesc(clan, desc.toString());

                List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan);

                for (UUID u : players) {

                    Player p = Bukkit.getPlayer(u);

                    if (p != null) {

                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW
                                + " has set the clan description to " + ChatColor.WHITE + desc.toString());

                    }
                }
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
