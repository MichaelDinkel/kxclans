package com.kaixeleron.clans.subcommand.user;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SubcommandMap implements Subcommand {

    private final char[] chars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdeghjmnopqrsuvwxyz=&\\/%^$£?".toCharArray();
    private final int max = chars.length;

    @Override
    public void execute(final CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.map")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to display a faction map.");
            return;
        }

        Chunk c = ((Player) sender).getLocation().getChunk();

        final int cx = c.getX(), cz = c.getZ();

        final Table<Integer, Integer, String> chunks = HashBasedTable.create();
        final Map<String, Policy> relations = new ConcurrentHashMap<>();

        String direction = getCardinalDirection((Player) sender);

        String senderclan;

        try {
            senderclan = ClansDatabase.getDatabase().getClan((Player) sender);
        } catch (DatabaseException e) {
            e.printStackTrace();
            return;
        }

        try {

            chunks.putAll(ClansDatabase.getDatabase().getChunkOwners(cx - 26, cx + 26, cz - 4, cz + 5));

            for (String owner : chunks.values()) {

                if (!relations.containsKey(owner)) {

                    relations.put(owner, ClansDatabase.getDatabase().getPolicy(owner, senderclan));

                }

            }

        } catch (DatabaseException e) {

            e.printStackTrace();

        }

        sender.sendMessage(ChatColor.GREEN + "---------------------[ " + ChatColor.WHITE + "Clan Map " + ChatColor.GREEN + "]---------------------");

        StringBuilder builder = new StringBuilder();

        int currentchar = -1;

        List<String> mapped = new ArrayList<>();
        Map<Character, String> keys = new HashMap<>();

        for (int z = cz - 4; z < cz + 5; z++) {

            for (int x = cx - 26; x < cx + 26; x++) {

                String owner;

                if (chunks.contains(x, z)) {

                    owner = chunks.get(x, z);

                } else {

                    owner = ClansDatabase.getUnclaimedName();

                }

                if (z == cz - 4 && x == cx - 26) {
                    builder.append(direction.equals("NW") ? ChatColor.RED : ChatColor.YELLOW).append("\\");
                } else if (z == cz - 4 && x == cx - 25) {
                    builder.append(direction.equals("N") ? ChatColor.RED : ChatColor.YELLOW).append("N");
                } else if (z == cz - 4 && x == cx - 24) {
                    builder.append(direction.equals("NE") ? ChatColor.RED : ChatColor.YELLOW).append("/");
                } else if (z == cz - 3 && x == cx - 26) {
                    builder.append(direction.equals("W") ? ChatColor.RED : ChatColor.YELLOW).append("W");
                } else if (z == cz - 3 && x == cx - 25) {
                    builder.append(ChatColor.YELLOW).append("+");
                } else if (z == cz - 3 && x == cx - 24) {
                    builder.append(direction.equals("E") ? ChatColor.RED : ChatColor.YELLOW).append("E");
                } else if (z == cz - 2 && x == cx - 26) {
                    builder.append(direction.equals("SW") ? ChatColor.RED : ChatColor.YELLOW).append("/");
                } else if (z == cz - 2 && x == cx - 25) {
                    builder.append(direction.equals("S") ? ChatColor.RED : ChatColor.YELLOW).append("S");
                } else if (z == cz - 2 && x == cx - 24) {
                    builder.append(direction.equals("SE") ? ChatColor.RED : ChatColor.YELLOW).append("\\");
                } else if (z == cz && x == cx) {
                    builder.append(ChatColor.RED).append("+");
                } else if (owner.equals(ClansDatabase.getUnclaimedName())) {
                    builder.append(ChatColor.GRAY).append("-");
                } else {

                    if (currentchar > max) {
                        currentchar = -1;
                    }

                    if (!mapped.contains(owner)) {
                        mapped.add(owner);
                        currentchar++;
                        keys.put(chars[currentchar], owner);
                    }

                    for (Character ch : keys.keySet()) {

                        if (keys.get(ch).equals(owner)) {

                            if (owner.equals(ClansDatabase.getSafeName())) {

                                builder.append(ChatColor.GOLD);

                            } else if (owner.equals(ClansDatabase.getWarName())) {

                                builder.append(ChatColor.DARK_RED);

                            } else {

                                builder.append(ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors." + relations.get(owner).toString()));

                            }

                            builder.append(ch);

                        }

                    }

                }
            }

            sender.sendMessage(builder.toString());

            builder = new StringBuilder();
        }

        StringBuilder list = new StringBuilder();

        for (char ch : keys.keySet()) {

            String clan = keys.get(ch);

            if (clan.equals(ClansDatabase.getSafeName())) {

                list.append(ChatColor.GOLD).append(ch).append(" - ").append(keys.get(ch)).append(" ");

            } else if (clan.equals(ClansDatabase.getWarName())) {

                list.append(ChatColor.DARK_RED).append(ch).append(" - ").append(keys.get(ch)).append(" ");

            } else {

                list.append(ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors." + relations.get(keys.get(ch)).toString())).append(ch).append(" - ").append(keys.get(ch)).append(" ");

            }

        }

        sender.sendMessage(list.toString());

    }

    private String getCardinalDirection(Player player) {

        float yaw = player.getLocation().getYaw() * -1.0F;

        if (yaw >= 0 && yaw < 22.5) {

            return "S";

        } else if (yaw >= 22.5 && yaw < 67.5) {

            return "SE";

        } else if (yaw >= 67.5 && yaw < 112.5) {

            return "E";

        } else if (yaw >= 112.5 && yaw < 157.5) {

            return "NE";

        } else if (yaw >= 157.5 && yaw < 202.5) {

            return "N";

        } else if (yaw >= 202.5 && yaw < 247.5) {

            return "NW";

        } else if (yaw >= 247.5 && yaw < 292.5) {

            return "W";

        } else if (yaw >= 292.5 && yaw < 337.5) {

            return "SW";

        } else if (yaw >= 337.5 && yaw < 360.0) {

            return "S";

        } else {

            return "";

        }
    }
}
