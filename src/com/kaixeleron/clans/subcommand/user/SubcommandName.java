package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SubcommandName implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.name")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to set clan names.");
            return;
        }

        try {

            if (args.length == 1) {
                sender.sendMessage(ChatColor.YELLOW + "Set the clan name");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " name <Name>");
                return;
            }

            String clan = ClansDatabase.getDatabase().getClan((Player) sender);

            if (!PermissionManager.getInstance().canUse(sender, "name", clan)) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan + ChatColor.RED + " does not allow you to rename the clan.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            StringBuilder name = new StringBuilder();

            for (int i = 1; i < args.length; i++) {
                name.append(args[i]).append(" ");
            }

            name = new StringBuilder(name.substring(0, name.length() - 1));

            if (!name.toString().matches("[a-zA-Z ]*")) {

                sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " contains invalid characters. Clan names may only contain letters and spaces.");

                return;

            }

            if (name.length() < 3) {

                sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " is too short. Clan names must be at most 3 characters.");

                return;

            }

            if (name.length() > 20) {

                sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " is too long. Clan names can be at most 20 characters.");

                return;

            }

            if (ClansDatabase.getDatabase().clanExists(name.toString()).length() > 0) {
                sender.sendMessage(ChatColor.RED + "A clan named " + ChatColor.WHITE + name.toString() + ChatColor.RED + " already exists.");
            } else {

                ClansDatabase.getDatabase().setName(clan, name.toString());

                for (UUID u : ClansDatabase.getDatabase().getPlayers(name.toString())) {

                    Player p = Bukkit.getPlayer(u);

                    if (p != null) p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                            + sender.getName() + ChatColor.YELLOW + " has renamed " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                            .getString("colors.CLAN") + clan + ChatColor.YELLOW + " to " + ChatColor.COLOR_CHAR + ClansMain.getInstance()
                            .getConfig().getString("colors.CLAN") + name.toString());

                }
            }

        } catch (DatabaseException e) {
            sender.sendMessage("A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
