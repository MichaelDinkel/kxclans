package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandDelWarp implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.delwarp")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to delete a clan warp.");
            return;
        }

        try {

            StringBuilder clan = new StringBuilder(ClansDatabase.getDatabase().getClan((Player) sender));
            boolean otherclan = false;

            if (args.length > 2) {

                clan = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                    sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                    return;
                }

                otherclan = true;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan.toString());

            if (!PermissionManager.getInstance().canUse(sender, "delwarp", clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to delete warps.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.YELLOW + "Delete a clan warp");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " delwarp <Warp> [Clan]");
                return;
            }

            String warp = args[1];

            Location l = ((Player) sender).getLocation();

            List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan.toString());

            for (UUID u : players) {

                Player p = Bukkit.getPlayer(u);

                if (p != null) {

                    p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + sender.getName() + ChatColor.YELLOW + " has deleted the clan warp "
                            + ChatColor.WHITE + warp + ChatColor.YELLOW + " for " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                }
            }

            if (otherclan) {

                String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW + " has deleted the clan warp "
                        + ChatColor.WHITE + warp + ChatColor.YELLOW + " for " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(senderclan, clan.toString())) + clan);
            }

            ClansDatabase.getDatabase().delWarp(clan.toString(), warp);

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
