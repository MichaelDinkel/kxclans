package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class SubcommandList implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.list")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to list clans.");
            return;
        }

        try {

            int total = ClansDatabase.getDatabase().getClanCount();

            int page = 1;

            if (args.length > 1) {

                try {

                    page = Integer.parseInt(args[1]);

                    if (page < 1 || (page - 1) * 10 > total) {
                        page = 1;
                    }

                } catch (NumberFormatException ignored) {}
            }

            String[] clans = ClansDatabase.getDatabase().getClans(page);

            String numbers = Integer.toString(page) + Integer.toString(total / 10 + 1);

            String equals = "==================";
            String subequals = equals.substring(0, equals.length() - numbers.length() / 2);

            sender.sendMessage(ChatColor.RED + subequals + "[ " + ChatColor.WHITE + "Clan List " + ChatColor.GOLD + "Page: "
                    + ChatColor.WHITE + page + ChatColor.GOLD + "/" + ChatColor.WHITE + (total / 10 + 1) + ChatColor.RED + " ]" + subequals);

            String userclan = ClansDatabase.getDatabase().getClan((Player) sender);

            for (String s : clans) {
                if (s.length() > 0) {

                    List<UUID> memberlist = ClansDatabase.getDatabase().getPlayers(s);

                    int members = memberlist.size(), online = 0, land = ClansDatabase.getDatabase().getClaimedLand(s);
                    String power, maxpower;

                    BigDecimal powerdecimal = new BigDecimal(ClansDatabase.getDatabase().getClanPower(s));
                    powerdecimal = powerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();
                    power = powerdecimal.toPlainString();

                    BigDecimal maxpowerdecimal = new BigDecimal(ClansDatabase.getDatabase().getClanMaxPower(s));
                    maxpowerdecimal = maxpowerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();
                    maxpower = maxpowerdecimal.toPlainString();

                    for (UUID u : memberlist) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null && ((Player) sender).canSee(p)) online++;

                    }

                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                            + ClansDatabase.getDatabase().getPolicy(userclan, s).toString()) + s + " " + ChatColor.GOLD
                            + members + " member" + (members == 1 ? "" : "s") + ", " + online + " online. " + ChatColor.GOLD
                            + "L:" + ChatColor.WHITE +  land + ChatColor.GOLD + " P:" + ChatColor.WHITE + power
                            + ChatColor.GOLD + " MP:" + ChatColor.WHITE + maxpower);
                }
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
