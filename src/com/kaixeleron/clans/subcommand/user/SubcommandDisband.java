package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandDisband implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.disband")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to disband a clan.");
            return;
        }

        try {

            StringBuilder clan = new StringBuilder(ClansDatabase.getDatabase().getClan((Player) sender));

            if (args.length > 1) {

                clan = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                    sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                    return;
                }
            }

            String syscomp = clan.toString();

            if (syscomp.equals(ClansDatabase.getUnclaimedName()) || syscomp.equals(ClansDatabase.getWarName()) || syscomp.equals(ClansDatabase.getSafeName())) {

                sender.sendMessage(ChatColor.RED + "System clans cannot be disbanded.");
                return;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan.toString());

            if (!PermissionManager.getInstance().canUse(sender, "disband", clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to disband the clan.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan.toString());

            for (UUID u : players) {

                Player p = Bukkit.getPlayer(u);

                if (p != null) {

                    p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + sender.getName() + ChatColor.YELLOW + " has disbanded " + ChatColor.COLOR_CHAR
                            + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                }
            }

            if (!players.contains(((Player) sender).getUniqueId())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "You " + ChatColor.YELLOW + "have disbanded " + ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);
            }

            ClansDatabase.getDatabase().deleteClan(clan.toString());

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
