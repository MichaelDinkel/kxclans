package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

//Multi-use subcommand for all relations
public class SubcommandPolicy implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        args[0] = args[0].toLowerCase();

        if (!sender.hasPermission("kxclans.command." + args[0])) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to manage " + args[0] + " foreign policy.");
            return;
        }

        try {

            String clan = ClansDatabase.getDatabase().getClan((Player) sender);
            
            if (!PermissionManager.getInstance().canUse(sender, args[0], clan)) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan + ChatColor.RED + " does not allow you to manage " + args[0] + " foreign policy.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                
                return;
            }
            
            if (args.length < 2) {
                
                sender.sendMessage(ChatColor.YELLOW + "Manage " + args[0] + " foreign policy with other clans");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " " + args[0] + " <Clan>");
                
                return;
            }

            String target = ClansDatabase.getDatabase().clanExists(args[1]);

            if (target.length() == 0) {

                sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + args[1] + ChatColor.RED + " does not exist.");

                return;
            }

            if (target.equalsIgnoreCase(clan)) {

                sender.sendMessage(ChatColor.RED + "A clan cannot have foreign policy with itself.");

                return;
            }

            Policy rel = Policy.valueOf(args[0].toUpperCase());
            Policy current = ClansDatabase.getDatabase().getPolicy(clan, target);

            if (rel.equals(current)) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target + ChatColor.YELLOW + " is already a " + rel.toString().toLowerCase() + " clan.");

                return;

            }

            switch (rel) {
                case ALLY:
                case TRUCE:

                    ClansDatabase.getDatabase().setPolicy(clan, target, rel);

                    Policy policy = ClansDatabase.getDatabase().getPolicy(clan, target);

                    if (policy.equals(rel)) {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target + ChatColor.YELLOW + " is now a" + (rel.equals(Policy.ALLY) ? "n allied" : " truced") + " clan.");

                            }
                        }

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(target)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.YELLOW + " is now a" + (rel.equals(Policy.ALLY) ? "n allied" : " truced") + " clan.");

                            }

                        }

                    } else {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                p.sendMessage(ChatColor.YELLOW + "A request for " + (rel == Policy.TRUCE ? "truce" : "alliance") + " has been sent to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + policy.toString()) + target);

                            }
                        }

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(target)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + policy.toString()) + clan + ChatColor.YELLOW + " has sent your clan a request for " + (rel.equals(Policy.TRUCE) ? "alliance" : "truce") + ".");

                            }

                        }

                    }

                    break;
                case NEUTRAL:

                    ClansDatabase.getDatabase().setPolicy(clan, target, Policy.NEUTRAL);

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target + ChatColor.YELLOW + " is now a neutral clan.");

                        }
                    }

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(target)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.YELLOW + " is now a neutral clan.");

                        }

                    }

                    break;
                case ENEMY:

                    ClansDatabase.getDatabase().setPolicy(clan, target, Policy.ENEMY);

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target + ChatColor.YELLOW + " is now an enemy clan.");

                        }
                    }

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(target)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.YELLOW + " is now an enemy clan.");

                        }

                    }

                    break;
            }
            
        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
