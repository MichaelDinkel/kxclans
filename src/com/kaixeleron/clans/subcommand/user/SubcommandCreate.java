package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandCreate implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.create")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to create a clan.");
            return;
        }

        if (args.length < 2) {
            sender.sendMessage(ChatColor.YELLOW + "Create a clan.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " create <Name>");
            return;
        }

        if (!PermissionManager.getInstance().canUse(sender, "create", "")) {
            sender.sendMessage(ChatColor.RED + "You are already in a clan. Leave your clan before creating a new one.");
            return;
        }

        StringBuilder name = new StringBuilder();

        for (int i = 1; i < args.length; i++) {
            name.append(args[i]).append(" ");
        }

        name = new StringBuilder(name.substring(0, name.length() - 1));

        if (!name.toString().matches("^[a-zA-Z ]*$")) {

            sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " contains invalid characters. Clan names may only contain letters and spaces.");

            return;

        }

        if (name.length() < 3) {

            sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " is too short. Clan names must be at most 3 characters.");

            return;

        }

        if (name.length() > 20) {

            sender.sendMessage(ChatColor.RED + "The clan name " + ChatColor.WHITE + name + ChatColor.RED + " is too long. Clan names can be at most 20 characters.");

            return;

        }

        try {

            boolean success = ClansDatabase.getDatabase().createClan((Player) sender, name.toString());

            if (!success) {
                sender.sendMessage(ChatColor.RED + "A clan named " + ChatColor.WHITE + name.toString() + ChatColor.RED + " already exists.");
            } else {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "You"
                        + ChatColor.YELLOW + " have created the clan: " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + name);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
