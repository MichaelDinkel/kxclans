package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ChatMode;
import com.kaixeleron.clans.data.ChatModeManager;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandChat implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be used by a player.");
            return;
        }

        if (!ClansMain.getInstance().getConfig().getBoolean("clanchats.enabled")) {
            sender.sendMessage(ChatColor.RED + "Clan chat has been disabled by the server administrator.");
            return;
        }

        if (args.length < 2) {

            sender.sendMessage(ChatColor.YELLOW + "Change your clan chat mode");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " chat <Mode>");
            ChatMode mode = ChatModeManager.getInstance().getMode((Player) sender);
            sender.sendMessage(ChatColor.YELLOW + "Your current chat mode is " + (mode.equals(ChatMode.PUBLIC) ? ChatColor.WHITE + "public" : ChatColor.COLOR_CHAR
                    + ClansMain.getInstance().getConfig().getString("colors." + mode.toString() + "") + mode.toString().toLowerCase()) + ChatColor.YELLOW + ". Available chat modes:");

            sender.sendMessage("p - Public ");
            if (ClansMain.getInstance().getConfig().getBoolean("clanchats.clan"))
                sender.sendMessage(ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "c - Visible only to your clan");
            if (ClansMain.getInstance().getConfig().getBoolean("clanchats.ally"))
                sender.sendMessage(ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors.ALLY") + "a - Visible to your clan and allies");
            if (ClansMain.getInstance().getConfig().getBoolean("clanchats.truce"))
                sender.sendMessage(ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors.TRUCE") + "t - Visible to your clan, allies, and truced clans");
            if (ClansMain.getInstance().getConfig().getBoolean("clanchats.enemy"))
                sender.sendMessage(ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors.ENEMY") + "e - Visible to your clan and enemies");
            return;
        }

        try {

            if (ClansDatabase.getDatabase().getClan((Player) sender).equals(ClansDatabase.getUnclaimedName())) {
                sender.sendMessage(ChatColor.RED + "You're not in a clan. Join or create a clan to use clan chat.");
                return;
            }

            switch (args[1].toLowerCase()) {
                case "p":
                case "public":

                    ChatModeManager.getInstance().setMode((Player) sender, ChatMode.PUBLIC);
                    sender.sendMessage(ChatColor.YELLOW + "Clan chat mode set to " + ChatColor.WHITE + "Public");

                    ClansDatabase.getDatabase().setChatMode(((Player) sender).getUniqueId(), ChatMode.PUBLIC);

                    break;
                case "c":
                case "clan":

                    if (ClansMain.getInstance().getConfig().getBoolean("clanchats.clan")) {

                        ChatModeManager.getInstance().setMode((Player) sender, ChatMode.CLAN);
                        sender.sendMessage(ChatColor.YELLOW + "Clan chat mode set to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "Clan");

                    } else {

                        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "Clan" + ChatColor.RED + " chat mode is disabled.");

                    }

                    ClansDatabase.getDatabase().setChatMode(((Player) sender).getUniqueId(), ChatMode.CLAN);

                    break;
                case "a":
                case "ally":

                    if (ClansMain.getInstance().getConfig().getBoolean("clanchats.ally")) {

                        ChatModeManager.getInstance().setMode((Player) sender, ChatMode.ALLY);
                        sender.sendMessage(ChatColor.YELLOW + "Clan chat mode set to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ALLY") + "Ally");

                    } else {

                        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ALLY") + "Ally" + ChatColor.RED + " chat mode is disabled.");

                    }

                    ClansDatabase.getDatabase().setChatMode(((Player) sender).getUniqueId(), ChatMode.ALLY);

                    break;
                case "t":
                case "truce":

                    if (ClansMain.getInstance().getConfig().getBoolean("clanchats.truce")) {

                        ChatModeManager.getInstance().setMode((Player) sender, ChatMode.TRUCE);
                        sender.sendMessage(ChatColor.YELLOW + "Clan chat mode set to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.TRUCE") + "Truce");

                    } else {

                        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.TRUCE") + "Truce" + ChatColor.RED + " chat mode is disabled.");

                    }

                    ClansDatabase.getDatabase().setChatMode(((Player) sender).getUniqueId(), ChatMode.TRUCE);

                    break;
                case "e":
                case "enemy":

                    if (ClansMain.getInstance().getConfig().getBoolean("clanchats.enemy")) {

                        ChatModeManager.getInstance().setMode((Player) sender, ChatMode.ENEMY);
                        sender.sendMessage(ChatColor.YELLOW + "Clan chat mode set to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ENEMY") + "Enemy");

                    } else {

                        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ENEMY") + "Enemy" + ChatColor.RED + " chat mode is disabled.");

                    }

                    ClansDatabase.getDatabase().setChatMode(((Player) sender).getUniqueId(), ChatMode.ENEMY);

                    break;
            }
        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
