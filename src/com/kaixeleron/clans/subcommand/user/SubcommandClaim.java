package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandClaim implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.claim")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to claim land.");
            return;
        }

        try {

            String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);
            if (senderclan.equals(ClansDatabase.getUnclaimedName())) {
                sender.sendMessage(ChatColor.RED + "You're not in a clan.");
                return;
            }

            StringBuilder clan = new StringBuilder(senderclan);
            boolean otherclan = false;

            if (args.length > 1) {

                clan = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                    sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                    return;
                }

                otherclan = true;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan.toString());

            if (!PermissionManager.getInstance().canUse(sender, "claim", clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to claim land.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            int land = ClansDatabase.getDatabase().getClaimedLand(clan.toString()), power = (int) ClansDatabase.getDatabase().getClanPower(clan.toString());

            if ((land + 1) > power && !PermissionManager.getInstance().isBypassing(sender) && !ClansDatabase.getDatabase().getFlag(clan.toString(), Flag.INFPOWER)) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not have enough power to claim more land.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            Chunk c = ((Player) sender).getLocation().getChunk();

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (owner.equals(clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " already owns this land.");
                return;
            }

            if (owner.equals(ClansDatabase.getSafeName()) || owner.equals(ClansDatabase.getWarName())) {

                sender.sendMessage(ChatColor.RED + "This land is owned by a system clan and cannot be claimed.");
                if (sender.hasPermission("kxclans.admin.unclaimsys"))
                    sender.sendMessage(ChatColor.YELLOW + "You may unclaim it with " + ChatColor.GREEN + "/cadmin unclaimsys");
                return;

            }

            if (!owner.equals(ClansDatabase.getUnclaimedName())) {

                if ((ClansDatabase.getDatabase().getClaimedLand(owner) <= (int) ClansDatabase.getDatabase().getClanPower(owner)) && !PermissionManager.getInstance().isBypassing(sender)) {

                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                            + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), owner).toString())
                            + owner + ChatColor.YELLOW + " owns this land and has enough power to keep it.");
                    if (sender.hasPermission("kxclans.admin.bypass"))
                        sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

                    return;
                }

                Location home = ClansDatabase.getDatabase().getHome(owner);

                if (home != null && home.getChunk().equals(c)) {
                    ClansDatabase.getDatabase().setHome(owner, null);
                }
            }

            List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan.toString());

            Policy clanrel = ClansDatabase.getDatabase().getPolicy(clan.toString(), owner);

            for (UUID u : players) {

                Player p = Bukkit.getPlayer(u);

                if (p != null) {

                    p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + sender.getName() + ChatColor.YELLOW + " has claimed chunk "
                            + ChatColor.WHITE + c.getX() + "," + c.getZ() + ChatColor.YELLOW + " for " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan
                            + (!owner.equals(ClansDatabase.getUnclaimedName()) ? ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + clanrel.toString()) + owner: ""));

                }
            }

            if (otherclan) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW + " has claimed chunk "
                        + ChatColor.WHITE + c.getX() + "," + c.getZ() + ChatColor.YELLOW + " for " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                        + ClansDatabase.getDatabase().getPolicy(senderclan, clan.toString())) + clan + (!owner.equals(ClansDatabase.getUnclaimedName()) ? ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR
                        + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(senderclan, owner).toString()) + owner: ""));
            }

            ClansDatabase.getDatabase().claim(clan.toString(), c);

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
