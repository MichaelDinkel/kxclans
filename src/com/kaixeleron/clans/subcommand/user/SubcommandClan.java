package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.subcommand.Subcommand;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandClan implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.c")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to display clans.");
            return;
        }

        try {

            StringBuilder clan = new StringBuilder();

            StringBuilder nameout;
            StringBuilder descout;
            StringBuilder ageout = new StringBuilder(ChatColor.YELLOW + "Age: ");
            ageout.append(ChatColor.WHITE);
            StringBuilder landout = new StringBuilder(ChatColor.YELLOW + "Land: ");
            landout.append(ChatColor.WHITE);
            StringBuilder powerout = new StringBuilder(ChatColor.YELLOW + "Power: ");
            powerout.append(ChatColor.WHITE);
            StringBuilder maxpowerout = new StringBuilder(ChatColor.YELLOW + "Max power: ");
            maxpowerout.append(ChatColor.WHITE);
            StringBuilder alliesout;
            StringBuilder trucedout;
            StringBuilder enemiesout;
            StringBuilder onlineout = new StringBuilder(ChatColor.YELLOW + "Members online ");
            StringBuilder offlineout = new StringBuilder(ChatColor.YELLOW + "Members offline ");


            if (args.length > 1) {

                for (int i = 1; i < args.length; i++) {
                    clan.append(args[i]).append(" ");
                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

            } else {

                clan = new StringBuilder(ClansDatabase.getDatabase().getClan((Player) sender));

            }


            if (clan.toString().equalsIgnoreCase(ClansDatabase.getUnclaimedName())) {

                displayUnclaimed(sender);

                return;
            }

            if (clan.toString().equalsIgnoreCase(ClansDatabase.getWarName())) {

                displayWar(sender);

                return;
            }

            if (clan.toString().equalsIgnoreCase(ClansDatabase.getSafeName())) {

                displaySafe(sender);

                return;
            }

            String dbclan = ClansDatabase.getDatabase().clanExists(clan.toString());

            if (dbclan.length() == 0) {

                sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                return;
            }


            int length = clan.length();

            if (length % 2 != 0) {
                length = length + 1;
            }

            int linelength = (50 - length) / 2;

            nameout = new StringBuilder(ChatColor.YELLOW + "");

            for (int i = 0; i < linelength; i++) {
                nameout.append("=");
            }

            nameout.append("[ " + ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(clan.toString(),
                    ClansDatabase.getDatabase().getClan((Player) sender)).toString())).append(clan).append(ChatColor.YELLOW).append(" ]");

            for (int i = 0; i < linelength; i++) {
                nameout.append("=");
            }


            descout = new StringBuilder(ChatColor.YELLOW + "Description: ");
            descout.append(ChatColor.WHITE);
            descout.append(ClansDatabase.getDatabase().getDesc(clan.toString()));


            ageout.append(millisToDate(System.currentTimeMillis() - ClansDatabase.getDatabase().getCreateDate(clan.toString())));


            Map<Flag, Boolean> flags = ClansDatabase.getDatabase().getFlags(clan.toString());
            StringBuilder flagslist = new StringBuilder();
            for (Flag f : flags.keySet()) {

                boolean value = flags.get(f);

                if (value != ClansMain.getInstance().getConfig().getBoolean("flags." + f.toString().toLowerCase() + ".default")) {
                    flagslist.append(value ? ChatColor.GREEN : ChatColor.RED);
                    flagslist.append(f.toString().toLowerCase());
                    flagslist.append(ChatColor.YELLOW);
                    flagslist.append(", ");
                }
            }

            String flagsout = "";
            if (flagslist.length() > 0) flagsout = ChatColor.YELLOW + "Flags: " + flagslist.substring(0, flagslist.length() - 4);


            landout.append(ClansDatabase.getDatabase().getClaimedLand(clan.toString()));

            BigDecimal powerdecimal = new BigDecimal(ClansDatabase.getDatabase().getClanPower(clan.toString()));
            powerdecimal = powerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();

            powerout.append(powerdecimal.toPlainString());


            BigDecimal maxpowerdecimal = new BigDecimal(ClansDatabase.getDatabase().getClanMaxPower(clan.toString()));
            maxpowerdecimal = maxpowerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();

            maxpowerout.append(maxpowerdecimal.toPlainString());


            List<String> allies = ClansDatabase.getDatabase().getClanPolicies(clan.toString(), Policy.ALLY);

            alliesout = new StringBuilder(ChatColor.YELLOW + "Allies (");
            alliesout.append(ChatColor.WHITE);
            alliesout.append(allies.size());
            alliesout.append(ChatColor.YELLOW);
            alliesout.append("): ");
            alliesout.append(ChatColor.WHITE);

            for (String s : allies) alliesout.append(ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors.ALLY")).append(s).append(", ");

            if (allies.size() > 0) alliesout = new StringBuilder(alliesout.substring(0, alliesout.length() - 2));


            List<String> truced = ClansDatabase.getDatabase().getClanPolicies(clan.toString(), Policy.TRUCE);

            trucedout = new StringBuilder(ChatColor.YELLOW + "Truced (");
            trucedout.append(ChatColor.WHITE);
            trucedout.append(truced.size());
            trucedout.append(ChatColor.YELLOW);
            trucedout.append("): ");
            trucedout.append(ChatColor.WHITE);

            for (String s : truced) trucedout.append(ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors.TRUCE")).append(s).append(", ");

            if (truced.size() > 0) trucedout = new StringBuilder(trucedout.substring(0, trucedout.length() - 2));


            List<String> enemies = ClansDatabase.getDatabase().getClanPolicies(clan.toString(), Policy.ENEMY);

            enemiesout = new StringBuilder(ChatColor.YELLOW + "Enemies (");
            enemiesout.append(ChatColor.WHITE);
            enemiesout.append(enemies.size());
            enemiesout.append(ChatColor.YELLOW);
            enemiesout.append("): ");
            enemiesout.append(ChatColor.WHITE);

            for (String s : enemies) enemiesout.append(ChatColor.COLOR_CHAR).append(ClansMain.getInstance().getConfig().getString("colors.ENEMY")).append(s).append(", ");

            if (enemies.size() > 0) enemiesout = new StringBuilder(enemiesout.substring(0, enemiesout.length() - 2));


            List<String> players = ClansDatabase.getDatabase().getPlayerNames(clan.toString()), online = new ArrayList<>(), offline = new ArrayList<>();

            for (String s : players) {

                Player p = Bukkit.getPlayerExact(s);

                if (p == null) {

                    offline.add(s);

                } else {

                    if (((Player) sender).canSee(p)) {
                        online.add(s);
                    } else {
                        offline.add(s);
                    }
                }
            }

            onlineout.append("(").append(ChatColor.WHITE).append(online.size()).append(ChatColor.YELLOW).append("): ").append(ChatColor.WHITE);

            for (String s : online) {
                String title = ClansDatabase.getDatabase().getPlayerTitleByName(s);

                onlineout.append(ClansMain.getInstance().getConfig().getString("ranktitles." + ClansDatabase.getDatabase().getClanRank(s, clan.toString())))
                        .append(title).append((title.length() > 0 ? " " : "")).append(s).append(", ");
            }

            if (online.size() > 0) onlineout = new StringBuilder(onlineout.substring(0, onlineout.length() - 2));

            offlineout.append("(").append(ChatColor.WHITE).append(offline.size()).append(ChatColor.YELLOW).append("): ").append(ChatColor.WHITE);

            for (String s : offline) {

                String title = ClansDatabase.getDatabase().getPlayerTitleByName(s);

                offlineout.append(ClansMain.getInstance().getConfig().getString("ranktitles." + ClansDatabase.getDatabase().getClanRank(s, clan.toString())))
                        .append(title).append((title.length() > 0 ? " " : "")).append(s).append(", ");
            }

            if (offline.size() > 0) offlineout = new StringBuilder(offlineout.substring(0, offlineout.length() - 2));


            sender.sendMessage(nameout.toString());
            if (descout.length() > 17) sender.sendMessage(descout.toString());
            sender.sendMessage(ageout.toString());
            if (flagslist.length() > 0) sender.sendMessage(flagsout);
            sender.sendMessage(landout.toString());
            sender.sendMessage(powerout.toString());
            sender.sendMessage(maxpowerout.toString());

            if (!allies.isEmpty()) {

                sender.sendMessage(alliesout.toString());

            }

            if (!truced.isEmpty()) {

                sender.sendMessage(trucedout.toString());

            }

            if (!enemies.isEmpty()) {

                sender.sendMessage(enemiesout.toString());

            }

            sender.sendMessage(onlineout.toString());
            sender.sendMessage(offlineout.toString());

        } catch (DatabaseException e) {

            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();

        }
    }

    private void displayUnclaimed(CommandSender sender) {

        String clan = ClansDatabase.getUnclaimedName();
        StringBuilder nameout;

        int length = clan.length();

        if (length % 2 != 0) {
            length = length + 1;
        }

        int linelength = (50 - length) / 2;

        nameout = new StringBuilder(ChatColor.YELLOW + "");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        nameout.append("[ ").append(ChatColor.DARK_GREEN).append(clan).append(ChatColor.YELLOW).append(" ]");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        sender.sendMessage(nameout.toString());
        sender.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + ClansDatabase.getUnclaimedDesc());
        sender.sendMessage(ChatColor.YELLOW + "This is a system clan. System clans have no age, permissions, relations, or members.");

    }

    private void displayWar(CommandSender sender) {

        String clan = ClansDatabase.getWarName();
        StringBuilder nameout;

        int length = clan.length();

        if (length % 2 != 0) {
            length = length + 1;
        }

        int linelength = (50 - length) / 2;

        nameout = new StringBuilder(ChatColor.YELLOW + "");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        nameout.append("[ ").append(ChatColor.DARK_RED).append(clan).append(ChatColor.YELLOW).append(" ]");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        sender.sendMessage(nameout.toString());
        sender.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + ClansDatabase.getWarDesc());
        sender.sendMessage(ChatColor.YELLOW + "This is a system clan. System clans have no age, permissions, relations, or members.");

    }

    private void displaySafe(CommandSender sender) {

        String clan = ClansDatabase.getSafeName();
        StringBuilder nameout;

        int length = clan.length();

        if (length % 2 != 0) {
            length = length + 1;
        }

        int linelength = (50 - length) / 2;

        nameout = new StringBuilder(ChatColor.YELLOW + "");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        nameout.append("[ ").append(ChatColor.GOLD).append(clan).append(ChatColor.YELLOW).append(" ]");

        for (int i = 0; i < linelength; i++) {
            nameout.append("=");
        }

        sender.sendMessage(nameout.toString());
        sender.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + ClansDatabase.getSafeDesc());
        sender.sendMessage(ChatColor.YELLOW + "This is a system clan. System clans have no age, permissions, relations, or members.");

    }

    private String millisToDate(long millis) {

        int years = 0, months = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0;

        while (millis >= 31556952000L) {
            millis = millis - 31556952000L;
            years = years + 1;
        }

        while (millis >= 2629746000L) {
            millis = millis - 31556952000L;
            months = months + 1;
        }

        while (millis >= 604800000L) {
            millis = millis - 604800000L;
            weeks = weeks + 1;
        }

        while (millis >= 86400000L) {
            millis = millis - 86400000L;
            days = days + 1;
        }

        while (millis >= 3600000L) {
            millis = millis - 3600000L;
            hours = hours + 1;
        }

        while (millis >= 60000L) {
            millis = millis - 60000L;
            minutes = minutes + 1;
        }

        while (millis >= 1000L) {
            millis = millis - 1000L;
            seconds = seconds + 1;
        }

        String out = "";

        if (years > 0) {
            out = out + years + " year" + (years == 1 ? "" : "s") + ", ";
        }

        if (months > 0) {
            out = out + months + " month" + (months == 1 ? "" : "s") + ", ";
        }

        if (weeks > 0) {
            out = out + weeks + " week" + (weeks == 1 ? "" : "s") + ", ";
        }

        if (days > 0) {
            out = out + days + " day" + (days == 1 ? "" : "s") + ", ";
        }

        if (hours > 0) {
            out = out + hours + " hour" + (hours == 1 ? "" : "s") + ", ";
        }

        if (minutes > 0) {
            out = out + minutes + " minute" + (minutes == 1 ? "" : "s") + ", ";
        }

        if (seconds > 0) {
            out = out + seconds + " second" + (seconds == 1 ? "" : "s") + ", ";
        }

        if (out.length() == 0) {
            out = "0 seconds";
        } else out = out.substring(0, out.length() - 2);

        return out;
    }
}
