package com.kaixeleron.clans.subcommand.user;

import com.google.common.collect.Table;
import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.Rank;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class SubcommandPerm implements Subcommand {

    private final int numPages = 3;

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        switch (args.length) {

            case 1:

                try {

                    showPerms(sender, ClansDatabase.getDatabase().getClan((Player) sender), 1);

                } catch (DatabaseException e) {

                    e.printStackTrace();

                }

                break;

            case 2:

                switch (args[0].toLowerCase()) {

                    case "set":

                        sender.sendMessage(ChatColor.YELLOW + "Manage clan permissions");
                        sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + String.format("/%s set <Perm> <True|False> <Rank> [Clan]", label));

                        break;

                    default:

                        int page;

                        try {

                            page = Integer.parseInt(args[1]);

                        } catch (NumberFormatException e) {

                            page = 1;

                        }

                        try {

                            showPerms(sender, ClansDatabase.getDatabase().getClan(((Player) sender).getUniqueId()), page);

                        } catch (DatabaseException e) {

                            e.printStackTrace();

                        }

                        break;

                }

                break;

            case 3:
            case 4:

                sender.sendMessage(ChatColor.YELLOW + "Manage clan permissions");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + String.format("/%s set <Perm> <Perm> <True|False> <Rank> [Clan]", label));

                break;

            default:

                String clan;

                if (args.length >= 6) {

                    StringBuilder builder = new StringBuilder();

                    for (int i = 5; i < args.length; i++) {

                        builder.append(args[i]).append(" ");

                    }

                    clan = builder.substring(0, builder.length() - 1);

                } else {

                    try {

                        clan = ClansDatabase.getDatabase().getClan((Player) sender);

                    } catch (DatabaseException e) {

                        clan = "";
                        e.printStackTrace();

                    }

                }

                try {

                    if (ClansDatabase.getDatabase().clanExists(clan) != null) {

                        boolean value = false, valid = true;

                        switch (args[3].toLowerCase()) {

                            case "yes":
                            case "on":
                            case "true":

                                value = true;

                                break;

                            case "no":
                            case "off":
                            case "nope":
                            case "false":

                                value = false;

                                break;

                            default:

                                valid = false;

                                break;

                        }

                        if (valid) {

                            try {

                                setPerm(sender, clan, Rank.valueOf(args[4].toUpperCase()), args[2], value);

                            } catch (DatabaseException e) {

                                e.printStackTrace();

                            } catch (IllegalArgumentException e) {

                                try {

                                    setPerm(sender, clan, Policy.valueOf(args[4].toUpperCase()), args[2], value);

                                } catch (IllegalArgumentException ex) {

                                    sender.sendMessage(ChatColor.RED + "Invalid rank. Type " + ChatColor.RESET + String.format("/%s perm", label) + ChatColor.RED + " to view ranks.");

                                }

                            }

                        } else {

                            sender.sendMessage(ChatColor.RED + "Invalid permission value. Must be a boolean value.");

                        }

                    }

                } catch (DatabaseException e) {

                    e.printStackTrace();

                }

                break;

        }


    }

    private void showPerms(CommandSender sender, String clan, int page) {

        if (page < 1) page = 1;
        if (page > numPages) page = numPages;

        try {

            Table<Rank, String, Boolean> perms = ClansDatabase.getDatabase().getMemberPerms(clan);

            String clanColor = ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN");

            String header = ChatColor.YELLOW + "RANK        " +
                    clanColor + "LEAD " +
                    clanColor + "CAPT " +
                    clanColor + "MEMB " +
                    clanColor + "NEWB " +
                    ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ALLY") + "ALLY " +
                    ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.TRUCE") + "TRUC " +
                    ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.NEUTRAL") + "NEUT " +
                    ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ENEMY") + "ENEM ";

            if (page == 1) {

                StringBuilder access = new StringBuilder(ChatColor.YELLOW + "ACCESS     "), ally = new StringBuilder(ChatColor.YELLOW + "ALLY        "),
                        build = new StringBuilder(ChatColor.YELLOW + "BUILD       "), claim = new StringBuilder(ChatColor.YELLOW + "CLAIM       "),
                        container = new StringBuilder(ChatColor.YELLOW + "CONTAINER "), delhome = new StringBuilder(ChatColor.YELLOW + "DELHOME " + ChatColor.BOLD + "  "),
                        delwarp = new StringBuilder(ChatColor.YELLOW + "DELWARP " + ChatColor.BOLD + "  "), demote = new StringBuilder(ChatColor.YELLOW + "DEMOTE     ");

                for (Rank r : perms.rowKeySet()) {

                    Map<String, Boolean> row = perms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "access":
                                if (row.get(s)) {
                                    access.append(ChatColor.GREEN);
                                    access.append("TRUE ");
                                } else {
                                    access.append(ChatColor.RED);
                                    access.append("NOPE ");
                                }
                                break;
                            case "ally":
                                if (row.get(s)) {
                                    ally.append(ChatColor.GREEN);
                                    ally.append("TRUE ");
                                } else {
                                    ally.append(ChatColor.RED);
                                    ally.append("NOPE ");
                                }
                                break;
                            case "build":
                                if (row.get(s)) {
                                    build.append(ChatColor.GREEN);
                                    build.append("TRUE ");
                                } else {
                                    build.append(ChatColor.RED);
                                    build.append("NOPE ");
                                }
                                break;
                            case "claim":
                                if (row.get(s)) {
                                    claim.append(ChatColor.GREEN);
                                    claim.append("TRUE ");
                                } else {
                                    claim.append(ChatColor.RED);
                                    claim.append("NOPE ");
                                }
                                break;
                            case "container":
                                if (row.get(s)) {
                                    container.append(ChatColor.GREEN);
                                    container.append("TRUE ");
                                } else {
                                    container.append(ChatColor.RED);
                                    container.append("NOPE ");
                                }
                                break;
                            case "delhome":
                                if (row.get(s)) {
                                    delhome.append(ChatColor.GREEN);
                                    delhome.append("TRUE ");
                                } else {
                                    delhome.append(ChatColor.RED);
                                    delhome.append("NOPE ");
                                }
                                break;
                            case "delwarp":
                                if (row.get(s)) {
                                    delwarp.append(ChatColor.GREEN);
                                    delwarp.append("TRUE ");
                                } else {
                                    delwarp.append(ChatColor.RED);
                                    delwarp.append("NOPE ");
                                }
                                break;
                            case "demote":
                                if (row.get(s)) {
                                    demote.append(ChatColor.GREEN);
                                    demote.append("TRUE ");
                                } else {
                                    demote.append(ChatColor.RED);
                                    demote.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                Table<Policy, String, Boolean> foreignPerms = ClansDatabase.getDatabase().getForeignPerms(clan);

                for (Policy r : foreignPerms.rowKeySet()) {

                    Map<String, Boolean> row = foreignPerms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "access":
                                if (row.get(s)) {
                                    access.append(ChatColor.GREEN);
                                    access.append("TRUE ");
                                } else {
                                    access.append(ChatColor.RED);
                                    access.append("NOPE ");
                                }
                                break;
                            case "ally":
                                if (row.get(s)) {
                                    ally.append(ChatColor.GREEN);
                                    ally.append("TRUE ");
                                } else {
                                    ally.append(ChatColor.RED);
                                    ally.append("NOPE ");
                                }
                                break;
                            case "build":
                                if (row.get(s)) {
                                    build.append(ChatColor.GREEN);
                                    build.append("TRUE ");
                                } else {
                                    build.append(ChatColor.RED);
                                    build.append("NOPE ");
                                }
                                break;
                            case "claim":
                                if (row.get(s)) {
                                    claim.append(ChatColor.GREEN);
                                    claim.append("TRUE ");
                                } else {
                                    claim.append(ChatColor.RED);
                                    claim.append("NOPE ");
                                }
                                break;
                            case "container":
                                if (row.get(s)) {
                                    container.append(ChatColor.GREEN);
                                    container.append("TRUE ");
                                } else {
                                    container.append(ChatColor.RED);
                                    container.append("NOPE ");
                                }
                                break;
                            case "delhome":
                                if (row.get(s)) {
                                    delhome.append(ChatColor.GREEN);
                                    delhome.append("TRUE ");
                                } else {
                                    delhome.append(ChatColor.RED);
                                    delhome.append("NOPE ");
                                }
                                break;
                            case "delwarp":
                                if (row.get(s)) {
                                    delwarp.append(ChatColor.GREEN);
                                    delwarp.append("TRUE ");
                                } else {
                                    delwarp.append(ChatColor.RED);
                                    delwarp.append("NOPE ");
                                }
                                break;
                            case "demote":
                                if (row.get(s)) {
                                    demote.append(ChatColor.GREEN);
                                    demote.append("TRUE ");
                                } else {
                                    demote.append(ChatColor.RED);
                                    demote.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                sender.sendMessage(ChatColor.YELLOW + "=====[ " + ChatColor.RESET + "Permissions for clan "
                        + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                        + ClansDatabase.getDatabase().getPolicy(clan, ClansDatabase.getDatabase().getClan((Player) sender)))
                        + clan + ChatColor.RESET + String.format(" (Page %d/%d)", page, numPages) + ChatColor.YELLOW + " ]=====");

                sender.sendMessage(header);
                sender.sendMessage(access.substring(0, access.length() - 1));
                sender.sendMessage(ally.substring(0, ally.length() - 1));
                sender.sendMessage(build.substring(0, build.length() - 1));
                sender.sendMessage(claim.substring(0, claim.length() - 1));
                sender.sendMessage(container.substring(0, container.length() - 1));
                sender.sendMessage(delhome.substring(0, delhome.length() - 1));
                sender.sendMessage(delwarp.substring(0, delwarp.length() - 1));
                sender.sendMessage(demote.substring(0, demote.length() - 1));

            } else if (page == 2) {

                StringBuilder desc = new StringBuilder(ChatColor.YELLOW + "DESC        "), disband = new StringBuilder(ChatColor.YELLOW + "DISBAND    "),
                        enemy = new StringBuilder(ChatColor.YELLOW + "ENEMY    " + ChatColor.BOLD + "  "), flag = new StringBuilder(ChatColor.YELLOW + "FLAG        "),
                        invite = new StringBuilder(ChatColor.YELLOW + "INVITE      "), title = new StringBuilder(ChatColor.YELLOW + "TITLE       "),
                        warp = new StringBuilder(ChatColor.YELLOW + "WARP        "), setwarp = new StringBuilder(ChatColor.YELLOW + "SETWARP " + ChatColor.BOLD + "  ");

                for (Rank r : perms.rowKeySet()) {

                    Map<String, Boolean> row = perms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "desc":
                                if (row.get(s)) {
                                    desc.append(ChatColor.GREEN);
                                    desc.append("TRUE ");
                                } else {
                                    desc.append(ChatColor.RED);
                                    desc.append("NOPE ");
                                }
                                break;
                            case "disband":
                                if (row.get(s)) {
                                    disband.append(ChatColor.GREEN);
                                    disband.append("TRUE ");
                                } else {
                                    disband.append(ChatColor.RED);
                                    disband.append("NOPE ");
                                }
                                break;
                            case "enemy":
                                if (row.get(s)) {
                                    enemy.append(ChatColor.GREEN);
                                    enemy.append("TRUE ");
                                } else {
                                    enemy.append(ChatColor.RED);
                                    enemy.append("NOPE ");
                                }
                                break;
                            case "flag":
                                if (row.get(s)) {
                                    flag.append(ChatColor.GREEN);
                                    flag.append("TRUE ");
                                } else {
                                    flag.append(ChatColor.RED);
                                    flag.append("NOPE ");
                                }
                                break;
                            case "invite":
                                if (row.get(s)) {
                                    invite.append(ChatColor.GREEN);
                                    invite.append("TRUE ");
                                } else {
                                    invite.append(ChatColor.RED);
                                    invite.append("NOPE ");
                                }
                                break;
                            case "title":
                                if (row.get(s)) {
                                    title.append(ChatColor.GREEN);
                                    title.append("TRUE ");
                                } else {
                                    title.append(ChatColor.RED);
                                    title.append("NOPE ");
                                }
                                break;
                            case "warp":
                                if (row.get(s)) {
                                    warp.append(ChatColor.GREEN);
                                    warp.append("TRUE ");
                                } else {
                                    warp.append(ChatColor.RED);
                                    warp.append("NOPE ");
                                }
                                break;
                            case "setwarp":
                                if (row.get(s)) {
                                    setwarp.append(ChatColor.GREEN);
                                    setwarp.append("TRUE ");
                                } else {
                                    setwarp.append(ChatColor.RED);
                                    setwarp.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                Table<Policy, String, Boolean> foreignPerms = ClansDatabase.getDatabase().getForeignPerms(clan);

                for (Policy r : foreignPerms.rowKeySet()) {

                    Map<String, Boolean> row = foreignPerms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "desc":
                                if (row.get(s)) {
                                    desc.append(ChatColor.GREEN);
                                    desc.append("TRUE ");
                                } else {
                                    desc.append(ChatColor.RED);
                                    desc.append("NOPE ");
                                }
                                break;
                            case "disband":
                                if (row.get(s)) {
                                    disband.append(ChatColor.GREEN);
                                    disband.append("TRUE ");
                                } else {
                                    disband.append(ChatColor.RED);
                                    disband.append("NOPE ");
                                }
                                break;
                            case "enemy":
                                if (row.get(s)) {
                                    enemy.append(ChatColor.GREEN);
                                    enemy.append("TRUE ");
                                } else {
                                    enemy.append(ChatColor.RED);
                                    enemy.append("NOPE ");
                                }
                                break;
                            case "flag":
                                if (row.get(s)) {
                                    flag.append(ChatColor.GREEN);
                                    flag.append("TRUE ");
                                } else {
                                    flag.append(ChatColor.RED);
                                    flag.append("NOPE ");
                                }
                                break;
                            case "invite":
                                if (row.get(s)) {
                                    invite.append(ChatColor.GREEN);
                                    invite.append("TRUE ");
                                } else {
                                    invite.append(ChatColor.RED);
                                    invite.append("NOPE ");
                                }
                                break;
                            case "title":
                                if (row.get(s)) {
                                    title.append(ChatColor.GREEN);
                                    title.append("TRUE ");
                                } else {
                                    title.append(ChatColor.RED);
                                    title.append("NOPE ");
                                }
                                break;
                            case "warp":
                                if (row.get(s)) {
                                    warp.append(ChatColor.GREEN);
                                    warp.append("TRUE ");
                                } else {
                                    warp.append(ChatColor.RED);
                                    warp.append("NOPE ");
                                }
                                break;
                            case "setwarp":
                                if (row.get(s)) {
                                    setwarp.append(ChatColor.GREEN);
                                    setwarp.append("TRUE ");
                                } else {
                                    setwarp.append(ChatColor.RED);
                                    setwarp.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                sender.sendMessage(ChatColor.YELLOW + "=====[ " + ChatColor.RESET + "Permissions for clan "
                        + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                        + ClansDatabase.getDatabase().getPolicy(clan, ClansDatabase.getDatabase().getClan((Player) sender)))
                        + clan + ChatColor.RESET + String.format(" (Page %d/%d)", page, numPages) + ChatColor.YELLOW + " ]=====");

                sender.sendMessage(header);
                sender.sendMessage(desc.substring(0, desc.length() - 1));
                sender.sendMessage(disband.substring(0, disband.length() - 1));
                sender.sendMessage(enemy.substring(0, enemy.length() - 1));
                sender.sendMessage(flag.substring(0, flag.length() - 1));
                sender.sendMessage(invite.substring(0, invite.length() - 1));
                sender.sendMessage(title.substring(0, title.length() - 1));
                sender.sendMessage(warp.substring(0, warp.length() - 1));
                sender.sendMessage(setwarp.substring(0, setwarp.length() - 1));

            } else if (page == 3) {

                StringBuilder delhome = new StringBuilder(ChatColor.YELLOW + "DELHOME " + ChatColor.BOLD + "  ");

                for (Rank r : perms.rowKeySet()) {

                    Map<String, Boolean> row = perms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "delhome":
                                if (row.get(s)) {
                                    delhome.append(ChatColor.GREEN);
                                    delhome.append("TRUE ");
                                } else {
                                    delhome.append(ChatColor.RED);
                                    delhome.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                Table<Policy, String, Boolean> foreignPerms = ClansDatabase.getDatabase().getForeignPerms(clan);

                for (Policy r : foreignPerms.rowKeySet()) {

                    Map<String, Boolean> row = foreignPerms.row(r);

                    for (String s : row.keySet()) {

                        switch (s) {
                            case "delhome":
                                if (row.get(s)) {
                                    delhome.append(ChatColor.GREEN);
                                    delhome.append("TRUE ");
                                } else {
                                    delhome.append(ChatColor.RED);
                                    delhome.append("NOPE ");
                                }
                                break;
                        }
                    }
                }

                sender.sendMessage(ChatColor.YELLOW + "=====[ " + ChatColor.RESET + "Permissions for clan "
                        + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                        + ClansDatabase.getDatabase().getPolicy(clan, ClansDatabase.getDatabase().getClan((Player) sender)))
                        + clan + ChatColor.RESET + String.format(" (Page %d/%d)", page, numPages) + ChatColor.YELLOW + " ]=====");

                sender.sendMessage(header);
                sender.sendMessage(delhome.substring(0, delhome.length() - 1));

            }

        } catch (DatabaseException e) {

            e.printStackTrace();

        }

    }

    private void setPerm(CommandSender sender, String clan, Rank rank, String permission, boolean value) throws DatabaseException {

        if (PermissionManager.getInstance().canUse(sender, "perm", clan)) {

            switch (permission.toLowerCase()) {

                case "access":
                case "claim":
                case "home":
                case "sethome":
                case "unclaim":
                case "ally":
                case "promote":
                case "truce":
                case "name":
                case "neutral":
                case "perm":
                case "kick":
                case "leader":
                case "demote":
                case "desc":
                case "disband":
                case "enemy":
                case "flag":
                case "invite":
                case "title":
                case "warp":
                case "setwarp":
                case "delhome":

                    ClansDatabase.getDatabase().setClanPermission(clan, rank, permission, value);

                    sender.sendMessage(ChatColor.YELLOW + "Permission " + ChatColor.WHITE + permission.toUpperCase() + ChatColor.YELLOW + " set to "
                            + (value ? ChatColor.GREEN + "TRUE" : ChatColor.RED + "NOPE") + ChatColor.YELLOW + " for rank " + ChatColor.RESET + rank.name()
                            + ChatColor.YELLOW + " in clan " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                            + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan)) + clan);

                    break;

                default:

                    sender.sendMessage(ChatColor.RED + "Invalid permission type. See " + ChatColor.RESET + "/c perm " + ChatColor.RED + "for a list of permissions.");

                    break;

            }

        } else {

            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan))
                    + clan + ChatColor.RED + " does not allow you change clan permissions.");

            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

        }

    }

    private void setPerm(CommandSender sender, String clan, Policy policy, String permission, boolean value) throws DatabaseException {

        if (PermissionManager.getInstance().canUse(sender, "perm", clan)) {

            switch (permission.toLowerCase()) {

                case "access":
                case "claim":
                case "home":
                case "sethome":
                case "unclaim":
                case "ally":
                case "promote":
                case "truce":
                case "name":
                case "neutral":
                case "perm":
                case "kick":
                case "leader":
                case "demote":
                case "desc":
                case "disband":
                case "enemy":
                case "flag":
                case "invite":
                case "title":
                case "warp":
                case "setwarp":
                case "delhome":

                    ClansDatabase.getDatabase().setClanPermission(clan, policy, permission, value);

                    sender.sendMessage(ChatColor.YELLOW + "Permission " + ChatColor.WHITE + permission.toUpperCase() + ChatColor.YELLOW + " set to "
                            + (value ? ChatColor.GREEN + "TRUE" : ChatColor.RED + "NOPE") + ChatColor.YELLOW + " for policy " + ChatColor.RESET + policy.name()
                            + ChatColor.YELLOW + " in clan " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                            + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan)) + clan);

                    break;

                default:

                    sender.sendMessage(ChatColor.RED + "Invalid permission type. See " + ChatColor.RESET + "/c perm " + ChatColor.RED + "for a list of permissions.");

                    break;

            }

        } else {

            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan))
                    + clan + ChatColor.RED + " does not allow you change clan permissions.");

            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

        }

    }

}