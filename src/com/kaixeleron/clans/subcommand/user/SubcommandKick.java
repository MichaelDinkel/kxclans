package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandKick implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.kick")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to kick clan members.");
            return;
        }

        if (args.length == 1) {
            sender.sendMessage(ChatColor.YELLOW + "Kick a player from a clan");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " kick <Player> [Clan]");
            return;
        }

        try {

            String clan;

            if (args.length > 2) {

                StringBuilder sb = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    sb.append(args[i]).append(" ");

                }

                clan = sb.substring(0, sb.length() - 1);

            } else {

                clan = ClansDatabase.getDatabase().getClan((Player) sender);

            }

            if (ClansDatabase.getDatabase().clanExists(clan).length() == 0) {

                sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                return;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(clan, ClansDatabase.getDatabase().getClan((Player) sender));

            if (!PermissionManager.getInstance().canUse(sender, "kick", clan)) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to kick members.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

            } else {

                @SuppressWarnings("deprecation")
                OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);

                if (((Player) sender).equals(op.getPlayer())) {

                    sender.sendMessage(ChatColor.YELLOW + "You can't kick youself. Use " + ChatColor.WHITE + "/" + label + " leave " + ChatColor.YELLOW + "instead.");
                    return;

                }

                List<UUID> members = ClansDatabase.getDatabase().getPlayers(clan);

                boolean kicked = false;

                for (UUID u : members) {
                    if (u.equals(op.getUniqueId())) {

                        kicked = true;

                        ClansDatabase.getDatabase().kickPlayer(clan, u);

                        for (UUID id : members) {

                            Player p;

                            if ((p = Bukkit.getPlayer(id)) != null && !p.equals(op.getPlayer()))
                                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) +
                                        sender.getName() + ChatColor.YELLOW + " has kicked " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                        + args[1] + ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                        }

                        if (!clan.equals(ClansDatabase.getDatabase().getClan((Player) sender)))
                            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                    + "You" + ChatColor.YELLOW + " have kicked " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                                    + args[1] + ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);


                        if (op.isOnline()) {

                            op.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) +
                                    sender.getName() + ChatColor.YELLOW + " has kicked " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                    + "You" + ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                        }

                        break;
                    }
                }

                if (!kicked) sender.sendMessage(args[1] + ChatColor.RED + " is not in " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
