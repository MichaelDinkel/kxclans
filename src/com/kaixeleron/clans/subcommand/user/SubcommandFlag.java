package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class SubcommandFlag implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.flag")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to manage clan flags.");
            return;
        }

        try {

            if (args.length == 1) {

                help(sender, label);

            } else if (args.length == 2) {

                if (args[1].equalsIgnoreCase("s") || args[1].equalsIgnoreCase("show")) {

                    showFlags(sender, ClansDatabase.getDatabase().getClan((Player) sender));

                } else {

                    help(sender, label);

                }

            } else {

                if (args[1].equalsIgnoreCase("s") || args[1].equalsIgnoreCase("show")) {

                    StringBuilder clan = new StringBuilder();

                    for (int i = 2; i < args.length; i++) {
                        clan.append(args[i]).append(" ");
                    }

                    clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                    if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {
                        sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan.toString() + ChatColor.RED + " does not exist.");
                        return;
                    }

                    showFlags(sender, clan.toString());

                } else {

                    String clan;

                    if (args.length > 3) {

                        StringBuilder clansb = new StringBuilder();

                        for (int i = 3; i < args.length; i++) {
                            clansb.append(args[i]).append(" ");
                        }

                        clan = clansb.substring(0, clansb.length() - 1);

                    } else {

                        clan = ClansDatabase.getDatabase().getClan((Player) sender);

                    }

                    Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

                    if (!PermissionManager.getInstance().canUse(sender, "flag", clan)) {

                        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to manage flags.");
                        if (sender.hasPermission("kxclans.admin.bypass")) {
                            sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                        }

                        return;
                    }

                    boolean value;

                    switch (args[2].toLowerCase()) {
                        case "true":
                        case "enabled":
                            value = true;
                            break;
                        case "false":
                        case "disabled":
                            value = false;
                            break;
                        default:
                            sender.sendMessage(ChatColor.RED + "Invalid flag value. Must be " + ChatColor.WHITE + "true" + ChatColor.RED + ", "
                                    + ChatColor.WHITE + "enabled" + ChatColor.RED + ", " + ChatColor.WHITE + "false" + ChatColor.RED + ", or "
                                    + ChatColor.WHITE + "disabled" + ChatColor.RED + ".");
                            return;
                    }

                    Flag flag;

                    try {

                        flag = Flag.valueOf(args[1].toUpperCase());

                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(ChatColor.RED + "Unknown flag. Type " + ChatColor.WHITE + "/" + label + " flag s|show " + ChatColor.RED + "to view available flags.");
                        return;
                    }

                    boolean user = ClansMain.getInstance().getConfig().getBoolean("flags." + args[1].toLowerCase() + ".user");

                    if (!user && !PermissionManager.getInstance().isBypassing(sender)) {

                        sender.sendMessage(ChatColor.RED + "The flag " + ChatColor.WHITE + args[1].toLowerCase() + ChatColor.RED + " is read only.");
                        if (sender.hasPermission("kxclans.admin.bypass")) {
                            sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                        }

                        return;
                    }

                    ClansDatabase.getDatabase().setFlag(clan, flag, value);

                    sender.sendMessage(ChatColor.YELLOW + "Flag " + (user ? ChatColor.WHITE : ChatColor.GRAY) + args[1].toLowerCase()
                            + ChatColor.YELLOW + " is now " + (value ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled")
                            + ChatColor.YELLOW + " on " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);
                }
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }

    private void help(CommandSender sender, String label) {

        sender.sendMessage(ChatColor.YELLOW + "Manage clan flags");
        sender.sendMessage(ChatColor.RED + "Usage to set a flag: " + ChatColor.WHITE + "/" + label + " flag <Flag> <Value> [Clan]");
        sender.sendMessage(ChatColor.RED + "Usage to show flags: " + ChatColor.WHITE + "/" + label + " flag s|show [Clan]");

    }

    private void showFlags(CommandSender sender, String clan) throws DatabaseException {

        Map<Flag, Boolean> flags = ClansDatabase.getDatabase().getFlags(clan);

        String equals = "====================";
        String subequals = equals.substring(0, equals.length() - clan.length() / 2);

        sender.sendMessage(ChatColor.RED + subequals + "[ " + ChatColor.WHITE + "Flags for " + ChatColor.COLOR_CHAR
                + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase()
                .getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan).toString()) + clan
                + ChatColor.RED + " ]" + subequals);

        for (Flag f : flags.keySet()) {

            String flag = f.toString();

            boolean user = ClansMain.getInstance().getConfig().getBoolean("flags." + flag.toLowerCase() + ".user");

            if (!user && !ClansMain.getInstance().getConfig().getBoolean("showadminflags") && !PermissionManager.getInstance().isBypassing(sender)) continue;

            StringBuilder message = new StringBuilder(user ? "" : "" + ChatColor.GRAY);
            message.append(flag.substring(0, 1).toUpperCase());
            message.append(flag.substring(1).toLowerCase());
            message.append(ChatColor.YELLOW);
            message.append(": ");
            message.append(flags.get(f) ? ChatColor.GREEN + "Enabled" : ChatColor.RED + "Disabled");

            switch (f) {
                case ENDERGRIEF:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Endermen ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" pick up blocks");
                    break;
                case EXPLOSIONS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Explosions ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" occur");
                    break;
                case FIRESPREAD:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Fire ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spread");
                    break;
                case FRIENDLYFIRE:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Members/allies ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" damage eachother");
                    break;
                case INFPOWER:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "has" : "doesn't have");
                    message.append(" infinite power");
                    break;
                case MONSTERS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Monsters ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spawn");
                    break;
                case OPEN:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Players ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" freely join");
                    break;
                case PEACEFUL:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "is" : "isn't");
                    message.append(" allies with everybody");
                    break;
                case PERMANENT:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "will" : "won't");
                    message.append(" remain with 0 members");
                    break;
                case POWERLOSS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Power ");
                    message.append(flags.get(f) ? "will" : "won't");
                    message.append(" be lost on death");
                    break;
                case PVP:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Players ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" damage eachother");
                    break;
                case ANIMALS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Animals ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spawn");
            }

            sender.sendMessage(message.toString());
        }
    }
}
