package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Particle;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class SubcommandShowChunk implements Subcommand {

    private List<Player> showing = new ArrayList<>();

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.showchunk")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to visualize chunks.");
            return;
        }

        if (showing.contains((Player) sender)) {

            showing.remove((Player) sender);

            sender.sendMessage(ChatColor.YELLOW + "Visualizing chunks " + ChatColor.RED + "disabled");

        } else {

            showing.add((Player) sender);

            sender.sendMessage(ChatColor.YELLOW + "Visualizing chunks " + ChatColor.GREEN + "enabled");

        }
    }

    public SubcommandShowChunk() {

        new BukkitRunnable() {

            @Override
            public void run() {

                for (Player p : showing) {

                    Chunk c = p.getLocation().getChunk();

                    for (int i = 0; i < 255; i++) {

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(0, i, 0).getLocation(), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(0, i, 0).getLocation().add(0, 0.5, 0), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(8, i, 0).getLocation().add(0.5, 0, 0), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(8, i, 0).getLocation().add(0.5, 0.5, 0), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(15, i, 0).getLocation().add(1, 0, 0), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(15, i, 0).getLocation().add(1, 0.5, 0), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(0, i, 8).getLocation().add(0, 0, 0.5), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(0, i, 8).getLocation().add(0, 0.5, 0.5), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(15, i, 8).getLocation().add(1, 0, 0.5), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(15, i, 8).getLocation().add(1, 0.5, 0.5), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(0, i, 15).getLocation().add(0, 0, 1), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(0, i, 15).getLocation().add(0, 0.5, 1), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(8, i, 15).getLocation().add(0.5, 0, 1), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(8, i, 15).getLocation().add(0.5, 0.5, 1), 0);

                        p.spawnParticle(Particle.VILLAGER_HAPPY, c.getBlock(15, i, 15).getLocation().add(1, 0, 1), 0);
                        p.spawnParticle(Particle.SPELL_WITCH, c.getBlock(15, i, 15).getLocation().add(1, 0.5, 1), 0);

                    }
                }
            }
        }.runTaskTimer(ClansMain.getInstance(), 10, 10);
    }
}
