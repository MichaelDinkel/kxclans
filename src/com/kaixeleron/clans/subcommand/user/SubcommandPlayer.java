package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;

public class SubcommandPlayer implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.player")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to view player info.");
            return;
        }

        if (args.length == 1) {

            showPlayer(sender, sender.getName());

        } else {

            showPlayer(sender, args[1]);

        }
    }

    private void showPlayer(CommandSender sender, String target) {

        try {

            @SuppressWarnings("deprecation")
            OfflinePlayer op = Bukkit.getOfflinePlayer(target);

            BigDecimal powerdecimal = new BigDecimal(ClansDatabase.getDatabase().getPlayerPower(op.getUniqueId()));
            powerdecimal = powerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();

            String equals = "======================";

            equals = equals.substring(0, equals.length() - target.length() / 2);

            String targetclan = ClansDatabase.getDatabase().getClan(op.getUniqueId());

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase
                            .getDatabase().getClan(((Player) sender).getUniqueId()), targetclan);

            sender.sendMessage(ChatColor.YELLOW + equals + "[ " + ChatColor.WHITE + "Player " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                    .getString("colors." + rel.toString()) + target + ChatColor.YELLOW + " ]" + equals);

            sender.sendMessage(ChatColor.YELLOW + "Clan: " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                    .getString("colors." + rel.toString()) + targetclan);
            sender.sendMessage(ChatColor.YELLOW + "Power: " + ChatColor.WHITE + powerdecimal.toPlainString());
            sender.sendMessage(ChatColor.YELLOW + "Power per hour: " + ChatColor.WHITE + ClansMain.getInstance().getConfig().getDouble("powerperhour"));
            if (ClansMain.getInstance().getConfig().getBoolean("deathpowerloss.enabled")) sender.sendMessage(ChatColor.YELLOW + "Power lost on death: "
                    + ChatColor.WHITE + ClansMain.getInstance().getConfig().getDouble("deathpowerloss.amount"));

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
