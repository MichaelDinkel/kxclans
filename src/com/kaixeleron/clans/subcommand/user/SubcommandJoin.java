package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SubcommandJoin implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.join")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to join clans.");
            return;
        }

        try {

            if (!ClansDatabase.getDatabase().getClan((Player) sender).equals(ClansDatabase.getUnclaimedName())) {
                sender.sendMessage(ChatColor.RED + "You are already in a clan.");
                return;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.YELLOW + "Join a clan");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " join <Clan>");
                return;
            }

            String clan;

            StringBuilder clansb = new StringBuilder();

            for (int i = 1; i < args.length; i++) {
                clansb.append(args[i]).append(" ");
            }

            clan = clansb.substring(0, clansb.length() - 1);

            if (ClansDatabase.getDatabase().clanExists(clan).length() == 0) {
                sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");
                return;
            }

            boolean invited = false;

            for (String s : ClansDatabase.getDatabase().getInvites(((Player) sender).getUniqueId())) {
                if (s.equalsIgnoreCase(clan)) {
                    invited = true;
                    break;
                }
            }

            if (ClansDatabase.getDatabase().getFlag(clan, Flag.OPEN) || invited || PermissionManager.getInstance().isBypassing(sender)) {

                ClansDatabase.getDatabase().removeInvite(((Player) sender).getUniqueId(), clan);

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                        + "You" + ChatColor.YELLOW + " have joined " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                for (UUID id : ClansDatabase.getDatabase().getPlayers(clan)) {

                    Player p;

                    if ((p = Bukkit.getPlayer(id)) != null)
                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                + sender.getName() + ChatColor.YELLOW + " has joined " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                }

                ClansDatabase.getDatabase().joinClan(clan, ((Player) sender).getUniqueId());

            } else {

                sender.sendMessage(clan + ChatColor.RED + " requires invitation to join.");
                if (sender.hasPermission("kxclans.admin.bypass")) {
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                }

            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database exception occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
