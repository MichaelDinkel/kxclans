package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandAccess implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.access")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to manage land access.");
            return;
        }

        try {

            if (args.length == 1) {

                showHelp(sender, label);

            } else if (args.length == 2 || args.length == 3) {

                if (args[1].equalsIgnoreCase("v")) {
                    view(sender);
                } else {
                    showHelp(sender, label);
                }

            } else {

                boolean value;

                switch (args[3].toLowerCase()) {
                    case "true":
                    case "enabled":
                        value = true;
                        break;
                    case "false":
                    case "disabled":
                        value = false;
                        break;
                    default:
                        sender.sendMessage(ChatColor.RED + "Invalid access value. Must be " + ChatColor.WHITE + "true" + ChatColor.RED + ", "
                                + ChatColor.WHITE + "enabled" + ChatColor.RED + ", " + ChatColor.WHITE + "false" + ChatColor.RED + ", or "
                                + ChatColor.WHITE + "disabled" + ChatColor.RED + ".");
                        return;
                }

                switch(args[1].toLowerCase()) {
                    case "v":
                    case "view":
                        view(sender);
                        break;
                    case "c":
                    case "clan":
                        setClan(sender, args[2], value);
                        break;
                    case "p":
                    case "player":
                        setPlayer(sender, args[2], value);
                        break;
                    default:
                        showHelp(sender, label);
                        break;
                }
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }

    private void showHelp(CommandSender sender, String label) {

        sender.sendMessage(ChatColor.YELLOW + "Manage land access");
        sender.sendMessage(ChatColor.RED + "Usage to view access: " + ChatColor.WHITE + "/" + label + " access v|view");
        sender.sendMessage(ChatColor.RED + "Usage to set clan access: " + ChatColor.WHITE + "/" + label + " access c|clan <Clan> <Value>");
        sender.sendMessage(ChatColor.RED + "Usage to set player access: " + ChatColor.WHITE + "/" + label + " access p|player <Clan> <Value>");

    }

    private void view(CommandSender sender) throws DatabaseException {

        Chunk c = ((Player) sender).getLocation().getChunk();

        String owner = ClansDatabase.getDatabase().getChunkOwner(c);

        if (owner.equals(ClansDatabase.getUnclaimedName()) || owner.equals(ClansDatabase.getWarName()) || owner.equals(ClansDatabase.getSafeName())) {
            sender.sendMessage(owner + ChatColor.RED + " is a system clan and cannot have access settings.");
            return;
        }

        String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

        List<String> clans = ClansDatabase.getDatabase().getClanAccess(owner, c), players = ClansDatabase.getDatabase().getPlayerAccess(owner, c);

        String equals = "================";

        int length = (owner.length() + Integer.toString(c.getX()).length() + Integer.toString(c.getZ()).length()) / 2;

        equals = equals.substring(0, equals.length() - length);

        sender.sendMessage(ChatColor.RED + equals + "[ " + ChatColor.WHITE + "Access for " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                .getString("colors." + ClansDatabase.getDatabase().getPolicy(owner, senderclan).toString()) + owner + ChatColor.WHITE + " chunk " + c.getX()
                        + ChatColor.GOLD + "," + ChatColor.WHITE + c.getZ() + ChatColor.RED + " ]" + equals);

        sender.sendMessage("Clans (" + clans.size() + "):");
        for (String s : clans) {
            sender.sendMessage(" - " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(s, senderclan)) + s);
        }

        sender.sendMessage("Players (" + players.size() + "):");
        for (String s : players) {
            sender.sendMessage(" - " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                    + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(UUID.fromString(s)), senderclan))
                    + Bukkit.getOfflinePlayer(UUID.fromString(s)).getName());
        }
    }

    private void setClan(CommandSender sender, String target, boolean value) throws DatabaseException {

        Chunk c = ((Player) sender).getLocation().getChunk();

        String owner = ClansDatabase.getDatabase().getChunkOwner(c);

        if (owner.equals(ClansDatabase.getUnclaimedName()) || owner.equals(ClansDatabase.getWarName()) || owner.equals(ClansDatabase.getSafeName())) {
            sender.sendMessage(owner + ChatColor.RED + " is a system clan and cannot have access settings.");
            return;
        }

        String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

        Policy rel = ClansDatabase.getDatabase().getPolicy(senderclan, owner);

        if (!PermissionManager.getInstance().canUse(sender, "access", owner)) {
            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + owner + ChatColor.RED + " does not allow you to manage land access.");
            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
            return;
        }

        if (ClansDatabase.getDatabase().clanExists(target).length() == 0) {
            sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + target + ChatColor.RED + " does not exist.");
            return;
        }

        if (value == ClansDatabase.getDatabase().getAccess(owner, c, target)) {
            sender.sendMessage(ChatColor.RED + "The access change specified is already set.");
            return;
        }

        ClansDatabase.getDatabase().setAccess(owner, c, target, value);

        List<UUID> members = ClansDatabase.getDatabase().getPlayers(owner);

        for (UUID id : members) {

            Player p = Bukkit.getPlayer(id);

            if (p != null && !p.equals(sender)) {

                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                        + sender.getName() + ChatColor.YELLOW + " has " + (value ? "granted" : "revoked") + " access to chunk " + ChatColor.WHITE + c.getX() + ","
                        + c.getZ() + ChatColor.YELLOW + " " + (value ? "to" : "from") + " clan " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                        .getString("colors." + ClansDatabase.getDatabase().getPolicy(owner, target).toString()) + target);

            }
        }

        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                + "You" + ChatColor.YELLOW + " have " + (value ? "granted" : "revoked") + " access to chunk " + ChatColor.WHITE + c.getX() + ","
                + c.getZ() + ChatColor.YELLOW + " " + (value ? "to" : "from") + " clan " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                .getString("colors." + ClansDatabase.getDatabase().getPolicy(senderclan, target).toString()) + target);
    }

    private void setPlayer(CommandSender sender, String target, boolean value) throws DatabaseException {

        Chunk c = ((Player) sender).getLocation().getChunk();

        String owner = ClansDatabase.getDatabase().getChunkOwner(c);

        if (owner.equals(ClansDatabase.getUnclaimedName()) || owner.equals(ClansDatabase.getWarName()) || owner.equals(ClansDatabase.getSafeName())) {
            sender.sendMessage(owner + ChatColor.RED + " is a system clan and cannot have access settings.");
            return;
        }

        String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

        Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), owner);

        if (!PermissionManager.getInstance().canUse(sender, "access", owner)) {
            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + owner + ChatColor.RED + " does not allow you to manage land access.");
            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
            return;
        }

        @SuppressWarnings("deprecation")
        OfflinePlayer o = Bukkit.getOfflinePlayer(target);

        if (value == ClansDatabase.getDatabase().getAccess(owner, c, o.getUniqueId().toString())) {
            sender.sendMessage(ChatColor.RED + "The access change specified is already set.");
            return;
        }
        ClansDatabase.getDatabase().setAccess(owner, c, o.getUniqueId().toString(), value);

        List<UUID> members = ClansDatabase.getDatabase().getPlayers(owner);

        for (UUID id : members) {

            Player p = Bukkit.getPlayer(id);

            if (p != null && !p.equals(sender)) {

                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                        + sender.getName() + ChatColor.YELLOW + " has " + (value ? "granted" : "revoked") + " access to chunk " + ChatColor.WHITE + c.getX() + ","
                        + c.getZ() + ChatColor.YELLOW + " " + (value ? "to" : "from") + " player " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                        .getString("colors." + ClansDatabase.getDatabase().getPolicy(owner, target).toString()) + target);

            }
        }

        sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                + "You" + ChatColor.YELLOW + " have " + (value ? "granted" : "revoked") + " access to chunk " + ChatColor.WHITE + c.getX() + ","
                + c.getZ() + ChatColor.YELLOW + " " + (value ? "to" : "from") + " player " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                .getString("colors." + ClansDatabase.getDatabase().getPolicy(senderclan, target).toString()) + target);
    }
}
