package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandUnclaim implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.unclaim")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to unclaim land.");
            return;
        }

        try {

            Chunk c = ((Player) sender).getLocation().getChunk();

            String clan = ClansDatabase.getDatabase().getChunkOwner(c);

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

            if (clan.equals(ClansDatabase.getSafeName()) || clan.equals(ClansDatabase.getWarName())) {

                sender.sendMessage(ChatColor.RED + "This land is owned by a system clan and cannot be unclaimed.");
                if (sender.hasPermission("kxclans.admin.unclaimsys"))
                    sender.sendMessage(ChatColor.YELLOW + "You may unclaim it with " + ChatColor.GREEN + "/cadmin unclaimsys");
                return;

            }

            if (clan.equals(ClansDatabase.getUnclaimedName())) {
                sender.sendMessage(ChatColor.RED + "This land is unowned.");
                return;
            }

            if (!PermissionManager.getInstance().canUse(sender, "unclaim", clan)) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to unclaim land.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            List<UUID> players = ClansDatabase.getDatabase().getPlayers(clan);

            for (UUID u : players) {

                Player p = Bukkit.getPlayer(u);

                if (p != null) {

                    p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + sender.getName() + ChatColor.YELLOW + " has unclaimed chunk "
                            + ChatColor.WHITE + c.getX() + "," + c.getZ() + ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                }
            }

            String senderclan = ClansDatabase.getDatabase().getClan((Player) sender);

            if (!senderclan.equals(clan)) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName() + ChatColor.YELLOW + " has unclaimed chunk "
                        + ChatColor.WHITE + c.getX() + "," + c.getZ() + ChatColor.YELLOW + " from " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                        + ClansDatabase.getDatabase().getPolicy(senderclan, clan)) + clan);
            }

            Location l = ClansDatabase.getDatabase().getHome(clan);

            if (l != null && l.getChunk().equals(c)) {
                ClansDatabase.getDatabase().setHome(clan, null);
            }

            ClansDatabase.getDatabase().unclaim(clan, c);

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}