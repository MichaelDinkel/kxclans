package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.TeleportTimer;
import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class SubcommandWarp implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.warp")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to teleport to a clan warp.");
            return;
        }

        try {

            StringBuilder clan = new StringBuilder(ClansDatabase.getDatabase().getClan((Player) sender));

            if (args.length > 2) {

                clan = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                    sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                    return;
                }
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan.toString());

            if (!PermissionManager.getInstance().canUse(sender, "warp", clan.toString())) {
                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to teleport to its warps.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                return;
            }

            if (args.length < 2) {

                List<String> warps = ClansDatabase.getDatabase().getWarps(clan.toString());

                sender.sendMessage(ChatColor.YELLOW + "Warps set by " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.YELLOW + ":");

                for (String s : warps) {
                    sender.sendMessage(ChatColor.GOLD + " - " + ChatColor.WHITE + s);
                }

                return;
            }

            Location warp = ClansDatabase.getDatabase().getWarp(clan.toString(), args[1]);

            if (warp == null) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not have a warp named " + ChatColor.WHITE + args[1] + ChatColor.RED + " set.");

                return;
            }

            int time = ClansMain.getInstance().getConfig().getInt("tpdelay");

            if (time > 0 && !PermissionManager.getInstance().isBypassing(sender)) {
                TeleportTimer.getInstance().teleport((Player) sender, warp);
                sender.sendMessage(ChatColor.YELLOW + "Teleporting to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                        + clan + ChatColor.YELLOW + "'s warp " + ChatColor.WHITE + args[1] + ChatColor.YELLOW + " in " + ChatColor.WHITE + time + ChatColor.YELLOW + " seconds. Don't move.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Teleporting to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                        + clan + ChatColor.YELLOW + "'s warp " + ChatColor.WHITE + args[1]);
                ((Player) sender).teleport(warp);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}