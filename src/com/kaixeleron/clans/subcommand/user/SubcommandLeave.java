package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SubcommandLeave implements Subcommand {

    public SubcommandLeave() {
        leaving = new HashMap<>();
    }

    private Map<Player, BukkitRunnable> leaving;

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.leave")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to leave clans.");
            return;
        }

        if (!PermissionManager.getInstance().canUse(sender, "leave", "")) {
            sender.sendMessage(ChatColor.RED + "You are not in a clan.");
            return;
        }

        try {

            String clan = ClansDatabase.getDatabase().getClan((Player) sender);

            if (leaving.containsKey((Player) sender)) {

                leaving.get((Player) sender).cancel();
                leaving.remove((Player) sender);

                ClansDatabase.getDatabase().kickPlayer(clan, ((Player) sender).getUniqueId());

                for (UUID id : ClansDatabase.getDatabase().getPlayers(clan)) {

                    Player p;

                    if ((p = Bukkit.getPlayer(id)) != null)
                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + sender.getName()
                                + ChatColor.YELLOW + " has left " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                }

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "You"
                        + ChatColor.YELLOW + " have left " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

            } else {

                sender.sendMessage(ChatColor.YELLOW + "Are you sure that you want to " + ChatColor.DARK_RED + ChatColor.BOLD
                        + "LEAVE" + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + " " + clan + ChatColor.YELLOW + "?");
                sender.sendMessage(ChatColor.YELLOW + "Type " + ChatColor.WHITE + "/" + label + " leave " + ChatColor.YELLOW + "again within 15 seconds to confirm.");

                BukkitRunnable r = new BukkitRunnable() {

                    @Override
                    public void run() {

                        leaving.remove((Player) sender);
                        sender.sendMessage(ChatColor.YELLOW + "Clan leave request expired.");

                    }
                };

                r.runTaskLater(ClansMain.getInstance(), 300);

                leaving.put((Player) sender, r);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
