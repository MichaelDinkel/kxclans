package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandTitle implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.title")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to manage clan member titles.");
            return;
        }

        try {

            if (args.length < 2) {

                sender.sendMessage(ChatColor.YELLOW + "Manage clan member titles");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " title <Player> [Title]");

            } else if (args.length == 2) {

                @SuppressWarnings("deprecation")
                OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
                UUID u = op.getUniqueId();

                String clan = ClansDatabase.getDatabase().getClan(u);
                Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

                if (!PermissionManager.getInstance().canUse(sender, "title", clan)) {

                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to manage member titles.");
                    if (sender.hasPermission("kxclans.admin.bypass"))
                        sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                    return;

                }

                ClansDatabase.getDatabase().setPlayerTitle(u, "");

                List<UUID> members = ClansDatabase.getDatabase().getPlayers(clan);

                for (UUID uuid : members) {

                    if (uuid.equals(((Player) sender).getUniqueId())) continue;

                    Player p = Bukkit.getPlayer(uuid);

                    if (p != null) p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                            + sender.getName() + ChatColor.YELLOW + " has removed the title of " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                            .getString("colors.CLAN") + args[1]);

                }

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                        + "You" + ChatColor.YELLOW + " have removed the title of " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                        .getString("colors." + rel.toString()) + args[1]);

            } else {

                @SuppressWarnings("deprecation")
                OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
                UUID u = op.getUniqueId();

                String clan = ClansDatabase.getDatabase().getClan(u);
                Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

                if (!PermissionManager.getInstance().canUse(sender, "title", clan)) {

                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to manage member titles.");
                    if (sender.hasPermission("kxclans.admin.bypass"))
                        sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                    return;

                }

                StringBuilder titleb = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    titleb.append(args[i]).append(" ");

                }

                String title = titleb.substring(0, titleb.length() - 1);

                if (!title.matches("^[a-zA-Z0-9&_+ \\[\\]{}()-]*$")) {

                    sender.sendMessage(ChatColor.RED + "The title " + ChatColor.WHITE + title + ChatColor.RED + " contains invalid characters. Titles may only contain letters, numbers, spaces, dashes, brackets, and ampersands.");

                    return;

                }

                if (title.length() > 16) {

                    sender.sendMessage(ChatColor.RED + "The title " + ChatColor.WHITE + title + ChatColor.RED + " is too long. Titles can be at most 16 characters.");

                    return;

                }

                title = ChatColor.translateAlternateColorCodes('&', title);

                ClansDatabase.getDatabase().setPlayerTitle(u, title);

                List<UUID> members = ClansDatabase.getDatabase().getPlayers(clan);

                for (UUID uuid : members) {

                    if (uuid.equals(((Player) sender).getUniqueId())) continue;

                    Player p = Bukkit.getPlayer(uuid);

                    if (p != null) p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                            + sender.getName() + ChatColor.YELLOW + " has set the title of " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                            .getString("colors.CLAN") + args[1] + ChatColor.YELLOW + " to " + ChatColor.WHITE + title);

                }

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                        + "You" + ChatColor.YELLOW + " have set the title of " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                        .getString("colors." + rel.toString()) + args[1] + ChatColor.YELLOW + " to " + ChatColor.WHITE + title);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
