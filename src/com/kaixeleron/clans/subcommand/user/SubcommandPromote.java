package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.Rank;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandPromote implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.promote")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to promote clan members.");
            return;
        }

        if (args.length == 1) {
            sender.sendMessage(ChatColor.YELLOW + "Promote a player within a clan");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " promote <Player> [Clan]");
            return;
        }

        try {

            String clan;

            if (args.length > 2) {

                StringBuilder sb = new StringBuilder();

                for (int i = 2; i < args.length; i++) {

                    sb.append(args[i]).append(" ");

                }

                clan = sb.substring(0, sb.length() - 1);

            } else {

                clan = ClansDatabase.getDatabase().getClan((Player) sender);

            }

            if (ClansDatabase.getDatabase().clanExists(clan).length() == 0) {

                sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                return;
            }

            Policy rel = ClansDatabase.getDatabase().getPolicy(clan, ClansDatabase.getDatabase().getClan((Player) sender));

            if (!PermissionManager.getInstance().canUse(sender, "promote", clan)) {

                sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan + ChatColor.RED + " does not allow you to promote members.");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

            } else {

                @SuppressWarnings("deprecation")
                OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);

                List<UUID> members = ClansDatabase.getDatabase().getPlayers(clan);

                boolean promoted = false;

                for (UUID u : members) {
                    if (u.equals(op.getUniqueId())) {

                        promoted = true;

                        Rank r = ClansDatabase.getDatabase().getClanRank(u, clan), newrank = Rank.NEWBIE;

                        switch (r) {
                            case LEADER:

                                sender.sendMessage(ChatColor.RED + "The clan leader cannot be promoted.");

                                return;
                            case CAPTAIN:

                                sender.sendMessage(ChatColor.RED + "Captains cannot be promoted. Use " + ChatColor.WHITE + "/" + label + " leader " + args[1] + ChatColor.RED + " instead.");

                                return;
                            case MEMBER:

                                newrank = Rank.CAPTAIN;

                                break;
                            case NEWBIE:

                                newrank = Rank.MEMBER;

                                break;
                            default:
                                return;
                        }

                        ClansDatabase.getDatabase().setRank(u, newrank);

                        for (UUID id : members) {

                            Player p;

                            if ((p = Bukkit.getPlayer(id)) != null && !p.equals(op.getPlayer()))
                                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) +
                                        sender.getName() + ChatColor.YELLOW + " has promoted " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                        + args[1] + ChatColor.YELLOW + " to " + newrank.toString().toLowerCase() + " in " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                        }

                        if (!clan.equals(ClansDatabase.getDatabase().getClan((Player) sender)))
                            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                    + "You" + ChatColor.YELLOW + " have promoted " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                                    + args[1] + ChatColor.YELLOW + " to " + newrank.toString().toLowerCase() + " in " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);


                        if (op.isOnline()) {

                            op.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) +
                                    sender.getName() + ChatColor.YELLOW + " has promoted " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                                    + "You" + ChatColor.YELLOW + " to " + newrank.toString().toLowerCase() + " in " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);

                        }

                        break;
                    }
                }

                if (!promoted) sender.sendMessage(args[1] + ChatColor.RED + " is not in " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + clan);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
