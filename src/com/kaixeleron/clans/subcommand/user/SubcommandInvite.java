package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class SubcommandInvite implements Subcommand {

    private final String[] help = new String[]{ChatColor.RED + "====================[ " + ChatColor.WHITE + "Clan Invites " + ChatColor.RED + "]====================",
            ChatColor.YELLOW + "/%label %canuseinvite " + ChatColor.YELLOW + "p " + ChatColor.GOLD + "<Player> [Clan] " + ChatColor.GREEN + "- Invite a player to a clan",
            ChatColor.YELLOW + "/%label %canuseinvite " + ChatColor.YELLOW + "c " + ChatColor.GOLD + "[Clan] " + ChatColor.GREEN + "- Show active clan invites",
            ChatColor.YELLOW + "/%label invite l " + ChatColor.GREEN + "- Show your clan invites"};

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.invite")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to view or manage clan invites.");
            return;
        }

        try {

            if (args.length == 1) {
                showHelp(sender, label);
            } else if (args.length == 2) {

                switch (args[1]) {

                    case "c":

                        clanInvites(sender, ClansDatabase.getDatabase().getClan((Player) sender));

                        break;
                    case "l":

                        playerInvites(sender);

                        break;
                    default:
                        showHelp(sender, label);
                        break;
                }

            } else if (args.length == 3) {

                switch (args[1]) {

                    case "c":

                        clanInvites(sender, args[2]);

                        break;
                    case "l":

                        playerInvites(sender);

                        break;
                    case "p":

                        invite(sender, ClansDatabase.getDatabase().getClan((Player) sender), args[2]);

                        break;
                    default:
                        showHelp(sender, label);
                        break;
                }

            } else {

                switch (args[1]) {

                    case "c":

                        StringBuilder clan = new StringBuilder();

                        for (int i = 2; i < args.length; i++) {

                            clan.append(args[i]).append(" ");

                        }

                        clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                        if (ClansDatabase.getDatabase().clanExists(clan.toString()).length() == 0) {

                            sender.sendMessage(ChatColor.RED + "The clan " + ChatColor.WHITE + clan + ChatColor.RED + " does not exist.");

                            return;
                        }

                        clanInvites(sender, clan.toString());

                        break;
                    case "l":

                        playerInvites(sender);

                        break;
                    case "p":

                        StringBuilder clan2 = new StringBuilder();

                        for (int i = 3; i < args.length; i++) {

                            clan2.append(args[i]).append(" ");

                        }

                        clan2 = new StringBuilder(clan2.substring(0, clan2.length() - 1));

                        invite(sender, clan2.toString(), args[2]);

                        break;
                    default:
                        showHelp(sender, label);
                        break;
                }

            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }

    private void showHelp(CommandSender sender, String label) throws DatabaseException {
        for (String s : help) {
            sender.sendMessage(s.replace("%canuse", PermissionManager.getInstance().canUse(sender, "invite",
                    ClansDatabase.getDatabase().getClan((Player) sender)) ? ChatColor.YELLOW + "" : ChatColor.RED + "").replace("%label", label));
        }
    }

    private void clanInvites(CommandSender sender, String clan) throws DatabaseException {

        if (!PermissionManager.getInstance().canUse(sender, "invite", clan)) {

            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                    + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan)
                    .toString()) + clan + ChatColor.RED + " does not allow you to list invites.");
            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

        } else {

            List<UUID> invites = ClansDatabase.getDatabase().getClanInvites(clan);

            String equals = "=================";
            String subequals = equals.substring(0, equals.length() - clan.length() / 2);

            sender.sendMessage(ChatColor.RED + subequals + "[ " + ChatColor.WHITE + "Players invited to "
                    + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                    + clan + ChatColor.RED + " ]" + subequals);

            for (UUID u : invites) {

                String name = Bukkit.getOfflinePlayer(u).getName();

                sender.sendMessage(name == null ? u.toString() : name);

            }

        }
    }

    private void playerInvites(CommandSender sender) throws DatabaseException {

        List<String> invites = ClansDatabase.getDatabase().getInvites(((Player) sender).getUniqueId());

        sender.sendMessage(ChatColor.RED + "====================[ " + ChatColor.WHITE + "Your invites" + ChatColor.RED + " ]====================");

        for (String s : invites) {

            sender.sendMessage(s);

        }
    }

    private void invite(CommandSender sender, String clan, String target) throws DatabaseException {

        if (!PermissionManager.getInstance().canUse(sender, "invite", clan)) {

            sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                    + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan)
                    .toString()) + clan + ChatColor.RED + " does not allow you to manage invites.");
            if (sender.hasPermission("kxclans.admin.bypass"))
                sender.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");

        } else {

            @SuppressWarnings("deprecation")
            OfflinePlayer op = Bukkit.getOfflinePlayer(target);
            UUID u = op.getUniqueId();

            Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

            if (ClansDatabase.getDatabase().getClanInvites(clan).contains(u)) {

                ClansDatabase.getDatabase().removeInvite(u, clan);

                for (UUID id : ClansDatabase.getDatabase().getPlayers(clan)) {

                    Player p;

                    if ((p = Bukkit.getPlayer(id)) != null)
                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                                + sender.getName() + ChatColor.YELLOW + " has revoked the invitation of " + ChatColor.WHITE + target + ChatColor.YELLOW + " to "
                                + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                }

                if (!clan.equals(ClansDatabase.getDatabase().getClan((Player) sender)))
                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                            + "You" + ChatColor.YELLOW + " have revoked the invitation of " + ChatColor.COLOR_CHAR
                            + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target
                            + ChatColor.YELLOW + " to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                            .getString("colors." + rel.toString()) + clan);

                if (op.isOnline()) {
                    op.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                            + sender.getName() + ChatColor.YELLOW + " has revoked " + ChatColor.WHITE + "Your" + ChatColor.YELLOW + " invitation to "
                            + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                }

            } else {

                ClansDatabase.getDatabase().addInvite(u, clan);

                for (UUID id : ClansDatabase.getDatabase().getPlayers(clan)) {

                    Player p;

                    if ((p = Bukkit.getPlayer(id)) != null)
                        p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                                + sender.getName() + ChatColor.YELLOW + " has invited " + ChatColor.WHITE + target + ChatColor.YELLOW + " to "
                                + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                }

                if (!clan.equals(ClansDatabase.getDatabase().getClan((Player) sender)))
                    sender.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN")
                            + "You" + ChatColor.YELLOW + " have invited " + ChatColor.COLOR_CHAR
                            + ClansMain.getInstance().getConfig().getString("colors." + rel.toString()) + target
                            + ChatColor.YELLOW + " to " + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig()
                            .getString("colors." + rel.toString()) + clan);

                if (op.isOnline()) {
                    op.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + rel.toString())
                            + sender.getName() + ChatColor.YELLOW + " has invited " + ChatColor.WHITE + "You" + ChatColor.YELLOW + " to "
                            + ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + clan);
                }
            }
        }
    }
}
