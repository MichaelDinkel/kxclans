package com.kaixeleron.clans.subcommand.user;

import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

public class SubcommandHelp implements Subcommand {

    private String header, headersuffix, page, pagesuffix;
    private HashMap<Integer, String[]> pages;

    public SubcommandHelp() {
        header = ChatColor.RED + "===============[" + ChatColor.WHITE + " kxClans Help ";
        headersuffix = ChatColor.RED + " ]===============";
        page = ChatColor.GOLD + "Page: " + ChatColor.WHITE;
        pagesuffix = ChatColor.GOLD + "/" + ChatColor.WHITE + "4";

        pages = new HashMap<>();

        pages.put(1, new String[]{ChatColor.YELLOW + "/%label %canusehelp " + ChatColor.GOLD + "[Page]" + ChatColor.GREEN + " - View command help",
                ChatColor.YELLOW + "/%label %canuseclan " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - View clan info",
                ChatColor.YELLOW + "/%label %canuseally " + ChatColor.GOLD + "<Clan>" + ChatColor.GREEN + " - Request/accept a request to ally",
                ChatColor.YELLOW + "/%label %canuseclaim " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - Claim the chunk you're standing in",
                ChatColor.YELLOW + "/%label %canusecreate " + ChatColor.GOLD + "<Name>" + ChatColor.GREEN + " - Create a clan",
                ChatColor.YELLOW + "/%label %canusedemote " + ChatColor.GOLD + "<Player> [Clan]" + ChatColor.GREEN + " - Lower a player's clan rank",
                ChatColor.YELLOW + "/%label %canusedesc " + ChatColor.GOLD + "[Desc]" + ChatColor.GREEN + " - Set/clear your clan's description",
                ChatColor.YELLOW + "/%label %canusedisband " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - Disband a clan"});

        pages.put(2, new String[]{ChatColor.YELLOW + "/%label %canuseenemy " + ChatColor.GOLD + "<Clan>" + ChatColor.GREEN + " - Declare a clan as an enemy",
                ChatColor.YELLOW + "/%label %canuseflag " + ChatColor.GREEN + "- Manage clan flags",
                ChatColor.YELLOW + "/%label %canusehome " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - Teleport to a clan's home",
                ChatColor.YELLOW + "/%label %canuseinvite " + ChatColor.GREEN + " - Manage clan invites",
                ChatColor.YELLOW + "/%label %canusejoin " + ChatColor.GOLD + "<Clan>" + ChatColor.GREEN + " - Join a clan",
                ChatColor.YELLOW + "/%label %canusekick " + ChatColor.GOLD + "<Player> [Clan]" + ChatColor.GREEN + " - Kick a player from a clan",
                ChatColor.YELLOW + "/%label %canuseleader " + ChatColor.GOLD + "<Player> [Clan]" + ChatColor.GREEN + " - Promote a player to clan leader",
                ChatColor.YELLOW + "/%label %canuseleave " + ChatColor.GREEN + " - Leave your clan"});

        pages.put(3, new String[]{ChatColor.YELLOW + "/%label %canusemap " + ChatColor.GREEN + "- Show a map of nearby chunks",
                ChatColor.YELLOW + "/%label %canusename " + ChatColor.GOLD + "<Name>" + ChatColor.GREEN + " - Set the clan name",
                ChatColor.YELLOW + "/%label %canuseneutral " + ChatColor.GOLD + "<Clan>" + ChatColor.GREEN + " - Declare neutrality to a clan",
                ChatColor.YELLOW + "/%label %canuseperm " + ChatColor.GOLD + "<Page|Set> <Perm> <True|False> <Rank> [Clan]" + ChatColor.GREEN + " - Manage clan permissions",
                ChatColor.YELLOW + "/%label %canuseplayer " + ChatColor.GOLD + "<Player>" + ChatColor.GREEN + " - View player info",
                ChatColor.YELLOW + "/%label %canusepromote " + ChatColor.GOLD + "<Player> [Clan]" + ChatColor.GREEN + " - Raise a player's clan rank",
                ChatColor.YELLOW + "/%label %canuseseechunk " + ChatColor.GREEN + " - Visualize the chunk you're in",
                ChatColor.YELLOW + "/%label %canusesethome " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - Set the clan home"});

        pages.put(4, new String[]{ChatColor.YELLOW + "/%label %canusetruce " + ChatColor.GOLD + "<Clan>" + ChatColor.GREEN + " - Request/accept a request to truce",
                ChatColor.YELLOW + "/%label %canuseunclaim " + ChatColor.GREEN + " - Unclaim the chunk you're in",
                ChatColor.YELLOW + "/%label %canuseaccess " + ChatColor.GREEN + "- Manage land access",
                ChatColor.YELLOW + "/%label %canusewarp " + ChatColor.GOLD + "<Warp> [Clan]" + ChatColor.GREEN + " - Teleport to a clan warp",
                ChatColor.YELLOW + "/%label %canusesetwarp " + ChatColor.GOLD + "<Warp> [Clan]" + ChatColor.GREEN + " - Set a clan warp",
                ChatColor.YELLOW + "/%label %canusetitle " + ChatColor.GOLD + "<Player> [Title]" + ChatColor.GREEN + " - Manage clan member titles",
                ChatColor.YELLOW + "/%label %canusedelhome " + ChatColor.GOLD + "[Clan]" + ChatColor.GREEN + " - Delete a clan's home"});
    }

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.command.help")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to view the help menu.");
            return;
        }

        int page = 1;

        if (args.length > 1) try {
            page = Integer.parseInt(args[1]);

            if (page > 4) page = 4;
        } catch (NumberFormatException ignored) {
        }

        sender.sendMessage(header + this.page + page + pagesuffix + headersuffix);

        for (String s : pages.get(page)) {
            String canuse = s.split("%canuse")[1].split(" ")[0];

            sender.sendMessage(s.replace("%label", label).replace("%canuse", PermissionManager.getInstance().canUse(sender, canuse, "")
                    ? "" : ChatColor.RED + ""));
        }
    }
}
