package com.kaixeleron.clans.subcommand;

import org.bukkit.command.CommandSender;

public interface Subcommand {

    void execute(CommandSender sender, String label, String... args);

}
