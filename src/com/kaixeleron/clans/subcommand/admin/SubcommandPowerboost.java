package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

public class SubcommandPowerboost implements Subcommand {

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.powerboost")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to boost power.");
            return;
        }

        if (args.length < 4) {

            sender.sendMessage(ChatColor.YELLOW + "Boost a clan or player's power");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " powerboost <Clan|Player> <Target> <Amount>");
            return;

        }

        boolean clan, player;

        clan = args[1].equalsIgnoreCase("clan");
        player = args[1].equalsIgnoreCase("player");

        if (!clan && !player) {
            sender.sendMessage(ChatColor.RED + "Invalid boost type. Must be " + ChatColor.WHITE + "clan" + ChatColor.RED + " or " + ChatColor.WHITE + "player" + ChatColor.RED + ".");
            return;
        }

        if (player && args.length > 4) {
            sender.sendMessage(ChatColor.RED + "Player names cannot contain spaces.");
            return;
        }

        StringBuilder target = new StringBuilder();

        for (int i = 2; i < args.length - 1; i++) {
            target.append(args[i]).append(" ");
        }

        target = new StringBuilder(target.substring(0, target.length() - 1));

        if (player && !target.toString().matches("[A-Za-z0-9]*")) {
            sender.sendMessage(target.toString() + ChatColor.RED + " is an invalid player name. Player names must be alphanumeric.");
            return;
        }

        if (player && target.length() > 20) {
            sender.sendMessage(target.toString() + ChatColor.RED + " is an invalid player name. Player names must be at least 3 characters.");
            return;
        }

        if (player && target.length() < 3) {
            sender.sendMessage(target.toString() + ChatColor.RED + " is an invalid player name. Player names must be at most 20 characters.");
            return;
        }

        try {

            double power = Double.parseDouble(args[args.length - 1]);

            OfflinePlayer op = null;

            if (player) op = Bukkit.getOfflinePlayer(target.toString());

            if (player && !ClansDatabase.getDatabase().playerExists(op.getUniqueId())) {
                sender.sendMessage(target.toString() + ChatColor.RED + " has not been recorded in the clans database.");
                return;
            }

            if (player) {
                ClansDatabase.getDatabase().addPlayerMaxPower(op.getUniqueId(), power);
                ClansDatabase.getDatabase().setPlayerPower(op.getUniqueId(), ClansDatabase.getDatabase().getPlayerPower(op.getUniqueId()) + power);

                sender.sendMessage(ChatColor.YELLOW + "Player " + ChatColor.WHITE + target.toString() + ChatColor.YELLOW + "'s power increased by " + ChatColor.WHITE + power);
            } else if (clan) {
                ClansDatabase.getDatabase().addClanMaxPower(target.toString(), power);

                sender.sendMessage(ChatColor.YELLOW + "Clan " + ChatColor.WHITE + target.toString() + ChatColor.YELLOW + "'s power increased by " + ChatColor.WHITE + power);
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Check the console for details.");
            e.printStackTrace();
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Invalid power input. The power to add must be a valid number.");
        }
    }
}
