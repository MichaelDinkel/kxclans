package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandNamesys implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.namesys")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to name system clans.");
            return;
        }

        if (args.length < 3) {
            sender.sendMessage(ChatColor.YELLOW + "Rename a system clan");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " namesys <Safe|War|Unclaimed> <Name>");
            return;
        }

        boolean safe = args[1].equalsIgnoreCase("safe"), war = args[1].equalsIgnoreCase("war"), unclaimed = args[1].equalsIgnoreCase("unclaimed");

        StringBuilder name = new StringBuilder();

        for (int i = 2; i < args.length; i++) {
            name.append(args[i]).append(" ");
        }

        name = new StringBuilder(name.substring(0, name.length() - 1));

        try {

            if (ClansDatabase.getDatabase().clanExists(name.toString()).equals(name.toString())) {

                sender.sendMessage(ChatColor.RED + "A clan named " + ChatColor.WHITE + name.toString() + ChatColor.RED + " already exists.");
                return;
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Check the console for details.");
            e.printStackTrace();
            return;
        }

        if (safe) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have renamed " + ChatColor.GOLD
                    + ClansDatabase.getSafeName() + ChatColor.YELLOW + " to " + ChatColor.GOLD + name.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.namesys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has renamed " + ChatColor.GOLD
                            + ClansDatabase.getSafeName() + ChatColor.YELLOW + " to " + ChatColor.GOLD + name.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has renamed " + ClansDatabase.getSafeName() + " to " + name.toString());

            ClansDatabase.setSafeName(name.toString());

        } else if (war) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have renamed " + ChatColor.DARK_RED
                    + ClansDatabase.getWarName() + ChatColor.YELLOW + " to " + ChatColor.DARK_RED + name.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.namesys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has renamed " + ChatColor.DARK_RED
                            + ClansDatabase.getWarName() + ChatColor.YELLOW + " to " + ChatColor.DARK_RED + name.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has renamed " + ClansDatabase.getWarName() + " to " + name.toString());

            ClansDatabase.setWarName(name.toString());

        } else if (unclaimed) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have renamed " + ChatColor.DARK_GREEN
                    + ClansDatabase.getUnclaimedName() + ChatColor.YELLOW + " to " + ChatColor.DARK_GREEN + name.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.namesys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has renamed " + ChatColor.DARK_GREEN
                            + ClansDatabase.getUnclaimedName() + ChatColor.YELLOW + " to " + ChatColor.DARK_GREEN + name.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has renamed " + ClansDatabase.getUnclaimedName() + " to " + name.toString());

            ClansDatabase.setUnclaimedName(name.toString());

        } else {
            sender.sendMessage(ChatColor.RED + "Invalid target clan. Must be " + ChatColor.WHITE + "safe" + ChatColor.RED
                    + ", " + ChatColor.WHITE + "war" + ChatColor.RED + ", or " + ChatColor.WHITE + "unclaimed" + ChatColor.RED + ".");
        }
    }
}
