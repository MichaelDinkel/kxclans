package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class SubcommandFlagsys implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.flagsys")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to manage system clan flags.");
            return;
        }

            if (args.length == 1 || args.length == 2) {

                help(sender, label);

            } else if (args.length == 3) {

                if (args[1].equalsIgnoreCase("s") || args[1].equalsIgnoreCase("show")) {

                    showFlags(sender, args[2]);

                } else {

                    help(sender, label);

                }

            } else {

                if (args[1].equalsIgnoreCase("s") || args[1].equalsIgnoreCase("show")) {

                    showFlags(sender, args[2]);

                } else {

                    boolean value;

                    switch (args[2].toLowerCase()) {
                        case "true":
                        case "enabled":
                            value = true;
                            break;
                        case "false":
                        case "disabled":
                            value = false;
                            break;
                        default:
                            sender.sendMessage(ChatColor.RED + "Invalid flag value. Must be " + ChatColor.WHITE + "true" + ChatColor.RED + ", "
                                    + ChatColor.WHITE + "enabled" + ChatColor.RED + ", " + ChatColor.WHITE + "false" + ChatColor.RED + ", or "
                                    + ChatColor.WHITE + "disabled" + ChatColor.RED + ".");
                            return;
                    }

                    Flag flag;

                    try {

                        flag = Flag.valueOf(args[1].toUpperCase());

                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(ChatColor.RED + "Unknown flag. Type " + ChatColor.WHITE + "/" + label + " flag s|show " + ChatColor.RED + "to view available flags.");
                        return;
                    }

                    ChatColor color;
                    String name;

                    switch (args[3].toLowerCase()) {
                        case "safe":
                            color = ChatColor.GOLD;
                            name = ClansDatabase.getSafeName();
                            break;
                        case "war":
                            color = ChatColor.DARK_RED;
                            name = ClansDatabase.getWarName();
                            break;
                        case "unclaimed":
                            color = ChatColor.DARK_GREEN;
                            name = ClansDatabase.getUnclaimedName();
                            break;
                        default:
                            sender.sendMessage(ChatColor.RED + "Invalid target clan. Must be " + ChatColor.WHITE + "safe" + ChatColor.RED
                                    + ", " + ChatColor.WHITE + "war" + ChatColor.RED + ", or " + ChatColor.WHITE + "unclaimed" + ChatColor.RED + ".");
                            return;
                    }

                    ClansMain.getInstance().getConfig().set("sysclans." + args[3] + ".flags." + flag.toString().toLowerCase(), value);
                    ClansMain.getInstance().saveConfig();

                    sender.sendMessage(ChatColor.YELLOW + "Flag " + ChatColor.WHITE + args[1].toLowerCase()
                            + ChatColor.YELLOW + " is now " + (value ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled")
                            + ChatColor.YELLOW + " for " + color + name);

                    for (Player p : Bukkit.getOnlinePlayers()) {

                        if (p.equals(sender)) continue;

                        if (p.hasPermission("kxclans.admin.flagsys")) p.sendMessage(sender.getName() + ChatColor.YELLOW
                                + " has " + (value ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled")
                                + ChatColor.YELLOW + " flag " + ChatColor.WHITE + args[1].toLowerCase() + ChatColor.YELLOW
                                + " on " + color + name);

                    }
                }
            }
    }

    private void help(CommandSender sender, String label) {

        sender.sendMessage(ChatColor.YELLOW + "Manage system clan flags");
        sender.sendMessage(ChatColor.RED + "Usage to set a flag: " + ChatColor.WHITE + "/" + label + " flagsys <Flag> <Value> <Clan>");
        sender.sendMessage(ChatColor.RED + "Usage to show flags: " + ChatColor.WHITE + "/" + label + " flagsys s|show <Clan>");

    }

    private void showFlags(CommandSender sender, String clan) {

        Map<Flag, Boolean> flags = null;

        ChatColor color;
        String name;

        switch (clan.toLowerCase()) {
            case "safe":
                color = ChatColor.GOLD;
                name = ClansDatabase.getSafeName();
                break;
            case "war":
                color = ChatColor.DARK_RED;
                name = ClansDatabase.getWarName();
                break;
            case "unclaimed":
                color = ChatColor.DARK_GREEN;
                name = ClansDatabase.getUnclaimedName();
                break;
            default:
                sender.sendMessage(ChatColor.RED + "Invalid target clan. Must be " + ChatColor.WHITE + "safe" + ChatColor.RED
                        + ", " + ChatColor.WHITE + "war" + ChatColor.RED + ", or " + ChatColor.WHITE + "unclaimed" + ChatColor.RED + ".");
                return;
        }

        try {
            flags = ClansDatabase.getDatabase().getFlags(name);
        } catch (DatabaseException ignored) {}

        String equals = "====================";
        String subequals = equals.substring(0, equals.length() - name.length() / 2);

        if (flags == null) return;

        sender.sendMessage(ChatColor.RED + subequals + "[ " + ChatColor.WHITE + "Flags for " + color + name
                + ChatColor.RED + " ]" + subequals);

        for (Flag f : flags.keySet()) {

            String flag = f.toString();

            boolean user = ClansMain.getInstance().getConfig().getBoolean("flags." + flag.toLowerCase() + ".user");

            if (!user && !ClansMain.getInstance().getConfig().getBoolean("showadminflags") && !PermissionManager.getInstance().isBypassing(sender)) continue;

            StringBuilder message = new StringBuilder(user ? "" : "" + ChatColor.GRAY);
            message.append(flag.substring(0, 1).toUpperCase());
            message.append(flag.substring(1).toLowerCase());
            message.append(ChatColor.YELLOW);
            message.append(": ");
            message.append(flags.get(f) ? ChatColor.GREEN + "Enabled" : ChatColor.RED + "Disabled");

            switch (f) {
                case ENDERGRIEF:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Endermen ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" pick up blocks");
                    break;
                case EXPLOSIONS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Explosions ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" occur");
                    break;
                case FIRESPREAD:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Fire ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spread");
                    break;
                case FRIENDLYFIRE:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Members/allies ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" damage eachother");
                    break;
                case INFPOWER:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "has" : "doesn't have");
                    message.append(" infinite power");
                    break;
                case MONSTERS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Monsters ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spawn");
                    break;
                case OPEN:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Players ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" freely join");
                    break;
                case PEACEFUL:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "is" : "isn't");
                    message.append(" allies with everybody");
                    break;
                case PERMANENT:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Clan ");
                    message.append(flags.get(f) ? "will" : "won't");
                    message.append(" remain with 0 members");
                    break;
                case POWERLOSS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Power ");
                    message.append(flags.get(f) ? "will" : "won't");
                    message.append(" be lost on death");
                    break;
                case PVP:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Players ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" damage eachother");
                    break;
                case ANIMALS:
                    message.append(ChatColor.YELLOW);
                    message.append(" - Animals ");
                    message.append(flags.get(f) ? "can" : "can't");
                    message.append(" spawn");
            }

            sender.sendMessage(message.toString());
        }
    }
}
