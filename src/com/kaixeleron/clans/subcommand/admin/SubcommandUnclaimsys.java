package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandUnclaimsys implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.unclaimsys")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to unclaim land from system clans.");
            return;
        }

        try {

            StringBuilder clan;
            boolean safe = false, war = false;

            Player p = (Player) sender;
            Chunk c = p.getLocation().getChunk();

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (owner.equals(ClansDatabase.getWarName())) {
                war = true;
            } else if (owner.equals(ClansDatabase.getSafeName())) {
                safe = true;
            } else if (owner.equals(ClansDatabase.getUnclaimedName())) {
                sender.sendMessage(ChatColor.RED + "This land is unowned.");
                return;
            } else {
                sender.sendMessage(owner + ChatColor.RED + " is not a system clan. Use " + ChatColor.WHITE + "/c unclaim " + ChatColor.RED + "instead.");
                return;
            }

            if (war) {

                ClansDatabase.getDatabase().unclaim(ClansDatabase.getWarName(), c);

                sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have unclaimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                        + ChatColor.YELLOW + " from the system clan " + ChatColor.DARK_RED + ClansDatabase.getWarName());

                for (Player gp : Bukkit.getOnlinePlayers()) {

                    if (gp.hasPermission("kxclans.notify.unclaimsys") && !gp.equals(p)) {
                        gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has unclaimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                                + ChatColor.YELLOW + " from the system clan " + ChatColor.DARK_RED + ClansDatabase.getWarName());
                    }
                }

                Bukkit.getLogger().info(sender.getName() + " has unclaimed " + c.getX() + "," + c.getZ() + " from the system clan " + ClansDatabase.getWarName());

            } else if (safe) {

                ClansDatabase.getDatabase().unclaim(ClansDatabase.getSafeName(), c);

                sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have unclaimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                        + ChatColor.YELLOW + " from the system clan " + ChatColor.GOLD + ClansDatabase.getSafeName());

                for (Player gp : Bukkit.getOnlinePlayers()) {

                    if (gp.hasPermission("kxclans.notify.unclaimsys") && !gp.equals(p)) {
                        gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has unclaimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                                + ChatColor.YELLOW + " from the system clan " + ChatColor.GOLD + ClansDatabase.getSafeName());
                    }
                }

                Bukkit.getLogger().info(sender.getName() + " has unclaimed " + c.getX() + "," + c.getZ() + " from the system clan " + ClansDatabase.getSafeName());

            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Check the console for details.");
            e.printStackTrace();
        }
    }
}
