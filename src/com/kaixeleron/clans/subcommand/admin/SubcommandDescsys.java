package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandDescsys implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.descsys")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to name system clans.");
            return;
        }

        if (args.length < 3) {
            sender.sendMessage(ChatColor.YELLOW + "Change a system clan's description");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " descsys <Safe|War|Unclaimed> <Desc>");
            return;
        }

        boolean safe = args[1].equalsIgnoreCase("safe"), war = args[1].equalsIgnoreCase("war"), unclaimed = args[1].equalsIgnoreCase("unclaimed");

        StringBuilder desc = new StringBuilder();

        for (int i = 2; i < args.length; i++) {
            desc.append(args[i]).append(" ");
        }

        desc = new StringBuilder(desc.substring(0, desc.length() - 1));

        if (safe) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have changed the description of " + ChatColor.GOLD
                    + ClansDatabase.getSafeName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.descsys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has changed the description of " + ChatColor.GOLD
                            + ClansDatabase.getSafeName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has changed the description of " + ClansDatabase.getSafeName() + " to " + desc.toString());

            ClansDatabase.setSafeDesc(desc.toString());

        } else if (war) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have changed the description of " + ChatColor.DARK_RED
                    + ClansDatabase.getWarName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.descsys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has changed the description of " + ChatColor.DARK_RED
                            + ClansDatabase.getWarName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has changed the description of " + ClansDatabase.getWarName() + " to " + desc.toString());

            ClansDatabase.setWarDesc(desc.toString());

        } else if (unclaimed) {

            sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have changed the description of " + ChatColor.DARK_GREEN
                    + ClansDatabase.getUnclaimedName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());

            for (Player gp : Bukkit.getOnlinePlayers()) {

                if (gp.hasPermission("kxclans.notify.descsys") && !gp.equals(sender)) {
                    gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has changed the description of " + ChatColor.DARK_GREEN
                            + ClansDatabase.getUnclaimedName() + ChatColor.YELLOW + " to " + ChatColor.WHITE + desc.toString());
                }
            }

            Bukkit.getLogger().info(sender.getName() + " has changed the description of " + ClansDatabase.getUnclaimedName() + " to " + desc.toString());

            ClansDatabase.setUnclaimedDesc(desc.toString());

        } else {
            sender.sendMessage(ChatColor.RED + "Invalid target clan. Must be " + ChatColor.WHITE + "safe" + ChatColor.RED
                    + ", " + ChatColor.WHITE + "war" + ChatColor.RED + ", or " + ChatColor.WHITE + "unclaimed" + ChatColor.RED + ".");
        }
    }
}
