package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandClaimsys implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.claimsys")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to claim land for system clans.");
            return;
        }

        try {

            StringBuilder clan;
            boolean safe, war;

            if (args.length > 1) {

                clan = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    clan.append(args[i]).append(" ");

                }

                clan = new StringBuilder(clan.substring(0, clan.length() - 1));

                if (ClansDatabase.getUnclaimedName().equals(clan.toString())) {

                    sender.sendMessage(ChatColor.DARK_GREEN + ClansDatabase.getUnclaimedName() + ChatColor.RED + " cannot claim land. Unclaim the chunk instead.");

                    return;
                }

                safe = ClansDatabase.getSafeName().equalsIgnoreCase(clan.toString());

                war = ClansDatabase.getWarName().equalsIgnoreCase(clan.toString());

            } else {

                sender.sendMessage(ChatColor.YELLOW + "Claim land for a system clan.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/" + label + " claimsys <Target>");
                return;

            }

            Player p = (Player) sender;
            Chunk c = p.getLocation().getChunk();

            if (war) {

                ClansDatabase.getDatabase().claim(ClansDatabase.getWarName(), c);

                sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have claimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                        + ChatColor.YELLOW + " for the system clan " + ChatColor.DARK_RED + ClansDatabase.getWarName());

                for (Player gp : Bukkit.getOnlinePlayers()) {

                    if (gp.hasPermission("kxclans.notify.claimsys") && !gp.equals(p)) {
                        gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has claimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                                + ChatColor.YELLOW + " for the system clan " + ChatColor.DARK_RED + ClansDatabase.getWarName());
                    }
                }

                Bukkit.getLogger().info(sender.getName() + " has claimed " + c.getX() + "," + c.getZ() + " for the system clan " + ClansDatabase.getWarName());

            } else if (safe) {

                ClansDatabase.getDatabase().claim(ClansDatabase.getSafeName(), c);

                sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have claimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                        + ChatColor.YELLOW + " for the system clan " + ChatColor.GOLD + ClansDatabase.getSafeName());

                for (Player gp : Bukkit.getOnlinePlayers()) {

                    if (gp.hasPermission("kxclans.notify.claimsys") && !gp.equals(p)) {
                        gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has claimed " + ChatColor.WHITE + c.getX() + "," + c.getZ()
                                + ChatColor.YELLOW + " for the system clan " + ChatColor.GOLD + ClansDatabase.getSafeName());
                    }
                }

                Bukkit.getLogger().info(sender.getName() + " has claimed " + c.getX() + "," + c.getZ() + " for the system clan " + ClansDatabase.getSafeName());

            } else {
                sender.sendMessage(clan.toString() + ChatColor.RED + " is not a system clan. Use " + ChatColor.WHITE + "/c claim " + clan.toString() + ChatColor.RED + " instead.");
            }

        } catch (DatabaseException e) {
            sender.sendMessage(ChatColor.RED + "A database error occurred. Check the console for details.");
            e.printStackTrace();
        }
    }
}
