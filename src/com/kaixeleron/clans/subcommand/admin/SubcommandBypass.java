package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubcommandBypass implements Subcommand {

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.bypass")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to toggle admin bypass mode.");
            return;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be used by a player.");
            return;
        }

        Player p = (Player) sender;

        boolean bypassing = PermissionManager.getInstance().isBypassing(p);

        PermissionManager.getInstance().setBypassing(p, !bypassing);

        sender.sendMessage(ChatColor.WHITE + "You " + ChatColor.YELLOW + "have " + (!bypassing ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled")
                + ChatColor.YELLOW + " admin bypass mode.");

        for (Player gp : Bukkit.getOnlinePlayers()) {

            if (gp.hasPermission("kxclans.notify.bypasstoggle") && !gp.equals(p)) {
                gp.sendMessage(ChatColor.WHITE + sender.getName() + ChatColor.YELLOW + " has " + (!bypassing ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled")
                        + ChatColor.YELLOW + " admin bypass mode.");
            }
        }

        Bukkit.getLogger().info(sender.getName() + " has " + (!bypassing ? "enabled" : "disabled") + " admin bypass mode.");
    }
}
