package com.kaixeleron.clans.subcommand.admin;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.CombatTagManager;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.SQLDatabase;
import com.kaixeleron.clans.subcommand.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Method;

public class SubcommandReload implements Subcommand {

    private final CombatTagManager combatTagManager;

    public SubcommandReload(CombatTagManager combatTagManager) {

        this.combatTagManager = combatTagManager;

        try {

            setEnabled = JavaPlugin.class.getDeclaredMethod("setEnabled", boolean.class);
            setEnabled.setAccessible(true);

        } catch (NoSuchMethodException ignored) {}
    }

    @Override
    public void execute(CommandSender sender, String label, String... args) {

        if (!sender.hasPermission("kxclans.admin.reload")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to reload clans.");
            return;
        }

        ClansMain.getInstance().reloadConfig();

        try {
            if (ClansDatabase.getDatabase() != null) ClansDatabase.getDatabase().close();
        } catch (DatabaseException e) {
            ClansMain.getInstance().getLogger().warning("kxClans could not disconnect from the SQL database.");
            e.printStackTrace();
            disable();
            return;
        }

        ClansMain.getInstance().saveDefaultConfig();

        ClansDatabase.setUnclaimedName(ClansMain.getInstance().getConfig().getString("sysclans.unclaimed.name"));
        ClansDatabase.setWarName(ClansMain.getInstance().getConfig().getString("sysclans.war.name"));
        ClansDatabase.setSafeName(ClansMain.getInstance().getConfig().getString("sysclans.safe.name"));

        ClansDatabase.setUnclaimedDesc(ClansMain.getInstance().getConfig().getString("sysclans.unclaimed.desc"));
        ClansDatabase.setWarDesc(ClansMain.getInstance().getConfig().getString("sysclans.war.desc"));
        ClansDatabase.setSafeDesc(ClansMain.getInstance().getConfig().getString("sysclans.safe.desc"));

        switch (ClansMain.getInstance().getConfig().getString("database")) {
            case "sql":

                try {

                    ClansDatabase.setDatabase(new SQLDatabase(ClansMain.getInstance().getConfig().getString("sql.address"), ClansMain.getInstance().getConfig().getString("sql.port"), ClansMain.getInstance().getConfig().getString("sql.database"),
                            ClansMain.getInstance().getConfig().getString("sql.user"), ClansMain.getInstance().getConfig().getString("sql.password"), ClansMain.getInstance().getConfig().getString("sql.prefix")));

                } catch (DatabaseException e) {

                    ClansMain.getInstance().getLogger().warning("kxClans could not connect to or set up the SQL database.");
                    e.printStackTrace();
                    disable();
                    return;

                } catch (ClassNotFoundException e) {

                    ClansMain.getInstance().getLogger().warning("kxClans could not find the MySQL driver.");
                    disable();
                    return;

                }

                break;
            case "yaml":

                //TODO

                break;
            case "none":
                break;
            default:

                ClansMain.getInstance().getLogger().warning("Invalid database type specified: " + ClansMain.getInstance().getConfig().getString("database"));
                disable();

                return;
        }

        combatTagManager.setActive(ClansMain.getInstance().getConfig().getBoolean("combatTag.active"));
        combatTagManager.setDelaySeconds(ClansMain.getInstance().getConfig().getLong("combatTag.seconds"));

        sender.sendMessage(ChatColor.GREEN + "kxClans reloaded successfully.");

    }

    private Method setEnabled = null;

    private void disable() {

        ClansMain.getInstance().getLogger().info("Disabling");

        try {

            setEnabled.invoke(ClansMain.getInstance(), false);

        } catch (Exception ignored) {}
    }
}
