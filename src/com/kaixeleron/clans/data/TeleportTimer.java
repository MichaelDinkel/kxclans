package com.kaixeleron.clans.data;

import com.kaixeleron.clans.ClansMain;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class TeleportTimer {

    public TeleportTimer() {
        TeleportTimer.instance = this;
    }

    private Map<Player, BukkitRunnable> timers = new HashMap<>();

    public void teleport(Player p, Location l) {

        BukkitRunnable br = new BukkitRunnable() {
            @Override
            public void run() {
                p.teleport(l);
                timers.remove(p);
            }
        };
        br.runTaskLater(ClansMain.getInstance(), ClansMain.getInstance().getConfig().getInt("tpdelay") * 20);
        timers.put(p, br);
    }

    public void removeTimer(Player p) {

        if (timers.containsKey(p)) {

            timers.get(p).cancel();

            p.sendMessage(ChatColor.RED + "Teleportation cancelled due to movement.");

            timers.remove(p);

        }
    }

    public static TeleportTimer getInstance() {
        return instance;
    }

    private static TeleportTimer instance;
}
