package com.kaixeleron.clans.data;

import com.kaixeleron.clans.data.cache.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DatabaseCache {

    private Map<String, CacheClan> clans;
    private Map<String, List<CacheWarp>> warps;
    private Map<String, List<CacheClaim>> claims;
    private Map<String, List<CachePerm>> perms;
    private Map<UUID, CachePlayer> players;
    private List<CachePolicy> policies;
    private Map<UUID, List<CacheInvitation>> invitations;
    private Map<String, List<CacheFlag>> flags;
    private Map<String, List<CacheAccess>> access;

    DatabaseCache() {

        clans = new ConcurrentHashMap<>();
        warps = new ConcurrentHashMap<>();
        claims = new ConcurrentHashMap<>();
        perms = new ConcurrentHashMap<>();
        players = new ConcurrentHashMap<>();
        policies = Collections.synchronizedList(new ArrayList<>());
        invitations = new ConcurrentHashMap<>();
        flags = new ConcurrentHashMap<>();
        access = new ConcurrentHashMap<>();

    }

    public void setClan(String name, CacheClan clan) {
        clans.put(name, clan);
    }

    public void removeClan(String name) {
        clans.remove(name);
    }

    public CacheClan getClan(String name) {
        return clans.get(name);
    }

    public void addWarp(String clan, CacheWarp warp) {

        List<CacheWarp> list = warps.get(clan);

        if (list == null) list = new ArrayList<>();

        if (list.contains(warp)) return;

        list.add(warp);

        warps.put(clan, list);

    }

    public void removeWarp(String clan, CacheWarp warp) {

        List<CacheWarp> list = warps.get(clan);

        if (list == null) return;

        list.remove(warp);

        if (list.isEmpty()) {
            warps.remove(clan);
        } else {
            warps.put(clan, list);
        }

    }

    public List<CacheWarp> getWarps(String clan) {
        return warps.get(clan);
    }

    public void addClaim(String clan, CacheClaim claim) {

        List<CacheClaim> list = claims.get(clan);

        if (list == null) list = new ArrayList<>();

        if (list.contains(claim)) return;

        list.add(claim);

        claims.put(clan, list);

    }

    public void removeClaim(String clan, CacheClaim claim) {

        List<CacheClaim> list = claims.get(clan);

        if (list == null) return;

        list.remove(claim);

        if (list.isEmpty()) {
            claims.remove(clan);
        } else {
            claims.put(clan, list);
        }

    }

    public List<CacheClaim> getClaims(String clan) {
        return claims.get(clan);
    }

    public void addPerm(String clan, CachePerm perm) {

        List<CachePerm> list = perms.get(clan);

        if (list == null) list = new ArrayList<>();

        if (list.contains(perm)) return;

        list.add(perm);

        perms.put(clan, list);

    }

    public void removePerm(String clan, CachePerm perm) {

        List<CachePerm> list = perms.get(clan);

        if (list == null) return;

        list.remove(perm);

        if (list.isEmpty()) {
            perms.remove(clan);
        } else {
            perms.put(clan, list);
        }

    }

    public List<CachePerm> getPerms(String clan) {
        return perms.get(clan);
    }

    public void setPlayer(UUID u, CachePlayer player) {
        players.put(u, player);
    }

    public void removePlayer(UUID u) {
        players.remove(u);
    }

    public CachePlayer getPlayer(UUID u) {
        return players.get(u);
    }

    public void addPolicy(CachePolicy policy) {

        if (policies.contains(policy)) return;

        policies.add(policy);

    }

    public void removePolicy(String clan, CachePolicy policy) {

        policies.remove(policy);

    }

    public List<CachePolicy> getPolicies() {
        return policies;
    }

    public void addInvitation(UUID uuid, CacheInvitation invitation) {

        List<CacheInvitation> list = invitations.get(uuid);

        if (list == null) list = new ArrayList<>();

        if (list.contains(invitation)) return;

        list.add(invitation);

        invitations.put(uuid, list);

    }

    public void removeInvitation(UUID uuid, CacheInvitation invitation) {

        List<CacheInvitation> list = invitations.get(uuid);

        if (list == null) return;

        list.remove(invitation);

        if (list.isEmpty()) {
            invitations.remove(uuid);
        } else {
            invitations.put(uuid, list);
        }

    }

    public List<CacheInvitation> getInvitations(UUID uuid) {
        return invitations.get(uuid);
    }

    public void addFlag(String clan, CacheFlag flag) {

        List<CacheFlag> list = flags.get(clan);

        if (list == null) list = new ArrayList<>();

        if (list.contains(flag)) return;

        list.add(flag);

        flags.put(clan, list);

    }

    public void removeFlag(String clan, CacheFlag flag) {

        List<CacheFlag> list = flags.get(clan);

        if (list == null) return;

        list.remove(flag);

        if (list.isEmpty()) {
            flags.remove(clan);
        } else {
            flags.put(clan, list);
        }

    }

    public List<CacheFlag> getFlags(String clan) {
        return flags.get(clan);
    }

    public void addAccess(String clan, CacheAccess access) {

        List<CacheAccess> list = this.access.get(clan);

        if (list == null) list = new ArrayList<>();

        if (list.contains(access)) return;

        list.add(access);

        this.access.put(clan, list);

    }

    public void removeAccess(String clan, CacheAccess access) {

        List<CacheAccess> list = this.access.get(clan);

        if (list == null) return;

        list.remove(access);

        if (list.isEmpty()) {
            this.access.remove(clan);
        } else {
            this.access.put(clan, list);
        }

    }

    public List<CacheAccess> getAccess(String clan) {
        return this.access.get(clan);
    }
}
