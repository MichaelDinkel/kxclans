package com.kaixeleron.clans.data;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.cache.*;
import org.bukkit.*;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.*;

public class SQLDatabase extends ClansDatabase {

    private final String prefix, hostname, port, database, username, password;

    private Connection c;

    private DatabaseCache cache = null;

    public SQLDatabase(String address, String port, String database, String user, String password, String prefix) throws DatabaseException, ClassNotFoundException {

        this.prefix = prefix;
        this.hostname = address;
        this.port = port;
        this.database = database;
        this.username = user;
        this.password = password;

        Class.forName("com.mysql.jdbc.Driver");

        try {
            c = DriverManager.getConnection("jdbc:mysql://" + address + ":" + port + "/" + database, user, password);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        PreparedStatement createClans = null;
        PreparedStatement createWarps = null;
        PreparedStatement createClaims = null;
        PreparedStatement createPerms = null;
        PreparedStatement createPlayers = null;
        PreparedStatement createpolicies = null;
        PreparedStatement createInvites = null;
        PreparedStatement createFlags = null;
        PreparedStatement createAccess = null;

        try {

            createClans = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "clans` (name VARCHAR(20), description VARCHAR(128), created BIGINT,"
                    + " powerboost DOUBLE, homex DOUBLE, homey DOUBLE, homez DOUBLE, homeyaw FLOAT, homepitch FLOAT, homeworld VARCHAR(32), PRIMARY KEY (`name`));");
            createWarps = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "warps` (clan VARCHAR(20), name VARCHAR(20), world VARCHAR(32), x DOUBLE, y DOUBLE, z DOUBLE, yaw FLOAT, pitch FLOAT);");
            createClaims = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "claims` (clan VARCHAR(20), x INT, z INT, PRIMARY KEY (x, z), INDEX(clan));");
            createPerms = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "perms` (clan VARCHAR(20), rank VARCHAR(7), perm VARCHAR(9), value BIT);");
            createPlayers = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "players` (uuid CHAR(36), name VARCHAR(16), clan VARCHAR(20), "
                    + "rank VARCHAR(7), title VARCHAR(16), power DOUBLE, maxpower DOUBLE, chatmode VARCHAR(6), PRIMARY KEY (`uuid`));");
            createpolicies = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "policies` (clan1 VARCHAR(20), clan2 VARCHAR(20), policy VARCHAR(10), PRIMARY KEY (clan1, clan2));");
            createInvites = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "invitations` (uuid CHAR(36), clan VARCHAR(20));");
            createFlags = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "flags` (clan VARCHAR(20), flag VARCHAR(15), value BIT);");
            createAccess = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "access` (clan VARCHAR(20), x INT, z INT, target VARCHAR(36))");

            createClans.executeUpdate();
            createWarps.executeUpdate();
            createClaims.executeUpdate();
            createPerms.executeUpdate();
            createPlayers.executeUpdate();
            createpolicies.executeUpdate();
            createInvites.executeUpdate();
            createFlags.executeUpdate();
            createAccess.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(createClans, createWarps, createClaims, createPerms, createPlayers, createpolicies, createInvites, createFlags, createAccess);
        }
    }

    private synchronized void closeSilently(@Nullable AutoCloseable... ac) {
        for (AutoCloseable c : ac)
            try {
                c.close();
            } catch (Exception ignored) {}
    }

    private synchronized void reopen() throws SQLException {
        if (c.isClosed())
            c = DriverManager.getConnection("jdbc:mysql://" + hostname + ":" + port + "/" + database, username, password);
    }

    @Override
    public void loadCache() throws DatabaseException {

        cache = new DatabaseCache();

        PreparedStatement getClans = null, getWarps = null, getClaims = null, getPerms = null, getPlayers = null, getPolicies = null, getInvites = null, getFlags = null, getAccess = null;
        ResultSet clans = null, warps = null, claims = null, perms = null, players = null, policies = null, invites = null, flags = null, access = null;

        try {

            reopen();

            getClans = c.prepareStatement("SELECT * FROM `" + prefix + "clans`;");
            getWarps = c.prepareStatement("SELECT * FROM `" + prefix + "warps`;");
            getClaims = c.prepareStatement("SELECT * FROM `" + prefix + "claims`;");
            getPerms = c.prepareStatement("SELECT * FROM `" + prefix + "perms`;");
            getPlayers = c.prepareStatement("SELECT * FROM `" + prefix + "players`;");
            getPolicies = c.prepareStatement("SELECT * FROM `" + prefix + "policies`;");
            getInvites = c.prepareStatement("SELECT * FROM `" + prefix + "invitations`;");
            getFlags = c.prepareStatement("SELECT * FROM `" + prefix + "flags`;");
            getAccess = c.prepareStatement("SELECT * FROM `" + prefix + "access`;");

            clans = getClans.executeQuery();
            warps = getWarps.executeQuery();
            claims = getClaims.executeQuery();
            perms = getPerms.executeQuery();
            players = getPlayers.executeQuery();
            policies = getPolicies.executeQuery();
            invites = getInvites.executeQuery();
            flags = getFlags.executeQuery();
            access = getAccess.executeQuery();

            while (clans.next()) {
                cache.setClan(clans.getString("name"), new CacheClan(clans.getString("name"),
                        clans.getString("description"), clans.getLong("created"), clans.getDouble("powerboost"),
                        clans.getDouble("homex"), clans.getDouble("homey"), clans.getDouble("homez"),
                        clans.getFloat("homeyaw"), clans.getFloat("homepitch"), clans.getString("homeworld")));
            }

            while (warps.next()) {
                cache.addWarp(warps.getString("clan"), new CacheWarp(warps.getString("clan"), warps.getString("name"),
                        warps.getString("world"), warps.getDouble("x"), warps.getDouble("y"),
                        warps.getDouble("z"), warps.getFloat("yaw"), warps.getFloat("pitch")));
            }

            while (claims.next()) {
                cache.addClaim(claims.getString("clan"), new CacheClaim(claims.getString("clan"), claims.getInt("x"), claims.getInt("z")));
            }

            while (perms.next()) {
                try {
                    cache.addPerm(perms.getString("clan"), new CachePerm(perms.getString("clan"), Rank.valueOf(perms.getString("rank").toUpperCase()),
                            perms.getString("perm"), perms.getBoolean("value")));
                } catch (IllegalArgumentException e) {
                    cache.addPerm(perms.getString("clan"), new CachePerm(perms.getString("clan"), Policy.valueOf(perms.getString("rank").toUpperCase()),
                            perms.getString("perm"), perms.getBoolean("value")));
                }
            }

            while (players.next()) {

                String rank = players.getString("rank");

                cache.setPlayer(UUID.fromString(players.getString("uuid")), new CachePlayer(UUID.fromString(players.getString("uuid")),
                        players.getString("name"), players.getString("clan"), rank == null ? Rank.NEWBIE : Rank.valueOf(rank),
                        players.getString("title"), players.getDouble("power"), players.getDouble("maxpower"), ChatMode.valueOf(players.getString("chatmode"))));
            }

            while (policies.next()) {
                cache.addPolicy(new CachePolicy(policies.getString("clan1"), policies.getString("clan2"), Policy.valueOf(policies.getString("policy").toUpperCase())));
            }

            while (invites.next()) {
                cache.addInvitation(UUID.fromString(invites.getString("uuid")), new CacheInvitation(UUID.fromString(invites.getString("uuid")), invites.getString("clan")));
            }

            while (flags.next()) {
                cache.addFlag(flags.getString("clan"), new CacheFlag(flags.getString("clan"), Flag.valueOf(flags.getString("flag").toUpperCase()), flags.getBoolean("value")));
            }

            while (access.next()) {
                cache.addAccess(access.getString("clan"), new CacheAccess(access.getString("clan"), access.getInt("x"), access.getInt("z"), access.getString("target")));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(getClans, getWarps, getClaims, getPerms, getPlayers, getPolicies, getInvites, getFlags, getAccess, clans, warps, claims, perms, players, policies, invites, flags, access);
        }
    }

    @Override
    public synchronized void claim(String clan, Chunk c) throws DatabaseException {

        PreparedStatement get = null, set = null;
        ResultSet rs = null;

        try {

            reopen();

            get = this.c.prepareStatement("SELECT `clan` FROM `" + prefix + "claims` WHERE `x` = ? AND `z` = ?;");
            get.setInt(1, c.getX());
            get.setInt(2, c.getZ());
            rs = get.executeQuery();

            if (rs.next()) {

                set = this.c.prepareStatement("UPDATE `" + prefix + "claims` SET `clan` = ? WHERE `x` = ? AND `z` = ?;");
                set.setString(1, clan);
                set.setInt(2, c.getX());
                set.setInt(3, c.getZ());

            } else {

                set = this.c.prepareStatement("INSERT INTO `" + prefix + "claims` (`clan`, `x`, `z`) VALUES (?, ?, ?);");
                set.setString(1, clan);
                set.setInt(2, c.getX());
                set.setInt(3, c.getZ());

            }

            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, set, rs);
        }
    }

    @Override
    public synchronized void unclaim(String clan, Chunk c) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            reopen();

            delete = this.c.prepareStatement("DELETE FROM `" + prefix + "claims` WHERE `clan` = ? AND `x` = ? AND `z` = ?;");
            delete.setString(1, clan);
            delete.setInt(2, c.getX());
            delete.setInt(3, c.getZ());
            delete.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(delete);
        }
    }

    @Override
    public synchronized String getChunkOwner(Chunk c) throws DatabaseException {

        return getChunkOwner(c.getX(), c.getZ());

    }

    @Override
    public String getChunkOwner(int x, int z) throws DatabaseException {

        String clan = getUnclaimedName();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = this.c.prepareStatement("SELECT `clan` FROM `" + prefix + "claims` WHERE `x` = ? AND `z` = ?;");
            get.setInt(1, x);
            get.setInt(2, z);
            rs = get.executeQuery();

            if (rs.next()) {
                clan = rs.getString("clan");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return clan;
    }

    @Override
    public Table<Integer, Integer, String> getChunkOwners(int lowX, int highX, int lowZ, int highZ) throws DatabaseException {

        Table<Integer, Integer, String> out = HashBasedTable.create();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `clan`, `x`, `z` FROM `" + prefix + "claims` WHERE `x` >= ? AND `x` <= ? AND `z` >= ? AND `z` <= ?;");
            get.setInt(1, lowX);
            get.setInt(2, highZ);
            get.setInt(3, lowZ);
            get.setInt(4, highZ);
            rs = get.executeQuery();

            while (rs.next()) {

                out.put(rs.getInt("x"), rs.getInt("z"), rs.getString("clan"));

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;

    }

    @Override
    public synchronized boolean createClan(Player p, String name) throws DatabaseException {

        if (name.equals(getUnclaimedName()) || name.equals(getWarName()) || name.equals(getSafeName()))
            return false;

        PreparedStatement find = null, create = null, player = null, perms = null, flags = null;
        ResultSet exists = null;

        try {

            reopen();

            find = c.prepareStatement("SELECT `name` FROM `" + prefix + "clans` WHERE `name` = ?;");
            find.setString(1, name);
            exists = find.executeQuery();

            if (exists.next()) return false;

            create = c.prepareStatement("INSERT INTO `" + prefix + "clans` (name, description, created, powerboost, homex, homey, homez, homeyaw, homepitch, homeworld) VALUES (?, ?, ?, ?, NULL, NULL, NULL, NULL, NULL, NULL);");
            create.setString(1, name);
            create.setString(2, "");
            create.setLong(3, System.currentTimeMillis());
            create.setDouble(4, 0.0D);
            create.executeUpdate();

            player = c.prepareStatement("UPDATE `" + prefix + "players` SET `clan` = ?, `rank` = ? WHERE `uuid` = ?;");
            player.setString(1, name);
            player.setString(2, "LEADER");
            player.setString(3, p.getUniqueId().toString());
            player.executeUpdate();

            perms = c.prepareStatement("INSERT INTO `" + prefix + "perms` (clan, rank, perm, value) VALUES (?, ?, ?, ?)");

            for (String key : ClansMain.getInstance().getConfig().getConfigurationSection("defaultperms").getKeys(false)) {

                for (String val : ClansMain.getInstance().getConfig().getConfigurationSection("defaultperms." + key).getKeys(false)) {

                    perms.setString(1, name);
                    perms.setString(2, key);
                    perms.setString(3, val);
                    perms.setBoolean(4, ClansMain.getInstance().getConfig().getBoolean("defaultperms." + key + "." + val));

                    perms.addBatch();

                }
            }

            perms.executeBatch();

            flags = c.prepareStatement("INSERT INTO `" + prefix + "flags` (clan, flag, value) VALUES (?, ?, ?);");

            for (String key : ClansMain.getInstance().getConfig().getConfigurationSection("flags").getKeys(false)) {

                flags.setString(1, name);
                flags.setString(2, key);
                flags.setBoolean(3, ClansMain.getInstance().getConfig().getBoolean("flags." + key + ".default"));

                flags.addBatch();
            }

            flags.executeBatch();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(find, create, player, perms, exists, flags);
        }

        return true;
    }

    @Override
    public synchronized void deleteClan(String name) throws DatabaseException {

        PreparedStatement deleteClan = null, deleteWarps = null, deleteClaims = null, deletePerms = null, deletePlayers = null, deletepolicies = null, deleteInvites = null, deleteFlags = null, deleteAccess = null;

        try {

            reopen();

            deleteClan = c.prepareStatement("DELETE FROM `" + prefix + "clans` WHERE `name` = ?;");
            deleteClan.setString(1, name);

            deleteWarps = c.prepareStatement("DELETE FROM `" + prefix + "warps` WHERE `clan` = ?;");
            deleteWarps.setString(1, name);

            deleteClaims = c.prepareStatement("DELETE FROM `" + prefix + "claims` WHERE `clan` = ?;");
            deleteClaims.setString(1, name);

            deletePerms = c.prepareStatement("DELETE FROM `" + prefix + "perms` WHERE `clan` = ?;");
            deletePerms.setString(1, name);

            deletePlayers = c.prepareStatement("UPDATE `" + prefix + "players` SET `clan` = NULL, `rank` = NULL, `title` = ?, `chatmode` = ? WHERE `clan` = ?;");
            deletePlayers.setString(1, "");
            deletePlayers.setString(2, "PUBLIC");
            deletePlayers.setString(3, name);

            deletepolicies = c.prepareStatement("DELETE FROM `" + prefix + "policies` WHERE `clan1` = ? OR `clan2` = ?;");
            deletepolicies.setString(1, name);
            deletepolicies.setString(2, name);

            deleteInvites = c.prepareStatement("DELETE FROM `" + prefix + "invitations` WHERE `clan` = ?;");
            deleteInvites.setString(1, name);

            deleteFlags = c.prepareStatement("DELETE FROM `" + prefix + "flags` WHERE `clan` = ?;");
            deleteFlags.setString(1, name);

            deleteAccess = c.prepareStatement("DELETE FROM `" + prefix + "access` WHERE `clan` = ?;");
            deleteAccess.setString(1, name);

            deleteClan.executeUpdate();
            deleteWarps.executeUpdate();
            deleteClaims.executeUpdate();
            deletePerms.executeUpdate();
            deletePlayers.executeUpdate();
            deletepolicies.executeUpdate();
            deleteInvites.executeUpdate();
            deleteFlags.executeUpdate();
            deleteAccess.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(deleteClan, deleteWarps, deleteClaims, deletePerms, deletePlayers, deletepolicies, deleteInvites, deleteFlags, deleteAccess);
        }
    }

    @Override
    public synchronized String getClan(UUID u) throws DatabaseException {

        String clan = getUnclaimedName();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `clan` FROM `" + prefix + "players` WHERE `uuid` = ?;");

            get.setString(1, u.toString());

            rs = get.executeQuery();

            if (rs.next() && rs.getString("clan") != null) {

                clan = rs.getString("clan");

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return clan;
    }

    @Override
    public synchronized String getClan(Player p) throws DatabaseException {
        return getClan(p.getUniqueId());
    }

    private synchronized Map<String, Boolean> getClanPermissions(String clan, String rank) throws DatabaseException {

        Map<String, Boolean> map = new HashMap<>();

        //default perms
        map.put("map", true);
        map.put("player", true);
        map.put("seechunk", true);
        map.put("create", true);
        map.put("join", false);
        map.put("leave", true);

        if (clan.equals(getUnclaimedName()) || clan.equals(getWarName())) {

            //idk a better way to do this

            map.put("access", false);
            map.put("home", false);
            map.put("claim", false);
            map.put("sethome", false);
            map.put("delhome", false);
            map.put("unclaim", false);
            map.put("ally", false);
            map.put("promote", false);
            map.put("truce", false);
            map.put("name", false);
            map.put("neutral", false);
            map.put("perm", false);
            map.put("kick", false);
            map.put("leader", false);
            map.put("demote", false);
            map.put("desc", false);
            map.put("disband", false);
            map.put("enemy", false);
            map.put("flag", false);
            map.put("invite", false);
            map.put("setwarp", false);
            map.put("warp", false);
            map.put("join", true);
            map.put("leave", false);
            map.put("build", clan.equals(getUnclaimedName()));
            map.put("door", true);
            map.put("interact", true);
            map.put("container", true);
            map.put("title", false);

            return map;

        } else if (clan.equals(getSafeName())) {

            map.put("access", false);
            map.put("home", false);
            map.put("claim", false);
            map.put("sethome", false);
            map.put("delhome", false);
            map.put("unclaim", false);
            map.put("ally", false);
            map.put("promote", false);
            map.put("truce", false);
            map.put("name", false);
            map.put("neutral", false);
            map.put("perm", false);
            map.put("kick", false);
            map.put("leader", false);
            map.put("demote", false);
            map.put("desc", false);
            map.put("disband", false);
            map.put("enemy", false);
            map.put("flag", false);
            map.put("invite", false);
            map.put("setwarp", false);
            map.put("warp", false);
            map.put("join", false);
            map.put("leave", false);
            map.put("build", clan.equals(getUnclaimedName()));
            map.put("door", true);
            map.put("interact", false);
            map.put("container", false);
            map.put("title", false);

            return map;

        }

        PreparedStatement getPerms = null;
        ResultSet perms = null;

        try {

            reopen();

            getPerms = c.prepareStatement("SELECT `perm`,`value` FROM `" + prefix + "perms` WHERE `clan` = ? and `rank` = ?;");
            getPerms.setString(1, clan);
            getPerms.setString(2, rank);

            perms = getPerms.executeQuery();

            while (perms.next()) {

                map.put(perms.getString("perm"), perms.getBoolean("value"));

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(getPerms, perms);
        }

        return map;
    }

    @Override
    public synchronized Map<String, Boolean> getClanPermissions(String clan, Rank rank) throws DatabaseException {
        return getClanPermissions(clan, rank.name());
    }

    @Override
    public synchronized Map<String, Boolean> getClanPermissions(String clan, Policy policy) throws DatabaseException {
        return getClanPermissions(clan, policy.name());
    }

    @Override
    public void setClanPermission(String clan, Rank rank, String permission, boolean value) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            reopen();

            insert = c.prepareStatement("UPDATE `" + prefix + "perms` SET `value` = ? WHERE `clan` = ? AND `rank` = ? AND `perm` = ?;");
            insert.setBoolean(1, value);
            insert.setString(2, clan);
            insert.setString(3, rank.name());
            insert.setString(4, permission);

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public void setClanPermission(String clan, Policy policy, String permission, boolean value) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            reopen();

            insert = c.prepareStatement("UPDATE `" + prefix + "perms` SET `value` = ? WHERE `clan` = ? AND `rank` = ? AND `perm` = ?;");
            insert.setBoolean(1, value);
            insert.setString(2, clan);
            insert.setString(3, policy.name());
            insert.setString(4, permission);

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public synchronized Rank getClanRank(UUID u, String clan) throws DatabaseException {

        PreparedStatement getRank = null;
        ResultSet rank = null;

        Rank out = null;

        try {

            reopen();

            getRank = c.prepareStatement("SELECT `rank` FROM `" + prefix + "players` WHERE `uuid` = ? AND `clan` = ?;");
            getRank.setString(1, u.toString());
            getRank.setString(2, clan);

            rank = getRank.executeQuery();

            if (rank.next()) {

                String rankout = rank.getString("rank");

                if (rankout != null) out = Rank.valueOf(rankout);
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(getRank, rank);
        }

        return out;
    }

    @Override
    public synchronized Rank getClanRank(String name, String clan) throws DatabaseException {

        PreparedStatement getRank = null;
        ResultSet rank = null;

        Rank out = null;

        try {

            reopen();

            getRank = c.prepareStatement("SELECT `rank` FROM `" + prefix + "players` WHERE `name` = ? AND `clan` = ?;");
            getRank.setString(1, name);
            getRank.setString(2, clan);

            rank = getRank.executeQuery();

            if (rank.next()) out = Rank.valueOf(rank.getString("rank"));

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(getRank, rank);
        }

        return out;
    }

    @Override
    public synchronized Policy getPolicy(String clan1, String clan2) throws DatabaseException {

        if (clan1.equals(clan2)) return Policy.CLAN;

        if (getFlag(clan1, Flag.PEACEFUL) || getFlag(clan2, Flag.PEACEFUL)) return Policy.PEACEFUL;

        Policy out = Policy.NEUTRAL;

        PreparedStatement get1 = null, get2 = null;
        ResultSet policy1 = null, policy2 = null;

        try {

            Policy result1 = Policy.NEUTRAL, result2 = Policy.NEUTRAL;

            reopen();

            get1 = c.prepareStatement("SELECT `policy` FROM `" + prefix + "policies` WHERE (`clan1` = ? AND `clan2` = ?);");
            get1.setString(1, clan1);
            get1.setString(2, clan2);

            policy1 = get1.executeQuery();

            if (policy1.next()) {

                result1 = Policy.valueOf(policy1.getString("policy"));

            }

            get2 = c.prepareStatement("SELECT `policy` FROM `" + prefix + "policies` WHERE (`clan1` = ? AND `clan2` = ?);");
            get2.setString(1, clan2);
            get2.setString(2, clan1);

            policy2 = get1.executeQuery();

            if (policy2.next()) {

                result2 = Policy.valueOf(policy2.getString("policy"));

            }

            if (result1 == Policy.ENEMY || result2 == Policy.ENEMY) {

                out = Policy.ENEMY;

            } else if (result1 == Policy.ALLY && result2 == Policy.ALLY) {

                out = Policy.ALLY;

            } else if ((result1 == Policy.TRUCE && result2 == Policy.TRUCE) || (result1 == Policy.ALLY && result2 == Policy.TRUCE) || (result1 == Policy.TRUCE && result2 == Policy.ALLY)) {

                out = Policy.TRUCE;

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get1, get2, policy1, policy2);
        }

        return out;
    }

    @Override
    public void setPolicy(String clan1, String clan2, Policy policy) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            reopen();

            insert = c.prepareStatement("INSERT INTO `" + prefix + "policies` (`clan1`,`clan2`,`policy`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `policy` = VALUES(`policy`);");
            insert.setString(1, clan1);
            insert.setString(2, clan2);
            insert.setString(3, policy.name());

            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }
    }

    @Override
    public synchronized String clanExists(String name) throws DatabaseException {

        String clan = "";

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `name` FROM `" + prefix + "clans` WHERE `name` = ?;");

            get.setString(1, name);

            rs = get.executeQuery();

            if (rs.next()) {
                clan = rs.getString("name");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return clan;
    }

    @Override
    public synchronized List<UUID> getPlayers(String clan) throws DatabaseException {

        List<UUID> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `uuid` FROM `" + prefix + "players` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {
                out.add(UUID.fromString(rs.getString("uuid")));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized List<String> getPlayerNames(String clan) throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `name` FROM `" + prefix + "players` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {
                out.add(rs.getString("name"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized String getDesc(String clan) throws DatabaseException {

        String desc = "";

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `description` FROM `" + prefix + "clans` WHERE `name` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) {
                desc = rs.getString("description");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return desc;
    }

    @Override
    public synchronized void setDesc(String clan, String description) throws DatabaseException {

        PreparedStatement set = null;

        try {

            reopen();

            set = c.prepareStatement("UPDATE `" + prefix + "clans` SET `description` = ? WHERE `name` = ?;");
            set.setString(1, description);
            set.setString(2, clan);
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(set);
        }
    }

    @Override
    public synchronized long getCreateDate(String clan) throws DatabaseException {

        long date = 0L;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `created` FROM `" + prefix + "clans` WHERE `name` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) {

                date = rs.getLong("created");

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return date;
    }

    @Override
    public synchronized int getClaimedLand(String clan) throws DatabaseException {

        int total = 0;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT COUNT(*) AS `count` FROM `" + prefix + "claims` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) {
                total = rs.getInt("count");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return total;
    }

    @Override
    public synchronized List<String> getClanPolicies(String clan, Policy policy) throws DatabaseException {

        List<String> list1 = new ArrayList<>(), list2 = new ArrayList<>(), out = new ArrayList<>();

        PreparedStatement get = null, otherget = null;
        ResultSet rs = null, otherrs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `clan2` FROM `" + prefix + "policies` WHERE `clan1` = ? AND `policy` = ?;");
            get.setString(1, clan);
            get.setString(2, policy.toString());
            rs = get.executeQuery();

            while (rs.next()) list1.add(rs.getString("clan2"));

            otherget = c.prepareStatement("SELECT `clan1` FROM `" + prefix + "policies` WHERE `clan2` = ? AND `policy` = ?;");
            otherget.setString(1, clan);
            otherget.setString(2, policy.toString());
            otherrs = otherget.executeQuery();

            while (otherrs.next()) list2.add(otherrs.getString("clan1"));

            list1.removeIf(s -> !list2.contains(s));
            list2.removeIf(s -> !list1.contains(s));

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs, otherget, otherrs);
        }

        return out;
    }

    @Override
    public synchronized double getClanPower(String clan) throws DatabaseException {

        double power = 0.0D;

        PreparedStatement get = null, getplayers = null;
        ResultSet rs = null, players = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `powerboost` FROM `" + prefix + "clans` WHERE `name` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) power = rs.getDouble("powerboost");

            getplayers = c.prepareStatement("SELECT `power` FROM `" + prefix + "players` WHERE `clan` = ?;");
            getplayers.setString(1, clan);
            players = getplayers.executeQuery();

            while (players.next()) {

                power = power + players.getDouble("power");

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs, getplayers, players);
        }

        return power;
    }

    @Override
    public synchronized double getClanMaxPower(String clan) throws DatabaseException {

        double power = 0.0D;

        PreparedStatement get = null, getplayers = null;
        ResultSet rs = null, players = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `powerboost` FROM `" + prefix + "clans` WHERE `name` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) power = rs.getDouble("powerboost");

            getplayers = c.prepareStatement("SELECT `maxpower` FROM `" + prefix + "players` WHERE `clan` = ?;");
            getplayers.setString(1, clan);
            players = getplayers.executeQuery();

            while (players.next()) {

                power = power + players.getDouble("maxpower");

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs, getplayers, players);
        }

        return power;
    }

    @Override
    public synchronized void addClanMaxPower(String clan, double power) throws DatabaseException {

        PreparedStatement set = null;

        try {

            reopen();

            set = c.prepareStatement("UPDATE `" + prefix + "clans` SET `powerboost` = (`powerboost` + ?) where `name` = ?;");
            set.setDouble(1, power);
            set.setString(2, clan);
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(set);
        }
    }

    @Override
    public synchronized double getPlayerPower(UUID id) throws DatabaseException {

        double power = 0.0D;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `power` FROM `" + prefix + "players` WHERE `uuid` = ?;");
            get.setString(1, id.toString());
            rs = get.executeQuery();

            if (rs.next()) power = rs.getDouble("power");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return power;
    }

    @Override
    public synchronized double getPlayerMaxPower(UUID id) throws DatabaseException {

        double power = 0.0D;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `maxpower` FROM `" + prefix + "players` WHERE `uuid` = ?;");
            get.setString(1, id.toString());
            rs = get.executeQuery();

            if (rs.next()) power = rs.getDouble("maxpower");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return power;
    }

    @Override
    public synchronized void setPlayerPower(UUID id, double power) throws DatabaseException {

        PreparedStatement set = null;

        try {

            reopen();

            set = c.prepareStatement("UPDATE `" + prefix + "players` SET `power` = ? where `uuid` = ?;");
            set.setDouble(1, power);
            set.setString(2, id.toString());
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(set);
        }
    }

    @Override
    public synchronized void addPlayerMaxPower(UUID id, double power) throws DatabaseException {

        PreparedStatement set = null;

        try {

            reopen();

            set = c.prepareStatement("UPDATE `" + prefix + "players` SET `maxpower` = (`maxpower` + ?) where `uuid` = ?;");
            set.setDouble(1, power);
            set.setString(2, id.toString());
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(set);
        }
    }

    //createPlayers = c.prepareStatement("CREATE TABLE IF NOT EXISTS `" + prefix + "players` (uuid CHAR(36), name VARCHAR(16), clan VARCHAR(20), "
                    //+ "rank VARCHAR(7), title VARCHAR(16), power DOUBLE, maxpower DOUBLE, chatmode VARCHAR(6), PRIMARY KEY (`uuid`));");

    @Override
    public synchronized String getPlayerTitle(UUID id) throws DatabaseException {

        PreparedStatement get = null;
        ResultSet rs = null;

        String out = "";

        try {

            reopen();

            get = c.prepareStatement("SELECT `title` FROM `" + prefix + "players` WHERE `uuid` = ?;");
            get.setString(1, id.toString());
            rs = get.executeQuery();

            if (rs.next()) out = rs.getString("title");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized String getPlayerTitleByName(String name) throws DatabaseException {

        PreparedStatement get = null;
        ResultSet rs = null;

        String out = "";

        try {

            reopen();

            get = c.prepareStatement("SELECT `title` FROM `" + prefix + "players` WHERE `name` = ?;");
            get.setString(1, name);
            rs = get.executeQuery();

            if (rs.next()) out = rs.getString("title");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void setPlayerTitle(UUID id, String title) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("UPDATE `" + prefix + "players` SET `title` = ? WHERE `uuid` = ?;");
            update.setString(1, title);
            update.setString(2, id.toString());
            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public synchronized void updatePlayer(Player p) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("INSERT INTO `" + prefix + "players` (`uuid`, `name`, `clan`, `rank`, `title`, `power`, `maxpower`, `chatmode`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `name` = ?;");
            update.setString(1, p.getUniqueId().toString());
            update.setString(2, p.getName());
            update.setNull(3, Types.VARCHAR);
            update.setNull(4, Types.VARCHAR);
            update.setString(5, "");
            update.setDouble(6, 0.0D);
            update.setDouble(7, 10.0D);
            update.setString(8, "PUBLIC");
            update.setString(9, p.getName());

            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public synchronized void setHome(String clan, Location loc) throws DatabaseException {

        boolean isnull = false;

        double x = 0.0D, y = 0.0D, z = 0.0D;
        float yaw = 0.0F, pitch = 0.0F;
        String world = "";

        if (loc == null) {

            isnull = true;

        } else {

            x = loc.getX();
            y = loc.getY();
            z = loc.getZ();
            yaw = loc.getYaw();
            pitch = loc.getPitch();
            world = loc.getWorld().getName();

        }

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("UPDATE `" + prefix + "clans` SET `homex` = ?, `homey` = ?, `homez` = ?, `homeyaw` = ?, `homepitch` = ?, `homeworld` = ? WHERE `name` = ?");

            if (isnull) {
                update.setNull(1, Types.DOUBLE);
                update.setNull(2, Types.DOUBLE);
                update.setNull(3, Types.DOUBLE);
                update.setNull(4, Types.FLOAT);
                update.setNull(5, Types.FLOAT);
                update.setNull(6, Types.VARCHAR);
            } else {
                update.setDouble(1, x);
                //noinspection SuspiciousNameCombination
                update.setDouble(2, y);
                update.setDouble(3, z);
                update.setFloat(4, yaw);
                update.setFloat(5, pitch);
                update.setString(6, world);
            }
            update.setString(7, clan);

            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public synchronized Location getHome(String clan) throws DatabaseException {

        Location home = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `homex`,`homey`,`homez`,`homeyaw`,`homepitch`,`homeworld` FROM `" + prefix + "clans` WHERE `name` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            if (rs.next()) {

                double x, y, z;
                float yaw, pitch;
                String world;
                boolean isnull = false;

                if ((x = rs.getDouble("homex")) == 0.0D) isnull = true;
                if ((y = rs.getDouble("homey")) == 0.0D) isnull = true;
                if ((z = rs.getDouble("homez")) == 0.0D) isnull = true;
                if ((yaw = rs.getFloat("homeyaw")) == 0.0F) isnull = true;
                if ((pitch = rs.getFloat("homepitch")) == 0.0F) isnull = true;
                if ((world = rs.getString("homeworld")) == null) isnull = true;

                if (!isnull) {
                    home = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
                }

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return home;
    }

    @Override
    public synchronized boolean playerExists(UUID id) throws DatabaseException {

        boolean out;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `uuid` FROM `" + prefix + "players` WHERE `uuid` = ?;");
            get.setString(1, id.toString());
            rs = get.executeQuery();

            out = rs.next();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized boolean setWarp(String clan, String name, Location loc) throws DatabaseException {

        PreparedStatement check = null, set = null;
        ResultSet rs = null;

        try {

            reopen();

            check = c.prepareStatement("SELECT `clan` FROM `" + prefix + "warps` WHERE `clan` = ? AND `name` = ?;");
            check.setString(1, clan);
            check.setString(2, name);
            rs = check.executeQuery();

            if (rs.next()) return false;

            set = c.prepareStatement("INSERT INTO `" + prefix + "warps` (clan, name, world, x, y, z, yaw, pitch) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
            set.setString(1, clan);
            set.setString(2, name);
            set.setString(3, loc.getWorld().getName());
            set.setDouble(4, loc.getX());
            set.setDouble(5, loc.getY());
            set.setDouble(6, loc.getZ());
            set.setFloat(7, loc.getYaw());
            set.setFloat(8, loc.getPitch());
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(check, set, rs);
        }

        return true;
    }

    @Override
    public synchronized Location getWarp(String clan, String name) throws DatabaseException {

        Location out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `world`,`x`,`y`,`z`,`yaw`,`pitch` FROM `" + prefix + "warps` WHERE `clan` = ? AND `name` = ?;");
            get.setString(1, clan);
            get.setString(2, name);
            rs = get.executeQuery();

            if (rs.next()) {
                out = new Location(Bukkit.getWorld(rs.getString("world")), rs.getDouble("x"), rs.getDouble("y"),
                        rs.getDouble("z"), rs.getFloat("yaw"), rs.getFloat("pitch"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void delWarp(String clan, String name) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            reopen();

            delete = c.prepareStatement("DELETE FROM `" + prefix + "warps` WHERE `clan` = ? AND `name` = ?;");
            delete.setString(1, clan);
            delete.setString(2, name);
            delete.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(delete);
        }
    }

    @Override
    public synchronized List<String> getWarps(String clan) throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `name` FROM `" + prefix + "warps` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {
                out.add(rs.getString("name"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void setRank(UUID u, Rank rank) throws DatabaseException {

        PreparedStatement set = null;

        try {

            reopen();

            set = c.prepareStatement("UPDATE `" + prefix + "players` SET `rank` = ? WHERE `uuid` = ? AND `clan` IS NOT NULL;");
            set.setString(1, rank.toString());
            set.setString(2, u.toString());
            set.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(set);
        }
    }

    @Override
    public synchronized String getLeader(String clan) throws DatabaseException {

        String out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `name` FROM `" + prefix + "players` WHERE `clan` = ? AND `rank` = ?;");
            get.setString(1, clan);
            get.setString(2, "LEADER");
            rs = get.executeQuery();

            if (rs.next()) out = rs.getString("name");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized Table<Rank, String, Boolean> getMemberPerms(String clan) throws DatabaseException {

        Table<Rank, String, Boolean> out = HashBasedTable.create();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `rank`,`perm`,`value` FROM `" + prefix + "perms` WHERE `clan` = ? " +
                    "AND (`rank` = 'LEADER' OR `rank` = 'CAPTAIN' OR `rank` = 'MEMBER' OR `rank` = 'NEWBIE');");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {
                out.put(Rank.valueOf(rs.getString("rank")), rs.getString("perm"), rs.getBoolean("value"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized Table<Policy, String, Boolean> getForeignPerms(String clan) throws DatabaseException {

        Table<Policy, String, Boolean> out = HashBasedTable.create();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `rank`,`perm`,`value` FROM `" + prefix + "perms` WHERE `clan` = ? " +
                    "AND (`rank` = 'ALLY' OR `rank` = 'TRUCE' OR `rank` = 'NEUTRAL' OR `rank` = 'ENEMY');");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {
                out.put(Policy.valueOf(rs.getString("rank")), rs.getString("perm"), rs.getBoolean("value"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void addInvite(UUID u, String clan) throws DatabaseException {

        PreparedStatement check = null, add = null;
        ResultSet rs = null;

        try {

            reopen();

            check = c.prepareStatement("SELECT * FROM `" + prefix + "invitations` WHERE `uuid` = ? AND `clan` = ?;");
            check.setString(1, u.toString());
            check.setString(2, clan);
            rs = check.executeQuery();

            if (!rs.next()) {

                add = c.prepareStatement("INSERT INTO `" + prefix + "invitations` (uuid, clan) VALUES (?, ?);");
                add.setString(1, u.toString());
                add.setString(2, clan);
                add.executeUpdate();

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(check, add, rs);
        }
    }

    @Override
    public synchronized void removeInvite(UUID u, String clan) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            reopen();

            delete = c.prepareStatement("DELETE FROM `" + prefix + "invitations` WHERE `uuid` = ? AND `clan` = ?;");
            delete.setString(1, u.toString());
            delete.setString(2, clan);
            delete.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(delete);
        }
    }

    @Override
    public synchronized List<String> getInvites(UUID u) throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `clan` FROM `" + prefix + "invitations` WHERE `uuid` = ?;");
            get.setString(1, u.toString());
            rs = get.executeQuery();

            while (rs.next()) {

                out.add(rs.getString("clan"));

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized List<UUID> getClanInvites(String clan) throws DatabaseException {

        List<UUID> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `uuid` FROM `" + prefix + "invitations` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) {

                out.add(UUID.fromString(rs.getString("uuid")));

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void kickPlayer(String clan, UUID target) throws DatabaseException {

        PreparedStatement removePlayer = null, checkMembers = null;
        ResultSet members = null;

        try {

            reopen();

            removePlayer = c.prepareStatement("UPDATE `" + prefix + "players` SET `clan` = ?, `rank` = ?, `title` = ?, `chatmode` = ? WHERE `uuid` = ?;");
            removePlayer.setNull(1, Types.VARCHAR);
            removePlayer.setNull(2, Types.VARCHAR);
            removePlayer.setString(3, "");
            removePlayer.setString(4, "PUBLIC");
            removePlayer.setString(5, target.toString());
            removePlayer.executeUpdate();

            checkMembers = c.prepareStatement("SELECT `uuid` FROM `" + prefix + "players` WHERE `clan` = ?;");
            checkMembers.setString(1, clan);
            members = checkMembers.executeQuery();

            if (!members.next() && !getFlag(clan, Flag.PERMANENT)) { //empty clan

                deleteClan(clan);

            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(removePlayer, checkMembers, members);
        }
    }

    @Override
    public synchronized void setChatMode(UUID u, ChatMode mode) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("UPDATE `" + prefix + "players` SET `chatmode` = ? WHERE `uuid` = ?;");
            update.setString(1, mode.toString());
            update.setString(2, u.toString());
            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public synchronized ChatMode getChatMode(UUID u) throws DatabaseException {

        ChatMode out = ChatMode.PUBLIC;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `chatmode` FROM `" + prefix + "players` WHERE `uuid` = ?;");
            get.setString(1, u.toString());
            rs = get.executeQuery();

            if (rs.next()) out = ChatMode.valueOf(rs.getString("chatmode"));

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public int getClanCount() throws DatabaseException {

        int out;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT COUNT(*) AS `total` FROM `" + prefix + "clans`;");
            rs = get.executeQuery();

            rs.next();

            out = rs.getInt("total");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public String[] getClans(int page) throws DatabaseException {

        String[] out = new String[9];

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `name` FROM `" + prefix + "clans` LIMIT ?, 9;");
            get.setInt(1, (page - 1) * 10);
            rs = get.executeQuery();

            for (int i = 0; i < 9; i++) {

                if (rs.next()) {
                    out[i] = rs.getString("name");
                } else {
                    out[i] = "";
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public void setName(String clan, String newName) throws DatabaseException {

        PreparedStatement updateClans = null, updateWarps = null, updateClaims = null, updatePerms = null, updatePlayers = null, updatepolicies = null, updatepolicies2 = null, updateInvites = null, updateFlags = null, updateAccess = null;

        try {

            reopen();

            updateClans = c.prepareStatement("UPDATE `" + prefix + "clans` SET `name` = ? WHERE `name` = ?;");
            updateClans.setString(1, newName);
            updateClans.setString(2, clan);

            updateWarps = c.prepareStatement("UPDATE `" + prefix + "warps` SET `clan` = ? WHERE `clan` = ?;");
            updateWarps.setString(1, newName);
            updateWarps.setString(2, clan);

            updateClaims = c.prepareStatement("UPDATE `" + prefix + "claims` SET `clan` = ? WHERE `clan` = ?;");
            updateClaims.setString(1, newName);
            updateClaims.setString(2, clan);

            updatePerms = c.prepareStatement("UPDATE `" + prefix + "perms` SET `clan` = ? WHERE `clan` = ?;");
            updatePerms.setString(1, newName);
            updatePerms.setString(2, clan);

            updatePlayers = c.prepareStatement("UPDATE `" + prefix + "players` SET `clan` = ? WHERE `clan` = ?;");
            updatePlayers.setString(1, newName);
            updatePlayers.setString(2, clan);

            updatepolicies = c.prepareStatement("UPDATE `" + prefix + "policies` SET `clan1` = ? WHERE `clan1` = ?;");
            updatepolicies.setString(1, newName);
            updatepolicies.setString(2, clan);

            updatepolicies2 = c.prepareStatement("UPDATE `" + prefix + "policies` SET `clan2` = ? WHERE `clan2` = ?;");
            updatepolicies2.setString(1, newName);
            updatepolicies2.setString(2, clan);

            updateInvites = c.prepareStatement("UPDATE `" + prefix + "invitations` SET `clan` = ? WHERE `clan` = ?;");
            updateInvites.setString(1, newName);
            updateInvites.setString(2, clan);

            updateFlags = c.prepareStatement("UPDATE `" + prefix + "flags` SET `clan` = ? WHERE `clan` = ?;");
            updateFlags.setString(1, newName);
            updateFlags.setString(2, clan);

            updateAccess = c.prepareStatement("UPDATE `" + prefix + "access` SET `clan` = ? WHERE `clan` = ?;");
            updateAccess.setString(1, newName);
            updateAccess.setString(2, clan);


            updateClans.executeUpdate();
            updateWarps.executeUpdate();
            updateClaims.executeUpdate();
            updatePerms.executeUpdate();
            updatePlayers.executeUpdate();
            updatepolicies.executeUpdate();
            updateInvites.executeUpdate();
            updateFlags.executeUpdate();
            updateAccess.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(updateClans, updateWarps, updateClaims, updatePerms, updatePlayers, updatepolicies, updatepolicies2, updateInvites, updateFlags, updateAccess);
        }
    }

    @Override
    public boolean getFlag(String clan, Flag flag) throws DatabaseException {

        if (clan.equalsIgnoreCase(getUnclaimedName())) {

            return ClansMain.getInstance().getConfig().getBoolean("sysclans.unclaimed.flags." + flag.toString().toLowerCase());

        } else if (clan.equalsIgnoreCase(getWarName())) {

            return ClansMain.getInstance().getConfig().getBoolean("sysclans.war.flags." + flag.toString().toLowerCase());

        } else if (clan.equalsIgnoreCase(getSafeName())) {

            return ClansMain.getInstance().getConfig().getBoolean("sysclans.safe.flags." + flag.toString().toLowerCase());

        }

        boolean out = false;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `value` FROM `" + prefix + "flags` WHERE `clan` = ? AND `flag` = ?;");
            get.setString(1, clan);
            get.setString(2, flag.toString().toLowerCase());
            rs = get.executeQuery();

            if (rs.next()) out = rs.getBoolean("value");

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public void setFlag(String clan, Flag flag, boolean value) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("UPDATE `" + prefix + "flags` SET `value` = ? WHERE `clan` = ? AND `flag` = ?;");
            update.setBoolean(1, value);
            update.setString(2, clan);
            update.setString(3, flag.toString().toLowerCase());
            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public Map<Flag, Boolean> getFlags(String clan) throws DatabaseException {

        Map<Flag, Boolean> out = new HashMap<>();

        if (clan.equalsIgnoreCase(getUnclaimedName())) {

            for (String s : ClansMain.getInstance().getConfig().getConfigurationSection("sysclans.unclaimed.flags").getKeys(false)) {

                out.put(Flag.valueOf(s.toUpperCase()), ClansMain.getInstance().getConfig().getBoolean("sysclans.unclaimed.flags." + s));

            }

            return out;

        } else if (clan.equalsIgnoreCase(getWarName())) {

            for (String s : ClansMain.getInstance().getConfig().getConfigurationSection("sysclans.war.flags").getKeys(false)) {

                out.put(Flag.valueOf(s.toUpperCase()), ClansMain.getInstance().getConfig().getBoolean("sysclans.war.flags." + s));

            }

            return out;

        } else if (clan.equalsIgnoreCase(getSafeName())) {

            for (String s : ClansMain.getInstance().getConfig().getConfigurationSection("sysclans.safe.flags").getKeys(false)) {

                out.put(Flag.valueOf(s.toUpperCase()), ClansMain.getInstance().getConfig().getBoolean("sysclans.safe.flags." + s));

            }

            return out;

        }

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = c.prepareStatement("SELECT `flag`,`value` FROM `" + prefix + "flags` WHERE `clan` = ?;");
            get.setString(1, clan);
            rs = get.executeQuery();

            while (rs.next()) out.put(Flag.valueOf(rs.getString("flag").toUpperCase()), rs.getBoolean("value"));

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public void joinClan(String clan, UUID u) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            update = c.prepareStatement("UPDATE `" + prefix + "players` SET `clan` = (SELECT `name` FROM `" + prefix + "clans` WHERE `name` = ?), `rank` = ? WHERE `uuid` = ?;");
            update.setString(1, clan);
            update.setString(2, "NEWBIE");
            update.setString(3, u.toString());
            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public void setAccess(String clan, Chunk c, String target, boolean enabled) throws DatabaseException {

        PreparedStatement update = null;

        try {

            reopen();

            if (enabled) {

                update = this.c.prepareStatement("INSERT INTO `" + prefix + "access` (clan, x, z, target) VALUES (?, ?, ?, ?);");
                update.setString(1, clan);
                update.setInt(2, c.getX());
                update.setInt(3, c.getZ());
                update.setString(4, target);

            } else {

                update = this.c.prepareStatement("DELETE FROM `" + prefix + "access` WHERE `clan` = ? AND `x` = ? AND `z` = ? AND `target` = ?;");
                update.setString(1, clan);
                update.setInt(2, c.getX());
                update.setInt(3, c.getZ());
                update.setString(4, target);

            }

            update.executeUpdate();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(update);
        }
    }

    @Override
    public boolean getAccess(String clan, Chunk c, String target) throws DatabaseException {

        boolean out;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = this.c.prepareStatement("SELECT * FROM `" + prefix + "access` WHERE `clan` = ? AND `x` = ? AND `z` = ? AND `target` = ?;");
            get.setString(1, clan);
            get.setInt(2, c.getX());
            get.setInt(3, c.getZ());
            get.setString(4, target);
            rs = get.executeQuery();

            out = rs.next();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public List<String> getClanAccess(String clan, Chunk c) throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = this.c.prepareStatement("SELECT `target` FROM `" + prefix + "access` WHERE `clan` = ? AND `x` = ? AND `z` = ? AND CHAR_LENGTH(`target`) < 36");
            get.setString(1, clan);
            get.setInt(2, c.getX());
            get.setInt(3, c.getZ());

            rs = get.executeQuery();

            while (rs.next()) {
                out.add(rs.getString("target"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public List<String> getPlayerAccess(String clan, Chunk c) throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            reopen();

            get = this.c.prepareStatement("SELECT `target` FROM `" + prefix + "access` WHERE `clan` = ? AND `x` = ? AND `z` = ? AND CHAR_LENGTH(`target`) = 36");
            get.setString(1, clan);
            get.setInt(2, c.getX());
            get.setInt(3, c.getZ());

            rs = get.executeQuery();

            while (rs.next()) {
                out.add(rs.getString("target"));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            closeSilently(get, rs);
        }

        return out;
    }

    @Override
    public synchronized void close() throws DatabaseException {

        try {
            c.close();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }
}
