package com.kaixeleron.clans.data;

public enum ChatMode {

    PUBLIC,
    CLAN,
    ALLY,
    TRUCE,
    ENEMY

}
