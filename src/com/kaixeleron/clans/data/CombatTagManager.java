package com.kaixeleron.clans.data;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CombatTagManager {

    private boolean active;

    private long seconds;

    private final Set<Player> tagged;

    private final Map<Player, BukkitRunnable> runnables;

    public CombatTagManager(boolean active, long seconds) {

        this.active = active;
        this.seconds = seconds;

        tagged = new HashSet<>();

        runnables = new HashMap<>();

    }

    private void scheduleRemoval(final Player player) {

        if (runnables.containsKey(player)) {

            runnables.get(player).cancel();
            runnables.remove(player);

        }

        BukkitRunnable remove = new BukkitRunnable() {

            @Override
            public void run() {

                player.sendMessage(ChatColor.GREEN + "You are no longer in combat.");

            }

        };

        runnables.put(player, remove);

        remove.runTaskLater(ClansMain.getInstance(), seconds * 20L);

    }

    public void tagPlayer(Player player) {

        if (!PermissionManager.getInstance().isBypassing(player)) {

            if (!tagged.contains(player)) {

                tagged.add(player);

                player.sendMessage(ChatColor.RED + "You are now in combat! Do not log off!");

            }

            scheduleRemoval(player);

        }

    }

    public boolean removeTagged(Player player) {

        boolean isTagged = tagged.contains(player);

        if (isTagged) {

            tagged.remove(player);

            if (runnables.containsKey(player)) {

                runnables.get(player).cancel();
                runnables.remove(player);

            }

        }

        return isTagged;

    }

    public void setActive(boolean active) {

        this.active = active;

    }

    public void setDelaySeconds(long seconds) {

        this.seconds = seconds;

    }

}
