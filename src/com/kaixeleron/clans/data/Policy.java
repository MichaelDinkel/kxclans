package com.kaixeleron.clans.data;

public enum Policy {

    CLAN,
    ALLY,
    TRUCE,
    NEUTRAL,
    ENEMY,
    PEACEFUL

}
