package com.kaixeleron.clans.data;

import com.google.common.collect.Table;
import com.kaixeleron.clans.ClansMain;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public abstract class ClansDatabase {

    private static String sys_unclaimed, sys_war, sys_safe, sys_unclaimed_desc, sys_war_desc, sys_safe_desc;

    private static ClansDatabase db;

    public static ClansDatabase getDatabase() {
        return db;
    }

    public static void setDatabase(ClansDatabase db) {
        ClansDatabase.db = db;
    }

    public static String getUnclaimedName() {
        return sys_unclaimed;
    }

    public static void setUnclaimedName(String name) {
        sys_unclaimed = name;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.unclaimed.name", name);
            ClansMain.getInstance().saveConfig();
        }
    }

    public static String getWarName() {
        return sys_war;
    }

    public static void setWarName(String name) {
        sys_war = name;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.war.name", name);
            ClansMain.getInstance().saveConfig();
        }
    }

    public static String getSafeName() {
        return sys_safe;
    }

    public static void setSafeName(String name) {
        sys_safe = name;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.safe.name", name);
            ClansMain.getInstance().saveConfig();
        }
    }

    public static String getUnclaimedDesc() {
        return sys_unclaimed_desc;
    }

    public static void setUnclaimedDesc(String desc) {
        sys_unclaimed_desc = desc;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.unclaimed.desc", desc);
            ClansMain.getInstance().saveConfig();
        }
    }

    public static String getWarDesc() {
        return sys_war_desc;
    }

    public static void setWarDesc(String desc) {
        sys_war_desc = desc;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.war.desc", desc);
            ClansMain.getInstance().saveConfig();
        }
    }

    public static String getSafeDesc() {
        return sys_safe_desc;
    }

    public static void setSafeDesc(String desc) {
        sys_safe_desc = desc;

        if (ClansMain.getInstance() != null) {
            ClansMain.getInstance().getConfig().set("sysclans.safe.desc", desc);
            ClansMain.getInstance().saveConfig();
        }
    }

    public abstract void loadCache() throws DatabaseException;

    public abstract void claim(String clan, Chunk c) throws DatabaseException;

    public abstract void unclaim(String clan, Chunk c) throws DatabaseException;

    public abstract String getChunkOwner(Chunk c) throws DatabaseException;

    public abstract String getChunkOwner(int x, int z) throws DatabaseException;

    public abstract Table<Integer, Integer, String> getChunkOwners(int lowX, int highX, int lowZ, int highZ) throws DatabaseException;

    public abstract boolean createClan(Player p, String name) throws DatabaseException;

    public abstract void deleteClan(String name) throws DatabaseException;

    public abstract String getClan(Player p) throws DatabaseException;

    public abstract String getClan(UUID u) throws DatabaseException;

    public abstract Map<String, Boolean> getClanPermissions(String clan, Rank rank) throws DatabaseException;

    public abstract Map<String, Boolean> getClanPermissions(String clan, Policy policy) throws DatabaseException;

    public abstract void setClanPermission(String clan, Rank rank, String permission, boolean value) throws DatabaseException;

    public abstract void setClanPermission(String clan, Policy policy, String permission, boolean value) throws DatabaseException;

    public abstract Rank getClanRank(UUID u, String clan) throws DatabaseException;

    public abstract Rank getClanRank(String name, String clan) throws DatabaseException;

    public abstract Policy getPolicy(String clan1, String clan2) throws DatabaseException;

    public abstract void setPolicy(String clan1, String clan2, Policy policy) throws DatabaseException;

    public abstract String clanExists(String clan) throws DatabaseException;

    public abstract List<UUID> getPlayers(String clan) throws DatabaseException;

    public abstract List<String> getPlayerNames(String clan) throws DatabaseException;

    public abstract String getDesc(String clan) throws DatabaseException;

    public abstract void setDesc(String clan, String description) throws DatabaseException;

    public abstract long getCreateDate(String clan) throws DatabaseException;

    public abstract int getClaimedLand(String clan) throws DatabaseException;

    public abstract List<String> getClanPolicies(String clan, Policy policy) throws DatabaseException;

    public abstract double getClanPower(String clan) throws DatabaseException;

    public abstract double getClanMaxPower(String clan) throws DatabaseException;

    public abstract void addClanMaxPower(String clan, double power) throws DatabaseException;

    public abstract double getPlayerPower(UUID id) throws DatabaseException;

    public abstract double getPlayerMaxPower(UUID id) throws DatabaseException;

    public abstract void setPlayerPower(UUID id, double power) throws DatabaseException;

    public abstract void addPlayerMaxPower(UUID id, double power) throws DatabaseException;

    public abstract String getPlayerTitle(UUID id) throws DatabaseException;

    public abstract String getPlayerTitleByName(String name) throws DatabaseException;

    public abstract void setPlayerTitle(UUID id, String title) throws DatabaseException;

    public abstract void updatePlayer(Player p) throws DatabaseException;

    public abstract void setHome(String clan, Location loc) throws DatabaseException;

    public abstract Location getHome(String clan) throws DatabaseException;

    public abstract boolean playerExists(UUID id) throws DatabaseException;

    public abstract boolean setWarp(String clan, String name, Location loc) throws DatabaseException;

    public abstract Location getWarp(String clan, String name) throws DatabaseException;

    public abstract void delWarp(String clan, String name) throws DatabaseException;

    public abstract List<String> getWarps(String clan) throws DatabaseException;

    public abstract void setRank(UUID u, Rank rank) throws DatabaseException;

    public abstract String getLeader(String clan) throws DatabaseException;

    public abstract Table<Rank, String, Boolean> getMemberPerms(String clan) throws DatabaseException;

    public abstract Table<Policy, String, Boolean> getForeignPerms(String clan) throws DatabaseException;

    public abstract void addInvite(UUID u, String clan) throws DatabaseException;

    public abstract void removeInvite(UUID u, String clan) throws DatabaseException;

    public abstract List<String> getInvites(UUID u) throws DatabaseException;

    public abstract List<UUID> getClanInvites(String clan) throws DatabaseException;

    public abstract void kickPlayer(String clan, UUID target) throws DatabaseException;

    public abstract void setChatMode(UUID u, ChatMode mode) throws DatabaseException;

    public abstract ChatMode getChatMode(UUID u) throws DatabaseException;

    public abstract int getClanCount() throws DatabaseException;

    public abstract String[] getClans(int page) throws DatabaseException;

    public abstract void setName(String clan, String newName) throws DatabaseException;

    public abstract boolean getFlag(String clan, Flag flag) throws DatabaseException;

    public abstract void setFlag(String clan, Flag flag, boolean value) throws DatabaseException;

    public abstract Map<Flag, Boolean> getFlags(String clan) throws DatabaseException;

    public abstract void joinClan(String clan, UUID u) throws DatabaseException;

    public abstract void setAccess(String clan, Chunk c, String target, boolean enabled) throws DatabaseException;

    public abstract boolean getAccess(String clan, Chunk c, String target) throws DatabaseException;

    public abstract List<String> getClanAccess(String clan, Chunk c) throws DatabaseException;

    public abstract List<String> getPlayerAccess(String clan, Chunk c) throws DatabaseException;

    public abstract void close() throws DatabaseException;

}
