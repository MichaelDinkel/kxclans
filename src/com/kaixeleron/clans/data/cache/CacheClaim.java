package com.kaixeleron.clans.data.cache;

public class CacheClaim {

    public CacheClaim(String clan, int x, int z) {

        this.clan = clan;
        this.x = x;
        this.z = z;

    }

    private String clan;
    private int x, z;

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

}
