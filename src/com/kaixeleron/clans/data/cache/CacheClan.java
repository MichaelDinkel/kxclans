package com.kaixeleron.clans.data.cache;

public class CacheClan {

    public CacheClan(String name, String description, long created, double powerboost, double homex, double homey, double homez, float homeyaw, float homepitch, String homeworld) {

        this.name = name;
        this.description = description;
        this.created = created;
        this.powerboost = powerboost;
        this.homex = homex;
        this.homey = homey;
        this.homez = homez;
        this.homeyaw = homeyaw;
        this.homepitch = homepitch;
        this.homeworld = homeworld;

    }

    private String name, description, homeworld;
    private long created;
    private double powerboost, homex, homey, homez;
    private float homeyaw, homepitch;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public void setHomeworld(String homeworld) {
        this.homeworld = homeworld;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public double getPowerboost() {
        return powerboost;
    }

    public void setPowerboost(double powerboost) {
        this.powerboost = powerboost;
    }

    public double getHomex() {
        return homex;
    }

    public void setHomex(double homex) {
        this.homex = homex;
    }

    public double getHomey() {
        return homey;
    }

    public void setHomey(double homey) {
        this.homey = homey;
    }

    public double getHomez() {
        return homez;
    }

    public void setHomez(double homez) {
        this.homez = homez;
    }

    public float getHomeyaw() {
        return homeyaw;
    }

    public void setHomeyaw(float homeyaw) {
        this.homeyaw = homeyaw;
    }

    public float getHomepitch() {
        return homepitch;
    }

    public void setHomepitch(float homepitch) {
        this.homepitch = homepitch;
    }
}
