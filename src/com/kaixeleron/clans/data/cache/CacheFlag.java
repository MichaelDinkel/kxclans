package com.kaixeleron.clans.data.cache;

import com.kaixeleron.clans.data.Flag;

public class CacheFlag {

    public CacheFlag(String clan, Flag flag, boolean value) {

        this.clan = clan;
        this.flag = flag;
        this.value = value;

    }

    private String clan;
    private Flag flag;
    private boolean value;

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public Flag getFlag() {
        return flag;
    }

    public void setFlag(Flag flag) {
        this.flag = flag;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
