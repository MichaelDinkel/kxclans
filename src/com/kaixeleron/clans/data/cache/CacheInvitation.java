package com.kaixeleron.clans.data.cache;

import java.util.UUID;

public class CacheInvitation {

    public CacheInvitation(UUID u, String clan) {

        this.u = u;
        this.clan = clan;

    }

    private UUID u;
    private String clan;

    public UUID getUUID() {
        return u;
    }

    public void setUUID(UUID u) {
        this.u = u;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }
}
