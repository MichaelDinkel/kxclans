package com.kaixeleron.clans.data.cache;

import com.kaixeleron.clans.data.Policy;

public class CachePolicy {

    public CachePolicy(String clan1, String clan2, Policy policy) {

        this.clan1 = clan1;
        this.clan2 = clan2;
        this.policy = policy;

    }

    private String clan1, clan2;
    private Policy policy;

    public String getClan1() {
        return clan1;
    }

    public void setClan1(String clan1) {
        this.clan1 = clan1;
    }

    public String getClan2() {
        return clan2;
    }

    public void setClan2(String clan2) {
        this.clan2 = clan2;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }
}
