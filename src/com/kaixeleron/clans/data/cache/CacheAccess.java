package com.kaixeleron.clans.data.cache;

public class CacheAccess {

    public CacheAccess(String clan, int x, int z, String target) {

        this.clan = clan;
        this.x = x;
        this.z = z;
        this.target = target;

    }

    private String clan, target;
    private int x, z;

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
