package com.kaixeleron.clans.data.cache;

import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.Rank;

public class CachePerm {

    public CachePerm(String clan, Rank rank, String perm, boolean value) {
        this.clan = clan;
        this.rank = rank;
        this.policy = null;
        this.perm = perm;
        this.value = value;
    }

    public CachePerm(String clan, Policy policy, String perm, boolean value) {
        this.clan = clan;
        this.rank = null;
        this.policy = policy;
        this.perm = perm;
        this.value = value;
    }

    private String clan, perm;
    private Rank rank;
    private Policy policy;
    private boolean value;

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getPerm() {
        return perm;
    }

    public void setPerm(String perm) {
        this.perm = perm;
    }

    public Rank getRank() {
        return rank;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
