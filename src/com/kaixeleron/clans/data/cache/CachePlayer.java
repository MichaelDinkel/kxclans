package com.kaixeleron.clans.data.cache;

import com.kaixeleron.clans.data.ChatMode;
import com.kaixeleron.clans.data.Rank;

import java.util.UUID;

public class CachePlayer {

    public CachePlayer(UUID u, String name, String clan, Rank rank, String title, double power, double maxpower, ChatMode chatmode) {

        this.u = u;
        this.name = name;
        this.clan = clan;
        this.rank = rank;
        this.title = title;
        this.power = power;
        this.maxpower = maxpower;
        this.chatmode = chatmode;

    }

    private UUID u;
    private String name, clan, title;
    private Rank rank;
    private double power, maxpower;
    private ChatMode chatmode;

    public UUID getUUID() {
        return u;
    }

    public void setUUID(UUID u) {
        this.u = u;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public double getMaxpower() {
        return maxpower;
    }

    public void setMaxpower(double maxpower) {
        this.maxpower = maxpower;
    }

    public ChatMode getChatmode() {
        return chatmode;
    }

    public void setChatmode(ChatMode chatmode) {
        this.chatmode = chatmode;
    }
}
