package com.kaixeleron.clans.data;

import org.bukkit.entity.Player;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChatModeManager {

    public ChatModeManager() {
        instance = this;
    }

    private volatile Map<Player, ChatMode> modes = new ConcurrentHashMap<>();

    public void setMode(Player p, ChatMode c) {
        modes.put(p, c);
    }

    public ChatMode getMode(Player p) {
        return modes.get(p);
    }

    public void clearMode(Player p) {
        modes.remove(p);
    }

    private static ChatModeManager instance;

    public static ChatModeManager getInstance() {
        return instance;
    }
}
