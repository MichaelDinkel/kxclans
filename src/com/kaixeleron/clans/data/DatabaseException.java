package com.kaixeleron.clans.data;

public class DatabaseException extends Throwable {

    /**
     *
     */
    private static final long serialVersionUID = -5597473911650827125L;

    DatabaseException(Exception e) {
        super(e);
    }

}
