package com.kaixeleron.clans.data;

public enum Rank {

    LEADER,
    CAPTAIN,
    MEMBER,
    NEWBIE

}
