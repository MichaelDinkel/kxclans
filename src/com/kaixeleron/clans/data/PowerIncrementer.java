package com.kaixeleron.clans.data;

import com.kaixeleron.clans.ClansMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class PowerIncrementer extends BukkitRunnable {

    @Override
    public void run() {

        try {

            for (Player p : Bukkit.getOnlinePlayers()) {

                UUID id = p.getUniqueId();

                double power = ClansDatabase.getDatabase().getPlayerPower(id), maxpower = ClansDatabase.getDatabase().getPlayerMaxPower(id);

                if (power == maxpower) continue;

                double amount = ClansMain.getInstance().getConfig().getDouble("powerperhour") / 60.0D;

                if (power + amount >= maxpower) {

                    ClansDatabase.getDatabase().setPlayerPower(id, maxpower);
                    continue;

                }

                ClansDatabase.getDatabase().setPlayerPower(id, power + amount);
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
}
