package com.kaixeleron.clans.data;

public enum Flag {

    ENDERGRIEF,
    EXPLOSIONS,
    FIRESPREAD,
    FRIENDLYFIRE,
    INFPOWER,
    ANIMALS,
    MONSTERS,
    OPEN,
    PEACEFUL,
    PERMANENT,
    POWERLOSS,
    PVP

}
