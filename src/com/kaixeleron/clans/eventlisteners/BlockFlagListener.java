package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.hanging.HangingBreakEvent;

import java.util.List;

public class BlockFlagListener implements Listener {

    @EventHandler
    public void onEnderGrief(EntityChangeBlockEvent event) {

        if (!(event.getEntity() instanceof Enderman)) return;

        try {

            Chunk c = event.getBlock().getChunk();
            String clan = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!ClansDatabase.getDatabase().getFlag(clan, Flag.ENDERGRIEF)) {
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent event) {

        List<Block> blocks = event.blockList();

        try {

            for (Block b : blocks) {

                Chunk c = b.getChunk();
                String clan = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!ClansDatabase.getDatabase().getFlag(clan, Flag.EXPLOSIONS)) {
                    event.setCancelled(true);
                    break;
                }

            }

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onHangingBreakByExplosion(HangingBreakEvent event) {

        if (event.getCause().equals(HangingBreakEvent.RemoveCause.EXPLOSION)) {

            try {

                String clan = ClansDatabase.getDatabase().getChunkOwner(event.getEntity().getLocation().getChunk());

                if (!ClansDatabase.getDatabase().getFlag(clan, Flag.EXPLOSIONS)) {
                    event.setCancelled(true);
                }

            } catch (DatabaseException e) {
                e.printStackTrace();
            }

        }

    }

    @EventHandler
    public void onFireSpread(BlockIgniteEvent event) {

        if (!event.getCause().equals(BlockIgniteEvent.IgniteCause.SPREAD)) return;

        try {

            Chunk c = event.getBlock().getChunk();
            String clan = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!ClansDatabase.getDatabase().getFlag(clan, Flag.FIRESPREAD)) {
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onMonsterSpawn(EntitySpawnEvent event) {

        if (event.getEntity() instanceof Monster || event.getEntity() instanceof Ghast || event.getEntity() instanceof Slime) {

            try {

                Chunk c = event.getEntity().getLocation().getChunk();
                String clan = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!ClansDatabase.getDatabase().getFlag(clan, Flag.MONSTERS)) {
                    event.setCancelled(true);
                }

            } catch (DatabaseException e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onAnimalSpawn(EntitySpawnEvent event) {

        if (event.getEntity() instanceof Animals || event.getEntity() instanceof WaterMob) {

            try {

                Chunk c = event.getEntity().getLocation().getChunk();
                String clan = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!ClansDatabase.getDatabase().getFlag(clan, Flag.ANIMALS)) {
                    event.setCancelled(true);
                }

            } catch (DatabaseException e) {
                e.printStackTrace();
            }
        }
    }
}
