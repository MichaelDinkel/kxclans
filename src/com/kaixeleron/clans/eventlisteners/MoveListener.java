package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.TeleportTimer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;

public class MoveListener implements Listener {

    public MoveListener() {
        MoveListener.instance = this;
    }

    private Map<Player, Location> lastloc = new HashMap<>();
    private Map<Player, String> inclan = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMove(PlayerMoveEvent event) {

        Location l = event.getPlayer().getLocation();

        if (!lastloc.containsKey(event.getPlayer())) {
            lastloc.put(event.getPlayer(), l);
            update(event.getPlayer(), l);
        } else {
            Location last = lastloc.get(event.getPlayer());

            if (!(last.getX() == l.getX() && last.getZ() == l.getZ())) {
                lastloc.put(event.getPlayer(), l);
                update(event.getPlayer(), l);
            }
        }
    }

    void update(Player p, Location l) {

        try {

            String clan = ClansDatabase.getDatabase().getChunkOwner(l.getChunk());

            if (!inclan.containsKey(p) || !inclan.get(p).equals(clan)) {

                inclan.put(p, clan);

                String description, color;

                if (clan.equals(ClansDatabase.getSafeName())) {
                    description = ClansDatabase.getSafeDesc();
                    color = ChatColor.GOLD + "";
                } else if (clan.equals(ClansDatabase.getWarName())) {
                    description = ClansDatabase.getWarDesc();
                    color = ChatColor.DARK_RED + "";
                } else if (clan.equals(ClansDatabase.getUnclaimedName())) {
                    description = ClansDatabase.getUnclaimedDesc();
                    color = ChatColor.DARK_GREEN + "";
                } else {
                    description = ClansDatabase.getDatabase().getDesc(clan);
                    color = ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors."
                            + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(p), clan).toString());
                }

                if (ClansMain.getInstance().getConfig().getBoolean("showTerritoryWithTitles")) {

                    p.sendTitle(color + clan, description, 10, 40, 10);

                } else {

                    p.sendMessage(ChatColor.YELLOW + "Now entering: " + color + clan + (description.length() > 0 ? ChatColor.YELLOW + " - " + ChatColor.WHITE + description : ""));

                }
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHit(EntityDamageByEntityEvent event) {

        if (event.getEntity() instanceof Player) {

            TeleportTimer.getInstance().removeTimer((Player) event.getEntity());

        }
    }

    static MoveListener instance;
}
