package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ContainerListener implements Listener {

    private final Material[] containers = {Material.CHEST, Material.TRAPPED_CHEST, Material.HOPPER, Material.FURNACE, Material.DISPENSER,
            Material.DROPPER, Material.WHITE_SHULKER_BOX, Material.ORANGE_SHULKER_BOX, Material.MAGENTA_SHULKER_BOX, Material.LIGHT_BLUE_SHULKER_BOX,
            Material.YELLOW_SHULKER_BOX, Material.LIME_SHULKER_BOX, Material.PINK_SHULKER_BOX, Material.GRAY_SHULKER_BOX, Material.LIGHT_GRAY_SHULKER_BOX,
            Material.CYAN_SHULKER_BOX, Material.PURPLE_SHULKER_BOX, Material.BLUE_SHULKER_BOX, Material.BROWN_SHULKER_BOX, Material.GREEN_SHULKER_BOX,
            Material.RED_SHULKER_BOX, Material.BLACK_SHULKER_BOX};

    @EventHandler(priority = EventPriority.LOWEST)
    public void onContainerInteract(PlayerInteractEvent event) {

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;

        for (Material m : containers) {
            if (event.getClickedBlock().getType().equals(m)) {

                Chunk c = event.getClickedBlock().getLocation().getChunk();

                try {

                    String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                    if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                        event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                                owner).toString()) + owner + ChatColor.RED + " does not allow you to use containers.");
                        if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                            event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                        event.setCancelled(true);
                    }

                } catch (DatabaseException e) {
                    if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                    e.printStackTrace();
                }
            }
        }
    }
}
