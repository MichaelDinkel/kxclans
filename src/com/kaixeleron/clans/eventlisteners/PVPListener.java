package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.data.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PVPListener implements Listener {

    private final CombatTagManager combatTagManager;

    public PVPListener(CombatTagManager combatTagManager) {

        this.combatTagManager = combatTagManager;

    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {

        if (!(event.getEntity() instanceof Player && event.getDamager() instanceof Player)) return;

        Player victim = (Player) event.getEntity();
        Player attacker = (Player) event.getDamager();

        try {

            String chunkOwner = ClansDatabase.getDatabase().getChunkOwner(victim.getLocation().getChunk());

            if (!ClansDatabase.getDatabase().getFlag(chunkOwner, Flag.PVP)) {

                event.setCancelled(true);

            } else {

                String victimClan = ClansDatabase.getDatabase().getClan(victim), attackerClan = ClansDatabase.getDatabase().getClan(attacker);

                if (!victimClan.equals(ClansDatabase.getUnclaimedName()) && !attackerClan.equals(ClansDatabase.getUnclaimedName())) {

                    Policy rel = ClansDatabase.getDatabase().getPolicy(victimClan, attackerClan);

                    switch (rel) {
                        case CLAN:
                        case ALLY:
                        case PEACEFUL:

                            if (!ClansDatabase.getDatabase().getFlag(chunkOwner, Flag.FRIENDLYFIRE)) {

                                event.setCancelled(true);

                            } else {

                                combatTagManager.tagPlayer(victim);
                                combatTagManager.tagPlayer(attacker);

                            }

                            break;

                        default:

                            break;

                    }

                }

            }

        } catch (DatabaseException e) {

            System.err.println("A database exception occurred while processing a PVP action.");
            e.printStackTrace();
            event.setCancelled(true);

        }
    }
}
