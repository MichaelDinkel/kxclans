package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class DoorListener implements Listener {

    private final Material[] doors = {Material.OAK_DOOR,  Material.IRON_DOOR, Material.SPRUCE_DOOR, Material.BIRCH_DOOR,
            Material.JUNGLE_DOOR, Material.DARK_OAK_DOOR, Material.ACACIA_DOOR, Material.OAK_TRAPDOOR, Material.IRON_TRAPDOOR,
            Material.SPRUCE_TRAPDOOR, Material.BIRCH_TRAPDOOR, Material.JUNGLE_TRAPDOOR, Material.ACACIA_TRAPDOOR, Material.DARK_OAK_TRAPDOOR,
            Material.OAK_FENCE_GATE, Material.SPRUCE_FENCE_GATE, Material.BIRCH_FENCE_GATE, Material.JUNGLE_FENCE_GATE,
            Material.DARK_OAK_FENCE_GATE, Material.ACACIA_FENCE_GATE};

    private List<Player> debounce = new ArrayList<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDoorInteract(PlayerInteractEvent event) {

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;

        if (debounce.contains(event.getPlayer())) {
            event.setCancelled(true);
            return;
        }

        for (Material m : doors) {
            if (event.getClickedBlock().getType().equals(m)) {

                Chunk c = event.getClickedBlock().getLocation().getChunk();

                try {

                    String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                    if (!PermissionManager.getInstance().canUse(event.getPlayer(), "door", owner)) {
                        event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                                owner).toString()) + owner + ChatColor.RED + " does not allow you to use doors.");
                        if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                            event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                        event.setCancelled(true);

                        debounce.add(event.getPlayer());

                        new BukkitRunnable() {

                            @Override
                            public void run() {
                                debounce.remove(event.getPlayer());
                            }

                        }.runTaskLater(ClansMain.getInstance(), 5);
                    }

                } catch (DatabaseException e) {
                    if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                    e.printStackTrace();
                }
            }
        }
    }
}
