package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class InteractListener implements Listener {

    private final Material[] list = {Material.LEVER, Material.JUKEBOX, Material.NOTE_BLOCK, Material.ANVIL, Material.END_PORTAL_FRAME,
            Material.ITEM_FRAME, Material.REPEATER, Material.COMPARATOR, Material.MINECART, Material.CHEST_MINECART, Material.FURNACE_MINECART, Material.COMMAND_BLOCK_MINECART,
            Material.HOPPER_MINECART, Material.TNT_MINECART, Material.COMMAND_BLOCK, Material.CHAIN_COMMAND_BLOCK, Material.REPEATING_COMMAND_BLOCK, Material.BEACON,
            Material.CAULDRON, Material.TNT, Material.STONE_BUTTON, Material.OAK_BUTTON, Material.SPRUCE_BUTTON, Material.BIRCH_BUTTON, Material.JUNGLE_BUTTON,
            Material.ACACIA_BUTTON, Material.DARK_OAK_BUTTON, Material.DAYLIGHT_DETECTOR, Material.WHITE_BED, Material.ORANGE_BED, Material.MAGENTA_BED, Material.LIGHT_BLUE_BED,
            Material.YELLOW_BED, Material.LIME_BED, Material.PINK_BED, Material.GRAY_BED, Material.LIGHT_GRAY_BED, Material.CYAN_BED, Material.PURPLE_BED, Material.BLUE_BED,
            Material.BROWN_BED, Material.GREEN_BED, Material.RED_BED, Material.BLACK_BED};
    private List<Player> debounce = new ArrayList<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onArmorStandManipulate(PlayerArmorStandManipulateEvent event) {

        Chunk c = event.getRightClicked().getLocation().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to interact with that.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteract(PlayerInteractEvent event) {

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {

            for (Material m : list) {
                if (event.getClickedBlock().getType().equals(m)) {

                    if (debounce.contains(event.getPlayer())) {
                        event.setCancelled(true);
                        return;
                    }

                    Chunk c = event.getClickedBlock().getLocation().getChunk();

                    try {

                        String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                        if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                            event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                                    owner).toString()) + owner + ChatColor.RED + " does not allow you to interact with that.");
                            if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                                event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                            event.setCancelled(true);
                            debounce.add(event.getPlayer());

                            new BukkitRunnable() {

                                @Override
                                public void run() {
                                    debounce.remove(event.getPlayer());
                                }

                            }.runTaskLater(ClansMain.getInstance(), 5);
                        }

                    } catch (DatabaseException e) {
                        if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                        e.printStackTrace();
                    }
                }
            }
        } else if (event.getAction().equals(Action.PHYSICAL)) {

            if (event.getClickedBlock().getType().equals(Material.OAK_PRESSURE_PLATE) || event.getClickedBlock().getType().equals(Material.STONE_PRESSURE_PLATE)
                    || event.getClickedBlock().getType().equals(Material.LIGHT_WEIGHTED_PRESSURE_PLATE) || event.getClickedBlock().getType().equals(Material.HEAVY_WEIGHTED_PRESSURE_PLATE)
                    || event.getClickedBlock().getType().equals(Material.TRIPWIRE) || event.getClickedBlock().getType().equals(Material.SPRUCE_PRESSURE_PLATE)
                    || event.getClickedBlock().getType().equals(Material.BIRCH_PRESSURE_PLATE) || event.getClickedBlock().getType().equals(Material.JUNGLE_PRESSURE_PLATE)
                    || event.getClickedBlock().getType().equals(Material.ACACIA_PRESSURE_PLATE) || event.getClickedBlock().getType().equals(Material.DARK_OAK_PRESSURE_PLATE)) {

                if (debounce.contains(event.getPlayer())) {
                    event.setCancelled(true);
                    return;
                }

                Chunk c = event.getClickedBlock().getLocation().getChunk();

                try {

                    String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                    if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                        event.setCancelled(true);
                        debounce.add(event.getPlayer());

                        new BukkitRunnable() {

                            @Override
                            public void run() {
                                debounce.remove(event.getPlayer());
                            }

                        }.runTaskLater(ClansMain.getInstance(), 5);
                    }

                } catch (DatabaseException e) {
                    if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                    e.printStackTrace();
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onItemUse(PlayerInteractEvent event) {

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;

        boolean blocked = false;

        switch (event.getPlayer().getInventory().getItemInMainHand().getType()) {

            case BAT_SPAWN_EGG:
            case BEE_SPAWN_EGG:
            case BLAZE_SPAWN_EGG:
            case CAT_SPAWN_EGG:
            case CAVE_SPIDER_SPAWN_EGG:
            case CHICKEN_SPAWN_EGG:
            case COD_SPAWN_EGG:
            case COW_SPAWN_EGG:
            case CREEPER_SPAWN_EGG:
            case DOLPHIN_SPAWN_EGG:
            case DONKEY_SPAWN_EGG:
            case DROWNED_SPAWN_EGG:
            case ELDER_GUARDIAN_SPAWN_EGG:
            case ENDERMAN_SPAWN_EGG:
            case ENDERMITE_SPAWN_EGG:
            case EVOKER_SPAWN_EGG:
            case FOX_SPAWN_EGG:
            case GHAST_SPAWN_EGG:
            case GUARDIAN_SPAWN_EGG:
            case HOGLIN_SPAWN_EGG:
            case HORSE_SPAWN_EGG:
            case HUSK_SPAWN_EGG:
            case LLAMA_SPAWN_EGG:
            case MAGMA_CUBE_SPAWN_EGG:
            case MOOSHROOM_SPAWN_EGG:
            case MULE_SPAWN_EGG:
            case OCELOT_SPAWN_EGG:
            case PANDA_SPAWN_EGG:
            case PARROT_SPAWN_EGG:
            case PHANTOM_SPAWN_EGG:
            case PIG_SPAWN_EGG:
            case PIGLIN_SPAWN_EGG:
            case PILLAGER_SPAWN_EGG:
            case POLAR_BEAR_SPAWN_EGG:
            case PUFFERFISH_SPAWN_EGG:
            case RABBIT_SPAWN_EGG:
            case RAVAGER_SPAWN_EGG:
            case SALMON_SPAWN_EGG:
            case SHEEP_SPAWN_EGG:
            case SHULKER_SPAWN_EGG:
            case SILVERFISH_SPAWN_EGG:
            case SKELETON_SPAWN_EGG:
            case SKELETON_HORSE_SPAWN_EGG:
            case SLIME_SPAWN_EGG:
            case SPIDER_SPAWN_EGG:
            case SQUID_SPAWN_EGG:
            case STRAY_SPAWN_EGG:
            case STRIDER_SPAWN_EGG:
            case TRADER_LLAMA_SPAWN_EGG:
            case TROPICAL_FISH_SPAWN_EGG:
            case TURTLE_SPAWN_EGG:
            case VEX_SPAWN_EGG:
            case VILLAGER_SPAWN_EGG:
            case VINDICATOR_SPAWN_EGG:
            case WANDERING_TRADER_SPAWN_EGG:
            case WITCH_SPAWN_EGG:
            case WITHER_SKELETON_SPAWN_EGG:
            case WOLF_SPAWN_EGG:
            case ZOMBIE_SPAWN_EGG:
            case ZOMBIE_HORSE_SPAWN_EGG:
            case ZOMBIFIED_PIGLIN_SPAWN_EGG:
            case ZOGLIN_SPAWN_EGG:
            case ZOMBIE_VILLAGER_SPAWN_EGG:
            case RED_DYE:
            case GREEN_DYE:
            case PURPLE_DYE:
            case CYAN_DYE:
            case LIGHT_GRAY_DYE:
            case GRAY_DYE:
            case PINK_DYE:
            case LIME_DYE:
            case YELLOW_DYE:
            case LIGHT_BLUE_DYE:
            case MAGENTA_DYE:
            case ORANGE_DYE:
            case BLUE_DYE:
            case BROWN_DYE:
            case BLACK_DYE:
            case WHITE_DYE:

                blocked = true;

                break;

        }

        if (!blocked) switch (event.getPlayer().getInventory().getItemInOffHand().getType()) {

            case BAT_SPAWN_EGG:
            case BEE_SPAWN_EGG:
            case BLAZE_SPAWN_EGG:
            case CAT_SPAWN_EGG:
            case CAVE_SPIDER_SPAWN_EGG:
            case CHICKEN_SPAWN_EGG:
            case COD_SPAWN_EGG:
            case COW_SPAWN_EGG:
            case CREEPER_SPAWN_EGG:
            case DOLPHIN_SPAWN_EGG:
            case DONKEY_SPAWN_EGG:
            case DROWNED_SPAWN_EGG:
            case ELDER_GUARDIAN_SPAWN_EGG:
            case ENDERMAN_SPAWN_EGG:
            case ENDERMITE_SPAWN_EGG:
            case EVOKER_SPAWN_EGG:
            case FOX_SPAWN_EGG:
            case GHAST_SPAWN_EGG:
            case GUARDIAN_SPAWN_EGG:
            case HOGLIN_SPAWN_EGG:
            case HORSE_SPAWN_EGG:
            case HUSK_SPAWN_EGG:
            case LLAMA_SPAWN_EGG:
            case MAGMA_CUBE_SPAWN_EGG:
            case MOOSHROOM_SPAWN_EGG:
            case MULE_SPAWN_EGG:
            case OCELOT_SPAWN_EGG:
            case PANDA_SPAWN_EGG:
            case PARROT_SPAWN_EGG:
            case PHANTOM_SPAWN_EGG:
            case PIG_SPAWN_EGG:
            case PIGLIN_SPAWN_EGG:
            case PILLAGER_SPAWN_EGG:
            case POLAR_BEAR_SPAWN_EGG:
            case PUFFERFISH_SPAWN_EGG:
            case RABBIT_SPAWN_EGG:
            case RAVAGER_SPAWN_EGG:
            case SALMON_SPAWN_EGG:
            case SHEEP_SPAWN_EGG:
            case SHULKER_SPAWN_EGG:
            case SILVERFISH_SPAWN_EGG:
            case SKELETON_SPAWN_EGG:
            case SKELETON_HORSE_SPAWN_EGG:
            case SLIME_SPAWN_EGG:
            case SPIDER_SPAWN_EGG:
            case SQUID_SPAWN_EGG:
            case STRAY_SPAWN_EGG:
            case STRIDER_SPAWN_EGG:
            case TRADER_LLAMA_SPAWN_EGG:
            case TROPICAL_FISH_SPAWN_EGG:
            case TURTLE_SPAWN_EGG:
            case VEX_SPAWN_EGG:
            case VILLAGER_SPAWN_EGG:
            case VINDICATOR_SPAWN_EGG:
            case WANDERING_TRADER_SPAWN_EGG:
            case WITCH_SPAWN_EGG:
            case WITHER_SKELETON_SPAWN_EGG:
            case WOLF_SPAWN_EGG:
            case ZOMBIE_SPAWN_EGG:
            case ZOMBIE_HORSE_SPAWN_EGG:
            case ZOMBIFIED_PIGLIN_SPAWN_EGG:
            case ZOGLIN_SPAWN_EGG:
            case ZOMBIE_VILLAGER_SPAWN_EGG:
            case RED_DYE:
            case GREEN_DYE:
            case PURPLE_DYE:
            case CYAN_DYE:
            case LIGHT_GRAY_DYE:
            case GRAY_DYE:
            case PINK_DYE:
            case LIME_DYE:
            case YELLOW_DYE:
            case LIGHT_BLUE_DYE:
            case MAGENTA_DYE:
            case ORANGE_DYE:
            case BLUE_DYE:
            case BROWN_DYE:
            case BLACK_DYE:
            case WHITE_DYE:

                blocked = true;

                break;

        }

        if (blocked) {

            Chunk c = event.getClickedBlock().getChunk();

            try {

                String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                    event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                            owner).toString()) + owner + ChatColor.RED + " does not allow you to interact with that.");
                    if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                        event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                    event.setCancelled(true);

                    debounce.add(event.getPlayer());

                    new BukkitRunnable() {

                        @Override
                        public void run() {
                            debounce.remove(event.getPlayer());
                        }

                    }.runTaskLater(ClansMain.getInstance(), 5);
                }

            } catch (DatabaseException e) {
                if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                e.printStackTrace();
            }

        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityInteract(PlayerInteractEntityEvent event) {

        if (debounce.contains(event.getPlayer())) {
            event.setCancelled(true);
            return;
        }

        Chunk c = event.getRightClicked().getLocation().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "interact", owner)) {
                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to interact with that.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);

                debounce.add(event.getPlayer());

                new BukkitRunnable() {

                    @Override
                    public void run() {
                        debounce.remove(event.getPlayer());
                    }

                }.runTaskLater(ClansMain.getInstance(), 5);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }
}
