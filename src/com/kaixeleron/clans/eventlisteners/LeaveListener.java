package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.data.ChatModeManager;
import com.kaixeleron.clans.data.CombatTagManager;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener {

    private final CombatTagManager combatTagManager;

    public LeaveListener(CombatTagManager combatTagManager) {

        this.combatTagManager = combatTagManager;

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        ChatModeManager.getInstance().clearMode(event.getPlayer());

        if (combatTagManager.removeTagged(event.getPlayer()) && !PermissionManager.getInstance().isBypassing(event.getPlayer())) {

            event.getPlayer().setHealth(0.0D);

        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        ChatModeManager.getInstance().clearMode(event.getPlayer());

    }

}
