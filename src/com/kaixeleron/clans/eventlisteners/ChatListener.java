package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ChatModeManager;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.Policy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.UUID;

public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event) {

        if (ClansMain.getInstance().getConfig().getBoolean("chatformat.enabled")) event.setFormat(ClansMain.getInstance().getConfig().getString("chatformat.format"));

        try {

            String clan = ClansDatabase.getDatabase().getClan(event.getPlayer());

            switch (ChatModeManager.getInstance().getMode(event.getPlayer())) {
                case PUBLIC:

                    String space = "";

                    if (clan.equals(ClansDatabase.getUnclaimedName())) clan = "";

                    if (clan.length() > 0) space = " ";

                    event.setFormat(event.getFormat().replace("$clanspace", space).replace("$clancolor", ChatColor.COLOR_CHAR
                            + ClansMain.getInstance().getConfig().getString("colors.NEUTRAL")).replace("$clan", clan + ChatColor.RESET));

                    for (Player p : event.getRecipients()) {

                        p.sendMessage(event.getFormat().replace("%1$s", event.getPlayer().getName()).replace("%2$s",
                                event.getMessage()).replace(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.NEUTRAL"),
                                ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase()
                                        .getPolicy(clan, ClansDatabase.getDatabase().getClan(p)).toString())));

                    }

                    event.getRecipients().clear();

                    break;
                case CLAN:

                    event.getRecipients().clear();

                    event.setFormat(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.CLAN") + "[" + clan + "] " + "%1$s: " + ChatColor.WHITE + "%2$s");

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            event.getRecipients().add(p);

                        }
                    }

                    break;
                case ALLY:

                    event.getRecipients().clear();

                    event.setFormat(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ALLY") + "[" + clan + "] " + "%1$s: " + ChatColor.WHITE + "%2$s");

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            event.getRecipients().add(p);

                        }
                    }

                    for (String s : ClansDatabase.getDatabase().getClanPolicies(clan, Policy.ALLY)) {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(s)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                event.getRecipients().add(p);

                            }
                        }
                    }

                    break;
                case TRUCE:

                    event.getRecipients().clear();

                    event.setFormat(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.TRUCE") + "[" + clan + "] " + "%1$s: " + ChatColor.WHITE + "%2$s");

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            event.getRecipients().add(p);

                        }
                    }

                    for (String s : ClansDatabase.getDatabase().getClanPolicies(clan, Policy.ALLY)) {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(s)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                event.getRecipients().add(p);

                            }
                        }
                    }

                    for (String s : ClansDatabase.getDatabase().getClanPolicies(clan, Policy.TRUCE)) {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(s)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                event.getRecipients().add(p);

                            }
                        }
                    }

                    break;
                case ENEMY:

                    event.getRecipients().clear();

                    event.setFormat(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors.ENEMY") + "[" + clan + "] " + "%1$s: " + ChatColor.WHITE + "%2$s");

                    for (UUID u : ClansDatabase.getDatabase().getPlayers(clan)) {

                        Player p = Bukkit.getPlayer(u);

                        if (p != null) {

                            event.getRecipients().add(p);

                        }
                    }

                    for (String s : ClansDatabase.getDatabase().getClanPolicies(clan, Policy.ENEMY)) {

                        for (UUID u : ClansDatabase.getDatabase().getPlayers(s)) {

                            Player p = Bukkit.getPlayer(u);

                            if (p != null) {

                                event.getRecipients().add(p);

                            }
                        }
                    }

                    break;
            }
        } catch (DatabaseException e) {
            event.getPlayer().sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
            e.printStackTrace();
        }
    }
}
