package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent event) {

        Chunk c = event.getBlock().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to place blocks.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent event) {

        Chunk c = event.getBlock().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to break blocks.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onArmorStandPlace(PlayerInteractEvent event) {
        if ((event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.ARMOR_STAND) ||
                event.getPlayer().getInventory().getItemInOffHand().getType().equals(Material.ARMOR_STAND)) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

            Chunk c = event.getClickedBlock().getChunk();

            try {

                String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                        && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                    event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                            owner).toString()) + owner + ChatColor.RED + " does not allow you to place blocks.");
                    if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                        event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                    event.setCancelled(true);
                }

            } catch (DatabaseException e) {
                if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                e.printStackTrace();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityBreak(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof ArmorStand) && !(event.getEntity() instanceof ItemFrame)) return;

        Player p = (Player) event.getDamager();

        Chunk c = event.getEntity().getLocation().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(p, "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, p.getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(p))) {

                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(p),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to break blocks.");
                if (p.hasPermission("kxclans.admin.bypass"))
                    p.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(p)) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHangingPlace(HangingPlaceEvent event) {

        Chunk c = event.getEntity().getLocation().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to place blocks.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHangingBreak(HangingBreakByEntityEvent event) {

        if (!(event.getRemover() instanceof Player)) return;

        Player p = (Player) event.getRemover();

        Chunk c = event.getEntity().getLocation().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(p, "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, p.getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(p))) {

                p.sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(p),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to break blocks.");
                if (p.hasPermission("kxclans.admin.bypass"))
                    p.sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(p)) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {

        Chunk c = event.getBlockClicked().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to place blocks.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBucketFill(PlayerBucketFillEvent event) {

        Chunk c = event.getBlockClicked().getChunk();

        try {

            String owner = ClansDatabase.getDatabase().getChunkOwner(c);

            if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                    && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                        owner).toString()) + owner + ChatColor.RED + " does not allow you to break blocks.");
                if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                    event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                event.setCancelled(true);
            }

        } catch (DatabaseException e) {
            if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onFarmlandTrample(PlayerInteractEvent event) {

        if (event.getAction().equals(Action.PHYSICAL) && event.getClickedBlock().getType().equals(Material.FARMLAND)) {

            Chunk c = event.getClickedBlock().getChunk();

            try {

                String owner = ClansDatabase.getDatabase().getChunkOwner(c);

                if (!PermissionManager.getInstance().canUse(event.getPlayer(), "build", owner) && !ClansDatabase.getDatabase().getAccess(owner, c, event.getPlayer().getUniqueId().toString())
                        && !ClansDatabase.getDatabase().getAccess(owner, c, ClansDatabase.getDatabase().getClan(event.getPlayer()))) {

                    event.getPlayer().sendMessage(ChatColor.COLOR_CHAR + ClansMain.getInstance().getConfig().getString("colors." + ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan(event.getPlayer()),
                            owner).toString()) + owner + ChatColor.RED + " does not allow you to break blocks.");
                    if (event.getPlayer().hasPermission("kxclans.admin.bypass"))
                        event.getPlayer().sendMessage(ChatColor.YELLOW + "You may bypass this with " + ChatColor.GREEN + "/cadmin bypass");
                    event.setCancelled(true);
                }

            } catch (DatabaseException e) {
                if (!PermissionManager.getInstance().isBypassing(event.getPlayer())) event.setCancelled(true);
                e.printStackTrace();
            }
        }
    }
}
