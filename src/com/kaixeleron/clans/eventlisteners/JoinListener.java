package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.data.ChatModeManager;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        initPlayer(event.getPlayer());
    }

    public void initPlayer(Player p) {

        try {

            MoveListener.instance.update(p, p.getLocation());
            ClansDatabase.getDatabase().updatePlayer(p);
            ChatModeManager.getInstance().setMode(p, ClansDatabase.getDatabase().getChatMode(p.getUniqueId()));

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
}
