package com.kaixeleron.clans.eventlisteners;

import com.kaixeleron.clans.ClansMain;
import com.kaixeleron.clans.data.ClansDatabase;
import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Flag;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.math.BigDecimal;

public class DeathFlagListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {

        if (!ClansMain.getInstance().getConfig().getBoolean("deathpowerloss.enabled")) return;
        if (ClansMain.getInstance().getConfig().getDouble("deathpowerloss.amount") == 0.0D) return;

        try {

            if (ClansDatabase.getDatabase().getFlag(ClansDatabase.getDatabase().getChunkOwner(event.getEntity().getLocation().getChunk()), Flag.POWERLOSS)) {

                double power = ClansDatabase.getDatabase().getPlayerPower(event.getEntity().getUniqueId());

                power = power - ClansMain.getInstance().getConfig().getDouble("deathpowerloss.amount");

                if (power < 0.0D) power = 0.0D;

                BigDecimal powerdecimal = new BigDecimal(power);
                powerdecimal = powerdecimal.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();

                ClansDatabase.getDatabase().setPlayerPower(event.getEntity().getUniqueId(), power);
                event.getEntity().sendMessage(ChatColor.RED + "Your power is now: " + ChatColor.WHITE + powerdecimal.toPlainString());

            } else {

                event.getEntity().sendMessage(ChatColor.RED + "Power loss is disabled on this territory.");

            }

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
}
