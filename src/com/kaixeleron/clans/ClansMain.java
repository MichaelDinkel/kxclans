package com.kaixeleron.clans;

import com.kaixeleron.clans.command.CommandClans;
import com.kaixeleron.clans.command.CommandClansAdmin;
import com.kaixeleron.clans.eventlisteners.*;
import com.kaixeleron.clans.permission.PermissionManager;
import com.kaixeleron.clans.data.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ClansMain extends JavaPlugin {

    private static ClansMain instance;

    public synchronized static ClansMain getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {

        ClansMain.instance = this;

        saveDefaultConfig();

        ClansDatabase.setUnclaimedName(getConfig().getString("sysclans.unclaimed.name"));
        ClansDatabase.setWarName(getConfig().getString("sysclans.war.name"));
        ClansDatabase.setSafeName(getConfig().getString("sysclans.safe.name"));

        ClansDatabase.setUnclaimedDesc(getConfig().getString("sysclans.unclaimed.desc"));
        ClansDatabase.setWarDesc(getConfig().getString("sysclans.war.desc"));
        ClansDatabase.setSafeDesc(getConfig().getString("sysclans.safe.desc"));

        switch (getConfig().getString("database")) {
            case "sql":

                try {

                    ClansDatabase.setDatabase(new SQLDatabase(getConfig().getString("sql.address"), getConfig().getString("sql.port"), getConfig().getString("sql.database"),
                            getConfig().getString("sql.user"), getConfig().getString("sql.password"), getConfig().getString("sql.prefix")));

                } catch (DatabaseException e) {

                    getLogger().warning("kxClans could not connect to or set up the SQL database.");
                    e.printStackTrace();
                    setEnabled(false);
                    ClansMain.getInstance().getLogger().info("Disabling");
                    return;

                } catch (ClassNotFoundException e) {

                    getLogger().warning("kxClans could not find the MySQL driver.");
                    setEnabled(false);
                    ClansMain.getInstance().getLogger().info("Disabling");
                    return;

                }

                break;
            case "yaml":

                //TODO

                break;
            case "none":
                break;
            default:

                getLogger().warning("Invalid database type specified: " + getConfig().getString("database"));
                setEnabled(false);
                ClansMain.getInstance().getLogger().info("Disabling");

                return;
        }

        if (getConfig().getBoolean("cachedata")) {
            getLogger().info("Loading database cache");
            try {
                ClansDatabase.getDatabase().loadCache();
            } catch (DatabaseException e) {
                getLogger().warning("Could not load database cache");
                e.printStackTrace();
                setEnabled(false);
                ClansMain.getInstance().getLogger().info("Disabling");
                return;
            }
        }

        new PermissionManager();
        new TeleportTimer();
        new ChatModeManager();

        CombatTagManager combatTagManager = new CombatTagManager(getConfig().getBoolean("combatTag.enabled"), getConfig().getLong("combatTag.seconds"));

        getCommand("clans").setExecutor(new CommandClans());
        getCommand("clansadmin").setExecutor(new CommandClansAdmin(combatTagManager));

        JoinListener jlistener = new JoinListener();
        getServer().getPluginManager().registerEvents(new ChatListener(), this);
        getServer().getPluginManager().registerEvents(jlistener, this);
        getServer().getPluginManager().registerEvents(new BlockListener(), this);
        getServer().getPluginManager().registerEvents(new InteractListener(), this);
        getServer().getPluginManager().registerEvents(new ContainerListener(), this);
        getServer().getPluginManager().registerEvents(new DoorListener(), this);
        getServer().getPluginManager().registerEvents(new MoveListener(), this);
        getServer().getPluginManager().registerEvents(new LeaveListener(combatTagManager), this);
        getServer().getPluginManager().registerEvents(new BlockFlagListener(), this);
        getServer().getPluginManager().registerEvents(new PVPListener(combatTagManager), this);
        getServer().getPluginManager().registerEvents(new DeathFlagListener(), this);

        new PowerIncrementer().runTaskTimer(this, 1200, 1200);

        if (getServer().getOnlinePlayers().size() > 0) {

            //server was reloaded

            for (Player p : getServer().getOnlinePlayers()) {
                jlistener.initPlayer(p);
            }
        }
    }

    @Override
    public void onDisable() {
        try {
            if (ClansDatabase.getDatabase() != null) ClansDatabase.getDatabase().close();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    public synchronized FileConfiguration getConfig() {
        return super.getConfig();
    }

}
