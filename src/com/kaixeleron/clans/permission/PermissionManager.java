package com.kaixeleron.clans.permission;

import com.kaixeleron.clans.data.DatabaseException;
import com.kaixeleron.clans.data.Policy;
import com.kaixeleron.clans.data.Rank;
import com.kaixeleron.clans.data.ClansDatabase;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PermissionManager {

    private static PermissionManager instance;
    private ArrayList<Player> bypassing;

    public PermissionManager() {
        bypassing = new ArrayList<>();

        PermissionManager.instance = this;
    }

    public static PermissionManager getInstance() {
        return instance;
    }

    public boolean canUse(CommandSender sender, String subcommand, String clan) {

        if (isBypassing(sender)) {
            if (!(subcommand.equals("create") || subcommand.equals("leave"))) return true;
        }

        boolean out = false;

        switch (subcommand) {
            case "access":
            case "claim":
            case "home":
            case "sethome":
            case "unclaim":
            case "ally":
            case "promote":
            case "truce":
            case "name":
            case "neutral":
            case "perm":
            case "kick":
            case "leader":
            case "demote":
            case "desc":
            case "disband":
            case "enemy":
            case "flag":
            case "invite":
            case "title":
            case "warp":
            case "setwarp":
            case "delhome":

                if (!sender.hasPermission("kxclans.command." + subcommand)) return false;

                try {

                    if (clan.length() == 0) clan = ClansDatabase.getDatabase().getClan((Player) sender);

                    Rank rank = ClansDatabase.getDatabase().getClanRank(((Player) sender).getUniqueId(), clan);

                    if (rank == null) {

                        Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

                        out = ClansDatabase.getDatabase().getClanPermissions(clan, rel).get(subcommand);

                    } else {

                        out = ClansDatabase.getDatabase().getClanPermissions(clan, rank).get(subcommand);

                    }

                } catch (DatabaseException e) {
                    sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
                    e.printStackTrace();
                }

                return out;

            case "help":
            case "map":
            case "seechunk":
            case "join":
            case "player":
            case "clan":
            case "c":
            case "chat":

                return sender.hasPermission("kxclans.command." + subcommand);

            case "leave":

                if (!sender.hasPermission("kxclans.command." + subcommand)) return false;

                try {

                    return !ClansDatabase.getDatabase().getClan((Player) sender).equals(ClansDatabase.getUnclaimedName());

                } catch (DatabaseException e) {
                    sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
                    e.printStackTrace();
                }

                break;

            case "create":

                if (!sender.hasPermission("kxclans.command." + subcommand)) return false;

                try {

                    return ClansDatabase.getDatabase().getClan((Player) sender).equals(ClansDatabase.getUnclaimedName());

                } catch (DatabaseException e) {
                    sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
                    e.printStackTrace();
                }

                break;
            case "build":
            case "door":
            case "interact":
            case "container":

                try {

                    if (clan.length() == 0) clan = ClansDatabase.getDatabase().getClan((Player) sender);

                    Rank rank = ClansDatabase.getDatabase().getClanRank(((Player) sender).getUniqueId(), clan);

                    if (rank == null) {

                        Policy rel = ClansDatabase.getDatabase().getPolicy(ClansDatabase.getDatabase().getClan((Player) sender), clan);

                        out = ClansDatabase.getDatabase().getClanPermissions(clan, rel).get(subcommand);

                    } else {

                        out = ClansDatabase.getDatabase().getClanPermissions(clan, rank).get(subcommand);

                    }

                } catch (DatabaseException e) {
                    sender.sendMessage(ChatColor.RED + "A database error occurred. Contact the server administrator.");
                    e.printStackTrace();
                }

                return out;
        }

        return false;
    }

    public boolean isBypassing(CommandSender s) {

        if (s instanceof Player) {

            Player p = (Player) s;

            if (bypassing.contains(p) && !p.hasPermission("kxclans.admin.bypass")) {
                bypassing.remove(p);
                return false;
            }

            return bypassing.contains(p);
        } else {
            return true;
        }
    }

    public void setBypassing(Player p, boolean bypass) {
        if (bypass) {
            if (bypassing.contains(p)) return;
            bypassing.add(p);
        } else {
            bypassing.remove(p);
        }
    }
}
