package com.kaixeleron.clans.command;

import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.subcommand.user.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CommandClans implements CommandExecutor {

    private HashMap<String, Subcommand> subcommands;

    public CommandClans() {

        subcommands = new HashMap<>();
        subcommands.put("help", new SubcommandHelp());
        subcommands.put("create", new SubcommandCreate());
        subcommands.put("disband", new SubcommandDisband());
        SubcommandClan sc = new SubcommandClan();
        subcommands.put("clan", sc);
        subcommands.put("c", sc);
        subcommands.put("info", sc);
        subcommands.put("desc", new SubcommandDescription());
        subcommands.put("claim", new SubcommandClaim());
        subcommands.put("unclaim", new SubcommandUnclaim());
        subcommands.put("home", new SubcommandHome());
        subcommands.put("sethome", new SubcommandSetHome());
        subcommands.put("delhome", new SubcommandDelHome());
        SubcommandShowChunk ssc = new SubcommandShowChunk();
        subcommands.put("showchunk", ssc);
        subcommands.put("seechunk", ssc);
        subcommands.put("sc", ssc);
        subcommands.put("setwarp", new SubcommandSetWarp());
        subcommands.put("warp", new SubcommandWarp());
        subcommands.put("delwarp", new SubcommandDelWarp());
        subcommands.put("leader", new SubcommandLeader());
        subcommands.put("perm", new SubcommandPerm());
        subcommands.put("map", new SubcommandMap());
        subcommands.put("invite", new SubcommandInvite());
        subcommands.put("kick", new SubcommandKick());
        subcommands.put("chat", new SubcommandChat());
        subcommands.put("leave", new SubcommandLeave());
        SubcommandPolicy sp = new SubcommandPolicy();
        subcommands.put("ally", sp);
        subcommands.put("truce", sp);
        subcommands.put("neutral", sp);
        subcommands.put("enemy", sp);
        subcommands.put("promote", new SubcommandPromote());
        subcommands.put("demote", new SubcommandDemote());
        subcommands.put("list", new SubcommandList());
        subcommands.put("name", new SubcommandName());
        subcommands.put("join", new SubcommandJoin());
        subcommands.put("flag", new SubcommandFlag());
        subcommands.put("title", new SubcommandTitle());
        subcommands.put("access", new SubcommandAccess());
        SubcommandPlayer spl = new SubcommandPlayer();
        subcommands.put("player", spl);
        subcommands.put("p", spl);

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("This comand can only be used by a player.");//TODO eventually make this better
            return true;
        }

        switch (args.length) {
            case 0:
                subcommands.get("help").execute(sender, label, "", "1");
                break;
            default:
                Subcommand sc = subcommands.get(args[0].toLowerCase());

                if (sc == null) {
                    sender.sendMessage(ChatColor.RED + "Unknown subcommand. Type " + ChatColor.WHITE + "/" + label + " help [Page] " + ChatColor.RED + "for help.");
                    break;
                }

                sc.execute(sender, label, args);

                break;
        }

        return true;
    }
}
