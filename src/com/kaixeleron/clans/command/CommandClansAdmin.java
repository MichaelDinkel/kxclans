package com.kaixeleron.clans.command;

import com.kaixeleron.clans.data.CombatTagManager;
import com.kaixeleron.clans.subcommand.Subcommand;
import com.kaixeleron.clans.subcommand.admin.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CommandClansAdmin implements CommandExecutor {

    private HashMap<String, Subcommand> subcommands;

    public CommandClansAdmin(CombatTagManager combatTagManager) {
        subcommands = new HashMap<>();

        subcommands.put("bypass", new SubcommandBypass());
        subcommands.put("claimsys", new SubcommandClaimsys());
        subcommands.put("unclaimsys", new SubcommandUnclaimsys());
        subcommands.put("namesys", new SubcommandNamesys());
        subcommands.put("descsys", new SubcommandDescsys());
        subcommands.put("powerboost", new SubcommandPowerboost());
        subcommands.put("flagsys", new SubcommandFlagsys());
        subcommands.put("reload", new SubcommandReload(combatTagManager));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        switch (args.length) {
            case 0:

                sender.sendMessage(ChatColor.RED + "===============[" + ChatColor.WHITE + " kxClans Admin Commands " + ChatColor.RED + "]===============");
                if (sender.hasPermission("kxclans.admin.bypass"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + ((sender instanceof Player) ? "" : ChatColor.RED + "")
                            + " bypass" + ChatColor.GREEN + " - " + "Toggle admin bypass mode");

                if (sender.hasPermission("kxclans.admin.powerboost"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " powerboost " + ChatColor.GOLD +
                            "<Clan|Player> <Target> <Amount>" + ChatColor.GREEN + " - " + "Boost a clan or player's power");

                if (sender.hasPermission("kxclans.admin.namesys"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " namesys " + ChatColor.GOLD +
                            "<Safe|War|Unclaimed> <Name>" + ChatColor.GREEN + " - " + "Rename a system clan");

                if (sender.hasPermission("kxclans.admin.descsys"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " descsys " + ChatColor.GOLD +
                            "<Safe|War|Unclaimed> <Desc>" + ChatColor.GREEN + " - " + "Change a system clan's description");

                if (sender.hasPermission("kxclans.admin.claimsys"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " claimsys " + ChatColor.GOLD +
                            "<Target>" + ChatColor.GREEN + " - " + "Claim land for a system clan");

                if (sender.hasPermission("kxclans.admin.unclaimsys"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " unclaimsys" + ChatColor.GREEN + " - " + "Unclaim land owned by a system clan");

                if (sender.hasPermission("kxclans.admin.flagsys"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " flagsys" + ChatColor.GREEN + " - " + "Manage system clan flags");

                if (sender.hasPermission("kxclans.admin.reload"))
                    sender.sendMessage(ChatColor.YELLOW + "/" + label + " reload" + ChatColor.GREEN + " - " + "Reload all data");

                break;
            default:
                Subcommand sc = subcommands.get(args[0].toLowerCase());

                if (sc == null) {
                    sender.sendMessage(ChatColor.RED + "Unknown subcommand. Type " + ChatColor.WHITE + "/" + label + " help [Page] " + ChatColor.RED + "for help.");
                    break;
                }

                sc.execute(sender, label, args);

                break;
        }

        return true;
    }
}
